<?php
namespace Fooman\ConnectConvert;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectConvert
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class OrderXml extends AbstractConverter
{
    /**
     * @param array $orderItems
     *
     * @return string
     */
    public static function convert(array $orderItems)
    {
        array_walk_recursive($orderItems, '\Fooman\ConnectConvert\OrderXml::filterValuesforSimpleXml');

        $xml     = new \SimpleXMLElement('<Invoices></Invoices>');
        $invoice = $xml->addChild('Invoice');
        $invoice->addChild('Type', 'ACCREC');
        ContactXml::convert($orderItems, $invoice);
        $invoice->addChild('Date', $orderItems['createdAt']);
        if (isset($orderItems['dueDate'])) {
            $invoice->addChild('DueDate', $orderItems['dueDate']);
        } else {
            $invoice->addChild('DueDate', $orderItems['createdAt']);
        }
        if (!empty($orderItems['incrementId'])) {
            $invoice->addChild('InvoiceNumber', $orderItems['incrementId']);
        }
        if (isset($orderItems['reference'])) {
            $invoice->addChild('Reference', $orderItems['reference']);
        } else {
            $invoice->addChild('Reference', '');
        }

        if (isset($orderItems['url'])) {
            $invoice->addChild('Url', $orderItems['url']);
        }

        $invoice->addChild('CurrencyCode', $orderItems['currencyCode']);
        if (isset($orderItems['status'])) {
            $invoice->addChild('Status', $orderItems['status']);
        } else {
            $invoice->addChild('Status', 'DRAFT');
        }

        if (isset($orderItems['lineAmountTypes'])) {
            $invoice->addChild('LineAmountTypes', $orderItems['lineAmountTypes']);
        } else {
            $invoice->addChild('LineAmountTypes', 'Inclusive');
        }

        $invoice->addChild('TotalTax', $orderItems['taxAmount']);
        $invoice->addChild('Total', $orderItems['grandTotal']);
        LineItemsXml::convert($orderItems, $invoice);

        $dom                     = new \DOMDocument("1.0");
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput       = true;
        $dom->substituteEntities = false;
        $dom->loadXML($xml->asXML());
        return $dom->saveXML();
    }
}
