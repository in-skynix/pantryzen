<?php
namespace Fooman\ConnectConvert;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectConvert
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class ItemsXml extends AbstractConverter
{
    /**
     * @param array            $data
     *
     * @return string
     */
    public static function convert(array $data)
    {
        array_walk_recursive($data, '\Fooman\ConnectConvert\ItemsXml::filterValuesforSimpleXml');

        //BC
        if (isset($data['invoiceLines'])) {
            return V1\ItemsXml::convert($data);
        }

        $items = new \SimpleXMLElement('<Items></Items>');

        foreach ($data as $singleItem) {
            $item = $items->addChild('Item');
            $item->addChild('Code', $singleItem['item_code']);
            if (isset($singleItem['description']) && !empty($singleItem['description'])) {
                $item->addChild('Description', $singleItem['description']);
            }
            if (isset($singleItem['name'])) {
                $item->addChild('Name', $singleItem['name']);
            }
        }

        $dom                     = new \DOMDocument("1.0");
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput       = true;
        $dom->substituteEntities = false;
        $dom->loadXML($items->asXML());
        return $dom->saveXML();
    }
}
