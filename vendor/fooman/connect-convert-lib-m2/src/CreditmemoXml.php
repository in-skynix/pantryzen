<?php
namespace Fooman\ConnectConvert;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectConvert
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class CreditmemoXml extends AbstractConverter
{
    /**
     * @param array $orderItems
     *
     * @return string
     */
    public static function convert(array $orderItems)
    {
        array_walk_recursive($orderItems, '\Fooman\ConnectConvert\CreditmemoXml::filterValuesforSimpleXml');

        $xml = new \SimpleXMLElement('<CreditNote></CreditNote>');
        $xml->addChild('Type', 'ACCRECCREDIT');
        ContactMinXml::convert($orderItems, $xml);
        $xml->addChild('Date', $orderItems['createdAt']);
        if (isset($orderItems['dueDate'])) {
            $xml->addChild('DueDate', $orderItems['dueDate']);
        } else {
            $xml->addChild('DueDate', $orderItems['createdAt']);
        }
        if (!empty($orderItems['incrementId'])) {
            $xml->addChild('CreditNoteNumber', $orderItems['incrementId']);
        }
        if (isset($orderItems['reference'])) {
            $xml->addChild('Reference', $orderItems['reference']);
        } else {
            $xml->addChild('Reference', '');
        }

        if (isset($orderItems['url'])) {
            $xml->addChild('Url', $orderItems['url']);
        }

        $xml->addChild('CurrencyCode', $orderItems['currencyCode']);
        if (isset($orderItems['status'])) {
            $xml->addChild('Status', $orderItems['status']);
        } else {
            $xml->addChild('Status', 'DRAFT');
        }

        if (isset($orderItems['lineAmountTypes'])) {
            $xml->addChild('LineAmountTypes', $orderItems['lineAmountTypes']);
        } else {
            $xml->addChild('LineAmountTypes', 'Inclusive');
        }

        $xml->addChild('TotalTax', $orderItems['taxAmount']);
        $xml->addChild('Total', $orderItems['grandTotal']);
        LineItemsXml::convert($orderItems, $xml);

        $dom = new \DOMDocument("1.0");
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->substituteEntities = false;
        $dom->loadXML($xml->asXML());
        return $dom->saveXML();
    }
}
