<?php
namespace Fooman\ConnectConvert;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectConvert
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class AbstractConverter
{
    /**
     * @param string $value
     * @param $key
     */
    protected static function filterValuesforSimpleXml(&$value, $key)
    {
        //remove &amp; so we don't end up with &amp;amp;
        $value = str_replace('&amp;', '&', $value);

        //replace
        $value = str_replace('&', '&amp;', $value);
    }
}
