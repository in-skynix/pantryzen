<?php
namespace Fooman\ConnectConvert\V1;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectConvert
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class LineItemsXml
{
    /**
     * @param array            $orderItems
     * @param array            $invoiceLine
     * @param \SimpleXMLElement $lineItem
     */
    public static function convertLineItem(array $orderItems, array $invoiceLine, \SimpleXMLElement $lineItem)
    {
        if (!isset($orderItems['lineAmountTypes'])) {
            if (isset($invoiceLine['taxAmount'])) {
                $lineItem->addChild('TaxAmount', $invoiceLine['taxAmount']);
            }
            if (isset($invoiceLine['itemCode'])) {
                $lineItem->addChild(
                    'UnitAmount',
                    (round(($invoiceLine['lineTotal'] / $invoiceLine['qtyOrdered']), 2))
                );
            }
        }
    }

    /**
     * Collection of compatibility with older versions code
     *
     * @param array            $orderItems
     * @param \SimpleXMLElement $lineItems
     */
    public static function convert(array $orderItems, \SimpleXMLElement $lineItems)
    {
        if (isset($orderItems['shippingAmount']) && $orderItems['shippingAmount'] > 0) {
            $lineItem = $lineItems->addChild('LineItem');
            $lineItem->addChild('Description', $orderItems['shippingDescription']);
            $lineItem->addChild('Quantity', 1);

            if (isset($orderItems['shippingTaxTypeOverride'])
                && $orderItems['shippingTaxTypeOverride']
                && $orderItems['shippingTaxTypeOverride'] != 1
            ) {
                $lineItem->addChild('TaxType', $orderItems['shippingTaxTypeOverride']);
            } elseif ($orderItems['shippingTaxAmount'] > 0) {
                $lineItem->addChild('TaxType', $orderItems['shippingTaxType']);
            } else {
                $lineItem->addChild('TaxType', 'NONE');
            }
            $lineItem->addChild('TaxAmount', $orderItems['shippingTaxAmount']);
            $lineItem->addChild('LineAmount', $orderItems['shippingAmount'] + $orderItems['shippingTaxAmount']);
            $lineItem->addChild('AccountCode', $orderItems['xeroAccountCodeShipping']);
            if (!empty($orderItems['xeroTrackingCategoryID'])) {
                $tracking         = $lineItem->addChild('Tracking');
                $trackingCategory = $tracking->addChild('TrackingCategory');
                $trackingCategory->addChild('TrackingCategoryID', $orderItems['xeroTrackingCategoryID']);
                $trackingCategory->addChild('Name', $orderItems['xeroTrackingName']);
                $trackingCategory->addChild('Option', $orderItems['xeroTrackingOption']);
            }
        }

        if (isset($orderItems['rounding']) && $orderItems['rounding'] != 0) {
            $lineItem = $lineItems->addChild('LineItem');
            $lineItem->addChild('Description', 'Rounding');
            $lineItem->addChild('Quantity', 1);
            $lineItem->addChild('TaxAmount', '0.00');
            $lineItem->addChild('LineAmount', $orderItems['rounding']);
            $lineItem->addChild('AccountCode', $orderItems['xeroAccountRounding']);
            if (!empty($orderItems['xeroTrackingCategoryID'])) {
                $tracking         = $lineItem->addChild('Tracking');
                $trackingCategory = $tracking->addChild('TrackingCategory');
                $trackingCategory->addChild('TrackingCategoryID', $orderItems['xeroTrackingCategoryID']);
                $trackingCategory->addChild('Name', $orderItems['xeroTrackingName']);
                $trackingCategory->addChild('Option', $orderItems['xeroTrackingOption']);
            }
        }
    }
}
