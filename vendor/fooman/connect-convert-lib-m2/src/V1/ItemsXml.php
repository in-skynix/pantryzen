<?php
namespace Fooman\ConnectConvert\V1;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectConvert
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class ItemsXml
{
    /**
     * @param array            $data
     *
     * @return string
     */
    public static function convert(array $data)
    {
        $items = new \SimpleXMLElement('<Items></Items>');

        foreach ($data['invoiceLines'] as $singleItem) {
            if (isset($singleItem['itemCode'])) {
                $item = $items->addChild('Item');
                $item->addChild('Code', $singleItem['itemCode']);
                $item->addChild('Description', $singleItem['name']);
            }
        }

        $dom                     = new \DOMDocument("1.0");
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput       = true;
        $dom->substituteEntities = false;
        $dom->loadXML($items->asXML());
        return $dom->saveXML();
    }
}
