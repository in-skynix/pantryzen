<?php
namespace Fooman\ConnectConvert;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectConvert
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class PaymentsXml extends AbstractConverter
{
    /**
     * @param array $data
     *
     * @return string
     */
    public static function convert(array $data)
    {
        array_walk_recursive($data, '\Fooman\ConnectConvert\PaymentsXml::filterValuesforSimpleXml');

        $payments = new \SimpleXMLElement('<Payments></Payments>');

        if (isset($data['Invoices'])) {
            foreach ($data['Invoices'] as $singleItem) {
                $payment = $payments->addChild('Payment');
                $invoice = $payment->addChild('Invoice');
                $invoice->addChild('InvoiceID', $singleItem['xero_id']);
                $account = $payment->addChild('Account');
                $account->addChild('AccountID', $singleItem['account_id']);
                $payment->addChild('Date', $singleItem['date']);
                $payment->addChild('Amount', $singleItem['amount']);
            }
        }

        if (isset($data['CreditNotes'])) {
            foreach ($data['CreditNotes'] as $singleItem) {
                $payment = $payments->addChild('Payment');
                $creditNote = $payment->addChild('CreditNote');
                $creditNote->addChild('CreditNoteID', $singleItem['xero_id']);
                $account = $payment->addChild('Account');
                $account->addChild('AccountID', $singleItem['account_id']);
                $payment->addChild('Date', $singleItem['date']);
                $payment->addChild('Amount', $singleItem['amount']);
                $payment->addChild('CurrencyRate', 1);
            }
        }

        $dom = new \DOMDocument("1.0");
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->substituteEntities = false;
        $dom->loadXML($payments->asXML());
        return $dom->saveXML();
    }
}
