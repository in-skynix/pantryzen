<?php
namespace Fooman\ConnectConvert;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectConvert
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class LineItemsXml
{
    /**
     * @param array            $orderItems
     * @param \SimpleXMLElement $root
     *
     * @return \SimpleXMLElement
     */
    public static function convert(array $orderItems, \SimpleXMLElement $root = null)
    {
        if (null === $root) {
            $lineItems = new \SimpleXMLElement('<LineItems></LineItems>');
        } else {
            $lineItems = $root->addChild('LineItems');
        }

        if (isset($orderItems['invoiceLines'])) {
            foreach ($orderItems['invoiceLines'] as $key => $invoiceLine) {
                $lineItem = $lineItems->addChild('LineItem');
                if (isset($invoiceLine['itemCode'])) {
                    $lineItem->addChild('ItemCode', $invoiceLine['itemCode']);
                    $description = $invoiceLine['name'];
                } else {
                    if (empty($invoiceLine['sku'])) {
                        $description = $invoiceLine['name'];
                    } else {
                        $description = '[' . $invoiceLine['sku'] . '] ' . $invoiceLine['name'];
                    }
                }
                if (isset($invoiceLine['discountRate'])) {
                    $lineItem->addChild('DiscountRate', $invoiceLine['discountRate']);
                }

                $lineItem->addChild('Description', $description);
                $lineItem->addChild('Quantity', $invoiceLine['qtyOrdered']);
                $lineItem->addChild('TaxType', $invoiceLine['taxType']);

                if (strpos($key, 'discount') === 0) {
                    $lineItem->addChild('AccountCode', $orderItems['xeroAccountCodeDiscounts']);
                } elseif ($key == 'surcharge') {
                    $lineItem->addChild('AccountCode', $orderItems['xeroAccountCodeSurcharge']);
                } elseif (isset($invoiceLine['xeroAccountCodeSale']) && !empty($invoiceLine['xeroAccountCodeSale'])) {
                    $lineItem->addChild('AccountCode', $invoiceLine['xeroAccountCodeSale']);
                } else {
                    $lineItem->addChild('AccountCode', $orderItems['xeroAccountCodeSale']);
                }

                if (isset($orderItems['lineAmountTypes'])) {
                    if ($orderItems['lineAmountTypes'] == 'Exclusive'
                        && isset($invoiceLine['taxAmount'])
                    ) {
                        $lineItem->addChild('TaxAmount', $invoiceLine['taxAmount']);
                    }
                    if (isset($invoiceLine['unitAmount'])) {
                        $lineItem->addChild('UnitAmount', $invoiceLine['unitAmount']);
                    }
                }

                if (isset($invoiceLine['lineTotal'])) {
                    $lineItem->addChild('LineAmount', $invoiceLine['lineTotal']);
                }

                if (!empty($orderItems['xeroTrackingCategoryID'])) {
                    $tracking = $lineItem->addChild('Tracking');
                    $trackingCategory = $tracking->addChild('TrackingCategory');
                    $trackingCategory->addChild('TrackingCategoryID', $orderItems['xeroTrackingCategoryID']);
                    $trackingCategory->addChild('Name', $orderItems['xeroTrackingName']);
                    $trackingCategory->addChild('Option', $orderItems['xeroTrackingOption']);
                }
                V1\LineItemsXml::convertLineItem($orderItems, $invoiceLine, $lineItem);
            }
        }

        V1\LineItemsXml::convert($orderItems, $lineItems);

        return $lineItems;
    }
}
