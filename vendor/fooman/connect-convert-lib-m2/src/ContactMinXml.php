<?php
namespace Fooman\ConnectConvert;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectConvert
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class ContactMinXml
{
    /**
     * @param array            $orderItems
     * @param \SimpleXMLElement $root
     *
     * @return \SimpleXMLElement
     */
    public static function convert(array $orderItems, \SimpleXMLElement $root = null)
    {
        if (null === $root) {
            $contact = new \SimpleXMLElement('<Contact></Contact>');
        } else {
            $contact = $root->addChild('Contact');
        }

        if (isset($orderItems['xeroContactID']) && !empty($orderItems['xeroContactID'])) {
            $contact->addChild('ContactID', $orderItems['xeroContactID']);
        }
        if (isset($orderItems['customerId']) && !empty($orderItems['customerId'])) {
            $contact->addChild('ContactNumber', $orderItems['customerId']);
        }
        $contact->addChild('Name', trim($orderItems['customerFirstname'] . ' ' . $orderItems['customerLastname']));
        $contact->addChild('EmailAddress', $orderItems['customerEmail']);

        if (isset($orderItems['taxNumber']) && $orderItems['taxNumber']) {
            $contact->addChild('TaxNumber', $orderItems['taxNumber']);
        }

        return $contact;
    }
}
