<?php
namespace Fooman\ConnectConvert;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectConvert
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class ContactXml
{
    /**
     * @param array            $orderItems
     * @param \SimpleXMLElement $root
     *
     * @return \SimpleXMLElement
     */
    public static function convert(array $orderItems, \SimpleXMLElement $root = null)
    {
        if (null === $root) {
            $contact = new \SimpleXMLElement('<Contact></Contact>');
        } else {
            $contact = $root->addChild('Contact');
        }

        if (isset($orderItems['xeroContactID']) && !empty($orderItems['xeroContactID'])) {
            $contact->addChild('ContactID', $orderItems['xeroContactID']);
        }
        if (isset($orderItems['customerId']) && !empty($orderItems['customerId'])) {
            $contact->addChild('ContactNumber', $orderItems['customerId']);
        }
        if (isset($orderItems['contactPerson'])) {
            $contactPersons = $contact->addChild('ContactPersons');
            $contactPerson = $contactPersons->addChild('ContactPerson');
            $contactPerson->addChild('FirstName', $orderItems['contactPerson']['firstName']);
            $contactPerson->addChild('LastName', $orderItems['contactPerson']['lastName']);
            $contactPerson->addChild('EmailAddress', $orderItems['contactPerson']['email']);
            $contactPerson->addChild('IncludeInEmails', 'true');
        }

        $contact->addChild('Name', trim($orderItems['customerFirstname'] . ' ' . $orderItems['customerLastname']));
        $contact->addChild('EmailAddress', $orderItems['customerEmail']);
        $addresses = $contact->addChild('Addresses');
        $address   = $addresses->addChild('Address');
        $address->addChild('AddressType', 'POBOX');
        $address->addChild('AddressLine1', $orderItems['billingStreet1']);
        $address->addChild('AddressLine2', $orderItems['billingStreet2']);
        $address->addChild('AddressLine3', '');
        $address->addChild('AddressLine4', '');
        $address->addChild('City', $orderItems['billingCity']);
        $address->addChild('Region', $orderItems['billingRegion']);
        $address->addChild('PostalCode', $orderItems['billingPostcode']);
        $address->addChild('Country', $orderItems['billingCountry']);

        if (isset($orderItems['firstName'])) {
            $contact->addChild('FirstName', $orderItems['firstName']);
            $contact->addChild('LastName', $orderItems['lastName']);
            $address->addChild('AttentionTo', trim($orderItems['firstName']. ' '. $orderItems['lastName']));
        }

        $phones = $contact->addChild('Phones');
        $phone  = $phones->addChild('Phone');
        $phone->addChild('PhoneType', 'DEFAULT');
        $phone->addChild('PhoneNumber', $orderItems['billingTelephone']);
        $phone->addChild('PhoneAreaCode', '');
        $phone->addChild('PhoneCountryCode', '');

        $contact->addChild('IsCustomer', 'true');
        if (isset($orderItems['taxNumber']) && $orderItems['taxNumber']) {
            $contact->addChild('TaxNumber', str_replace(' ', '', $orderItems['taxNumber']));
        }
        if (null === $root) {
            $dom = new \DOMDocument("1.0");
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->substituteEntities = false;
            $dom->loadXML($contact->asXML());
            return $dom->saveXML();
        } else {
            return $contact;
        }
    }
}
