<?php
namespace Fooman\ConnectLicense\Model\DataSource\Converter;

/**
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectLicense
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class OrderXml
{
    /**
     * @param array $orderItems
     *
     * @return string
     */
    public static function convert(array $orderItems)
    {
        return \Fooman\ConnectConvert\OrderXml::convert($orderItems);
    }
}
