<?php
namespace Fooman\ConnectLicense\Model\DataSource\Converter;

/**
 * @author     Kristof Ringleff
 * @package    Fooman_ConnectLicense
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class ItemsXml
{
    /**
     * @param array            $data
     * @param \SimpleXMLElement $root
     *
     * @return string
     */
    public static function convert(array $data, \SimpleXMLElement $root = null)
    {
        return \Fooman\ConnectConvert\ItemsXml::convert($data, $root);
    }
}
