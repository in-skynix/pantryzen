<?php

namespace Fooman\Connect\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $this->createOrderTable($installer);
        $this->createOrderErrorTable($installer);
        $this->createInvoiceTable($installer);
        $this->createInvoiceErrorTable($installer);
        $this->createCreditmemoTable($installer);
        $this->createCreditmemoErrorTable($installer);
        $this->createItemTable($installer);
        $this->createItemErrorTable($installer);
        $this->createTrackingCategoryTable($installer);
        $this->createTaxrateLinkTable($installer);

        $installer->endSetup();
    }

    /**
     * @param $installer
     *
     * @return mixed
     */
    protected function createOrderTable(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()
            ->newTable($installer->getTable('foomanconnect_order'))
            ->addColumn(
                'order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Order Id'
            )
            ->addColumn(
                'xero_invoice_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Xero Invoice Id'
            )
            ->addColumn(
                'xero_invoice_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Xero Invoice Number'
            )
            ->addColumn(
                'xero_export_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => true,
                    'default'  => 0,
                ],
                'Xero Export Status'
            )
            ->addForeignKey(
                $installer->getFkName('foomanconnect_order', 'order_id', 'sales_order', 'entity_id'),
                'order_id',
                $installer->getTable('sales_order'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);
        return $table;
    }

    /**
     * @param $installer
     *
     * @return mixed
     */
    protected function createOrderErrorTable(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()
            ->newTable($installer->getTable('foomanconnect_order_error'))
            ->addColumn(
                'order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Order Id'
            )
            ->addColumn(
                'xero_last_validation_errors',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                4000,
                [],
                'Xero Last Errors'
            )
            ->addForeignKey(
                $installer->getFkName('foomanconnect_order_error', 'order_id', 'foomanconnect_order', 'order_id'),
                'order_id',
                $installer->getTable('foomanconnect_order'),
                'order_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);
        return $table;
    }

    /**
     * @param $installer
     *
     * @return mixed
     */
    protected function createInvoiceTable(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()
            ->newTable($installer->getTable('foomanconnect_invoice'))
            ->addColumn(
                'invoice_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Invoice Id'
            )
            ->addColumn(
                'xero_invoice_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Xero Invoice Id'
            )
            ->addColumn(
                'xero_invoice_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Xero Invoice Number'
            )
            ->addColumn(
                'xero_export_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => true,
                    'default'  => 0,
                ],
                'Xero Export Status'
            )
            ->addForeignKey(
                $installer->getFkName('foomanconnect_invoice', 'invoice_id', 'sales_invoice', 'entity_id'),
                'invoice_id',
                $installer->getTable('sales_invoice'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );

        $installer->getConnection()->createTable($table);
        return $table;
    }

    /**
     * @param $installer
     *
     * @return mixed
     */
    protected function createInvoiceErrorTable(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()
            ->newTable($installer->getTable('foomanconnect_invoice_error'))
            ->addColumn(
                'invoice_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Invoice Id'
            )
            ->addColumn(
                'xero_last_validation_errors',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                4000,
                [],
                'Xero Last Errors'
            )
            ->addForeignKey(
                $installer->getFkName(
                    'foomanconnect_invoice_error',
                    'invoice_id',
                    'foomanconnect_invoice',
                    'invoice_id'
                ),
                'invoice_id',
                $installer->getTable('foomanconnect_invoice'),
                'invoice_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);
        return $table;
    }

    /**
     * @param $installer
     *
     * @return mixed
     */
    protected function createCreditmemoTable(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()
            ->newTable($installer->getTable('foomanconnect_creditmemo'))
            ->addColumn(
                'creditmemo_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Creditmemo Id'
            )
            ->addColumn(
                'xero_creditnote_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Xero Credit Note Id'
            )
            ->addColumn(
                'xero_creditnote_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Xero Credit Note Number'
            )
            ->addColumn(
                'xero_export_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => true,
                    'default'  => 0,
                ],
                'Xero Export Status'
            )
            ->addForeignKey(
                $installer->getFkName('foomanconnect_creditmemo', 'creditmemo_id', 'sales_creditmemo', 'entity_id'),
                'creditmemo_id',
                $installer->getTable('sales_creditmemo'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );

        $installer->getConnection()->createTable($table);
        return $table;
    }

    /**
     * @param $installer
     *
     * @return mixed
     */
    protected function createCreditmemoErrorTable(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()
            ->newTable($installer->getTable('foomanconnect_creditmemo_error'))
            ->addColumn(
                'creditmemo_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Invoice Id'
            )
            ->addColumn(
                'xero_last_validation_errors',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                4000,
                [],
                'Xero Last Errors'
            )
            ->addForeignKey(
                $installer->getFkName(
                    'foomanconnect_creditmemo_error',
                    'creditmemo_id',
                    'foomanconnect_creditmemo',
                    'creditmemo_id'
                ),
                'creditmemo_id',
                $installer->getTable('foomanconnect_creditmemo'),
                'creditmemo_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);
        return $table;
    }

    /**
     * @param $installer
     *
     * @return mixed
     */
    protected function createItemTable(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()
            ->newTable($installer->getTable('foomanconnect_item'))
            ->addColumn(
                'item_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                30,
                [
                    'nullable' => false,
                    'primary'  => true,
                ],
                'Item Code'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                [
                    'nullable' => true,
                ],
                'Name'
            )
            ->addColumn(
                'description',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Description'
            )
            ->addColumn(
                'xero_item_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Xero Item Id'
            )
            ->addColumn(
                'xero_export_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => true,
                    'default'  => 0,
                ],
                'Xero Export Status'
            );

        $installer->getConnection()->createTable($table);
        return $table;
    }

    /**
     * @param $installer
     *
     * @return mixed
     */
    protected function createItemErrorTable(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()
            ->newTable($installer->getTable('foomanconnect_item_error'))
            ->addColumn(
                'item_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                30,
                [
                    'nullable' => false,
                    'primary'  => true,
                ],
                'Item Code'
            )
            ->addColumn(
                'xero_last_validation_errors',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                4000,
                [],
                'Xero Last Errors'
            )
            ->addForeignKey(
                $installer->getFkName(
                    'foomanconnect_item_error',
                    'item_code',
                    'foomanconnect_item',
                    'item_code'
                ),
                'item_code',
                $installer->getTable('foomanconnect_item'),
                'item_code',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);
        return $table;
    }

    /**
     * @param $installer
     */
    protected function createTrackingCategoryTable(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()
            ->newTable($installer->getTable('foomanconnect_tracking_rule'))
            ->addColumn(
                'rule_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Rule Id'
            )
            ->addColumn(
                'type',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                30,
                [],
                'Rule Type'
            )
            ->addColumn(
                'source_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'nullable' => false,
                    'unsigned' => true,
                ],
                'Source Id'
            )
            ->addColumn(
                'store_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'Store Id'
            )
            ->addColumn(
                'tracking_category_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Tracking Category Id'
            )
            ->addColumn(
                'tracking_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Tracking Name'
            )
            ->addColumn(
                'tracking_option',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Tracking Option'
            )
            ->addColumn(
                'sort_order',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => true,
                    'default'  => 0,
                ],
                'Sort Order'
            )
            ->addForeignKey(
                $installer->getFkName('foomanconnect_tracking_rule', 'store_id', 'store', 'store_id'),
                'store_id',
                $installer->getTable('store'),
                'store_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );

        $installer->getConnection()->createTable($table);
    }

    protected function createTaxrateLinkTable(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()
            ->newTable($installer->getTable(\Fooman\Connect\Model\ResourceModel\TaxrateLink::TABLE_NAME))
            ->addColumn(
                'taxrate_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                ],
                'Magento Taxrate Id'
            )
            ->addColumn(
                'magento_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => false,
                ],
                'Magento Code'
            )
            ->addColumn(
                'xero_taxrate',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => true,
                ],
                'Xero Taxrate'
            )
            ->addForeignKey(
                $installer->getFkName(
                    \Fooman\Connect\Model\ResourceModel\TaxrateLink::TABLE_NAME,
                    'taxrate_id',
                    'tax_calculation_rate',
                    'tax_calculation_rate_id'
                ),
                'taxrate_id',
                $installer->getTable('tax_calculation_rate'),
                'tax_calculation_rate_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);
        return $table;
    }
}
