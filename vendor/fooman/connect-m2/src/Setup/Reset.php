<?php

namespace Fooman\Connect\Setup;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Reset
{
    protected $setup;

    protected $cacheTypeList;

    public function __construct(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\App\Cache\TypeListInterface $typeListInterface
    ) {
        $this->setup = $setup;
        $this->cacheTypeList = $typeListInterface;
    }

    public function resetXeroInformation()
    {
        $this->setup->getConnection()->truncate($this->getTable('foomanconnect_order'));
        $this->setup->getConnection()->truncate($this->getTable('foomanconnect_invoice'));
        $this->setup->getConnection()->truncate($this->getTable('foomanconnect_creditmemo'));
        $this->setup->getConnection()->truncate($this->getTable('foomanconnect_item'));

        $this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Collection::TYPE_IDENTIFIER);
    }
}
