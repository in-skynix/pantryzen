<?php

namespace Fooman\Connect\Block\Adminhtml;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Creditmemo extends \Magento\Backend\Block\Widget\Grid\Container
{

    protected function _construct()
    {
        if ($this->_scopeConfig->getValue(
            'foomanconnect/settings/xeroenablereset',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        )
        ) {
            $this->_addButton(
                'reset_xero',
                [
                    'label'   => __('Reset All'),
                    'onclick' => "location.href='" . $this->getUrl('*/*/resetAll') . "'",
                    'class'   => '',
                ]
            );
        }

        $this->_controller = 'adminhtml_creditmemo';
        $this->_blockGroup = 'foomanconnect';
        $this->_headerText = __('Fooman Connect: Xero - Credit Memos');

        parent::_construct();
        $this->_removeButton('add');
    }
}
