<?php

namespace Fooman\Connect\Block\Adminhtml\Sales\Order\View\Tab;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Xero extends \Magento\Sales\Block\Adminhtml\Order\AbstractOrder implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Fooman\Connect\Model\OrderFactory
     */
    protected $connectOrderFactory;

    /**
     * @var \Fooman\Connect\Model\Xero\ApiFactory
     */
    protected $connectXeroApiFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \DateTime
     */
    protected $date;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Fooman\Connect\Model\OrderFactory $connectOrderFactory,
        \Fooman\Connect\Model\Xero\ApiFactory $connectXeroApiFactory,
        \Magento\Sales\Helper\Admin $adminHelper,
        \DateTime $date,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->connectOrderFactory = $connectOrderFactory;
        $this->connectXeroApiFactory = $connectXeroApiFactory;
        $this->scopeConfig = $context->getScopeConfig();
        $this->date = $date;
        parent::__construct($context, $registry, $adminHelper, $data);
    }
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('fooman/connect/order/view/tab/xero.phtml');
    }

    public function getTabLabel()
    {
        return __('Xero');
    }

    public function getTabTitle()
    {
        return __('Fooman Connect > Xero');
    }

    public function isHidden()
    {
        return false;
    }

    public function canShowTab()
    {
        return true;
    }

    public function getXeroOrderStatus()
    {
        $id = $this->getOrder()->getId();

        return $this->connectOrderFactory->create()->load($id);
    }

    public function getXeroPayments()
    {
        return $this->connectXeroApiFactory->create()->getPaymentsForInvoice(
            $this->getXeroOrderStatus()->getXeroInvoiceId()
        );
    }

    public function getXeroUrl()
    {
        $xeroInvoiceId = $this->getXeroOrderStatus()->getXeroInvoiceId();

        return \Fooman\Connect\Model\Xero\Api::XERO_INVOICE_LINK.$xeroInvoiceId;
    }

    public function displayPayments()
    {
        return $this->scopeConfig->getValue(
            'foomanconnect/order/xeropayments',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getOrder()->getStoreId()
        );
    }

    public function isExported()
    {
        if (!$this->getXeroOrderStatus()) {
            return false;
        }

        return ($this->getXeroOrderStatus()->getXeroExportStatus()
            == \Fooman\Connect\Model\Status::EXPORTED);
    }

    public function getXeroLastValidationErrors()
    {
        if (!$this->getXeroOrderStatus()) {
            return false;
        }
        $validationErrors = $this->getXeroOrderStatus()->getXeroLastValidationErrors();
        if ($validationErrors) {
            $validationErrorsArray = json_decode($validationErrors, true);
            if (!empty($validationErrorsArray)) {
                if (!is_array($validationErrorsArray)) {
                    return [$validationErrorsArray];
                }

                return $validationErrorsArray;
            }
        }

        return [];
    }

    public function shouldDisplay()
    {
        return $this->scopeConfig->getValue(
            'foomanconnect/order/exportmode',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getOrder()->getStoreId()
        ) === \Fooman\Connect\Model\System\ExportMode::ORDER_MODE;
    }

    public function getDisplayDateObject($timestamp)
    {
        $this->date->setTimestamp($timestamp);
        return $this->date;
    }
}
