<?php

namespace Fooman\Connect\Block\Adminhtml\Sales\Invoice;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Xero extends \Magento\Sales\Block\Adminhtml\Order\AbstractOrder
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Fooman\Connect\Model\InvoiceFactory
     */
    protected $connectInvoiceFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Fooman\Connect\Model\InvoiceFactory $invoiceFactory,
        \Magento\Sales\Helper\Admin $adminHelper,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->connectInvoiceFactory = $invoiceFactory;
        parent::__construct($context, $registry, $adminHelper, $data);
    }
    public function getInvoice()
    {
        return $this->registry->registry('current_invoice');
    }

    public function getXeroUrl()
    {
        $xeroCreditNoteId = $this->getXeroOrderStatus()->getXeroInvoiceId();

        return \Fooman\Connect\Model\Xero\Api::XERO_CREDITNOTE_LINK.$xeroCreditNoteId;
    }

    public function displayPayments()
    {
        return false;
    }

    public function getXeroOrderStatus()
    {
        $id = $this->getInvoice()->getId();

        return $this->connectInvoiceFactory->create()->load($id);
    }

    public function shouldDisplay()
    {
        return $this->scopeConfig->getValue(
            'foomanconnect/order/exportmode',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getInvoice()->getStoreId()
        ) === \Fooman\Connect\Model\System\ExportMode::INVOICE_MODE;
    }

    public function isExported()
    {
        if (!$this->getXeroOrderStatus()) {
            return false;
        }

        return ($this->getXeroOrderStatus()->getXeroExportStatus()
            == \Fooman\Connect\Model\Status::EXPORTED);
    }

    public function getXeroLastValidationErrors()
    {
        if (!$this->getXeroOrderStatus()) {
            return false;
        }
        $validationErrors = $this->getXeroOrderStatus()->getXeroLastValidationErrors();
        if ($validationErrors) {
            $validationErrorsArray = json_decode($validationErrors, true);
            if (!empty($validationErrorsArray)) {
                if (!is_array($validationErrorsArray)) {
                    return [$validationErrorsArray];
                }

                return $validationErrorsArray;
            }
        }

        return [];
    }

    public function getDisplayDateObject($timestamp)
    {
        $this->date->setTimestamp($timestamp);
        return $this->date;
    }
}
