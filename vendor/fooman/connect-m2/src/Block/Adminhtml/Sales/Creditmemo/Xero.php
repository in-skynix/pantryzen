<?php

namespace Fooman\Connect\Block\Adminhtml\Sales\Creditmemo;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Xero extends \Magento\Sales\Block\Adminhtml\Order\AbstractOrder
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Fooman\Connect\Model\CreditmemoFactory
     */
    protected $connectCreditmemoFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Fooman\Connect\Model\CreditmemoFactory $connectCreditmemoFactory,
        \Magento\Sales\Helper\Admin $adminHelper,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->connectCreditmemoFactory = $connectCreditmemoFactory;
        parent::__construct($context, $registry, $adminHelper, $data);
    }

    public function getCreditmemo()
    {
        return $this->registry->registry('current_creditmemo');
    }

    public function getXeroUrl()
    {
        $xeroCreditNoteId = $this->getXeroOrderStatus()->getXeroCreditnoteId();

        return \Fooman\Connect\Model\Xero\Api::XERO_CREDITNOTE_LINK.$xeroCreditNoteId;
    }

    public function displayPayments()
    {
        return false;
    }

    public function getXeroOrderStatus()
    {
        $id = $this->getCreditmemo()->getId();

        return $this->connectCreditmemoFactory->create()->load($id);
    }

    public function shouldDisplay()
    {
        return true;
    }

    public function isExported()
    {
        if (!$this->getXeroOrderStatus()) {
            return false;
        }

        return ($this->getXeroOrderStatus()->getXeroExportStatus()
            == \Fooman\Connect\Model\Status::EXPORTED);
    }

    public function getXeroLastValidationErrors()
    {
        if (!$this->getXeroOrderStatus()) {
            return false;
        }
        $validationErrors = $this->getXeroOrderStatus()->getXeroLastValidationErrors();
        if ($validationErrors) {
            $validationErrorsArray = json_decode($validationErrors, true);
            if (!empty($validationErrorsArray)) {
                if (!is_array($validationErrorsArray)) {
                    return [$validationErrorsArray];
                }

                return $validationErrorsArray;
            }
        }

        return [];
    }
}
