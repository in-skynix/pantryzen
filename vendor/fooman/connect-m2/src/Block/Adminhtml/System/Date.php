<?php

namespace Fooman\Connect\Block\Adminhtml\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Date extends \Magento\Framework\Data\Form\Element\Date
{
    public function getElementHtml()
    {
        $this->setFormat(\Magento\Framework\Stdlib\DateTime::DATE_INTERNAL_FORMAT);

        return parent::getElementHtml();
    }
}
