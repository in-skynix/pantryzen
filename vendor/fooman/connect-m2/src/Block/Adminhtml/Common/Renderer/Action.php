<?php

namespace Fooman\Connect\Block\Adminhtml\Common\Renderer;

/**
 * @author     Kristof Ringleff
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Action
    extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{
    /**
     * Render single action as link html.
     *
     * @param array $action
     *
     * @return string
     */
    protected function _toLinkHtml($action, \Magento\Framework\DataObject $row)
    {
        /** @var \Fooman\Connect\Model\Abstract $row */
        if (false === $row->getSalesEntityViewId()) {
            return '&nbsp;';
        }

        return parent::_toLinkHtml($action, $row);
    }
}
