<?php

namespace Fooman\Connect\Controller\Adminhtml\Xero\Creditmemo;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\LayoutFactory;

class Index extends AbstractCreditmemo
{

    public function execute()
    {
        $this->_view->loadLayout();
        $this->_setActiveMenu('Fooman_Connect::xero_creditmemo');
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Fooman Connect'));
        $this->_addBreadcrumb(__('Xero - Credit Memos'), __('Xero - Credit Memos'));
        $this->_view->renderLayout();
    }
}
