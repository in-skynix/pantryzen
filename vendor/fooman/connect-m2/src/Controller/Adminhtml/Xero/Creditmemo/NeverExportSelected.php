<?php

namespace Fooman\Connect\Controller\Adminhtml\Xero\Creditmemo;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class NeverExportSelected extends AbstractCreditmemoMassAction
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Fooman\Connect\Model\Creditmemo
     */
    protected $connectCreditmemo;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * ExportSelected constructor.
     *
     * @param Context                                                               $context
     * @param \Magento\Ui\Component\MassAction\Filter                               $filter
     * @param \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory $creditmemoCollectionFactory
     * @param \Fooman\Connect\Model\Creditmemo                                      $connectCreditmemo
     */
    public function __construct(
        Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory $creditmemoCollectionFactory,
        \Fooman\Connect\Model\Creditmemo $connectCreditmemo
    ) {
        $this->collectionFactory = $creditmemoCollectionFactory;
        $this->request = $context->getRequest();
        $this->connectCreditmemo = $connectCreditmemo;
        $this->messageManager = $context->getMessageManager();
        parent::__construct($context, $filter);
    }

    /**
     * @param AbstractCollection $collection
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
        $successes = 0;
        foreach ($collection as $order) {
            try {
                $this->connectCreditmemo->neverExportOne($order);
                ++$successes;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        if ($successes == 1) {
            $this->messageManager->addSuccess(
                __('Successfully changed %1 export status', $successes)
            );
        } elseif ($successes) {
            $this->messageManager->addSuccess(
                __('Successfully changed %1 export statuses', $successes)
            );
        }

        return $this->redirect();
    }
}
