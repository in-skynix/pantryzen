<?php

namespace Fooman\Connect\Controller\Adminhtml\Xero\Creditmemo;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class ExportSelected extends AbstractCreditmemoMassAction
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Fooman\Connect\Model\Creditmemo
     */
    protected $connectCreditmemo;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * ExportSelected constructor.
     *
     * @param Context                                                               $context
     * @param \Magento\Ui\Component\MassAction\Filter                               $filter
     * @param \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory $creditmemoCollectionFactory
     * @param \Fooman\Connect\Model\Creditmemo                                      $connectCreditmemo
     */
    public function __construct(
        Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory $creditmemoCollectionFactory,
        \Fooman\Connect\Model\Creditmemo $connectCreditmemo
    ) {
        $this->collectionFactory = $creditmemoCollectionFactory;
        $this->request = $context->getRequest();
        $this->connectCreditmemo = $connectCreditmemo;
        parent::__construct($context, $filter);
    }

    /**
     * @param AbstractCollection $collection
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
        $successes = 0;
        foreach ($collection as $order) {
            try {
                $this->connectCreditmemo->exportOne($order);
                ++$successes;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        if ($successes == 1) {
            $this->messageManager->addSuccess(
                __('Successfully exported %1 credit memo', $successes)
            );
        } elseif ($successes) {
            $this->messageManager->addSuccess(
                __('Successfully exported %1 credit memos', $successes)
            );
        }

        return $this->redirect();
    }
}
