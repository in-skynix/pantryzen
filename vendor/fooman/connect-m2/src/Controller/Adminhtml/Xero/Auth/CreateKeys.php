<?php

namespace Fooman\Connect\Controller\Adminhtml\Xero\Auth;

use Fooman\Connect\Helper\Config;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Encryption\Encryptor;
use Magento\Framework\Message\ManagerInterface;

class CreateKeys extends AbstractAuth
{
    /**
     * @var ManagerInterface
     */
    protected $messageManagerInterface;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Fooman\Connect\Model\KeyManagerFactory
     */
    protected $keyManagerFactory;

    public function __construct(
        Context $context,
        \Fooman\Connect\Model\KeyManagerFactory $keyManagerFactory
    ) {
        $this->messageManagerInterface = $context->getMessageManager();
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->keyManagerFactory = $keyManagerFactory;
        $this->request = $context->getRequest();

        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $storeId = (int)$this->request->getParam('store_id', \Magento\Store\Model\Store::DEFAULT_STORE_ID);
            $this->keyManagerFactory->create(['storeId' => $storeId])->createPrivateKey();
        } catch (\Exception $e) {
            $this->messageManagerInterface->addError('Error ' . $e->getMessage());
        }

        //go back to the Fooman Connect > Xero page
        return $this->resultRedirectFactory->create()->setRefererUrl();
    }
}
