<?php

namespace Fooman\Connect\Controller\Adminhtml\Xero\Auth;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Encryption\Encryptor;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class DownloadPublicKey extends AbstractAuth
{

    /**
     * @var Config
     */
    protected $helperConfig;

    /**
     * @var ManagerInterface
     */
    protected $messageManagerInterface;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Fooman\Connect\Model\KeyManagerFactory
     */
    protected $keyManagerFactory;

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    protected $adminUser;

    public function __construct(
        Context $context,
        \Fooman\Connect\Model\KeyManagerFactory $keyManagerFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    ) {
        $this->messageManagerInterface = $context->getMessageManager();
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->keyManagerFactory = $keyManagerFactory;
        $this->fileFactory = $fileFactory;
        $this->request = $context->getRequest();
        $this->adminUser = $context->getAuth()->getUser();

        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $storeId = (int) $this->request->getParam('store_id', \Magento\Store\Model\Store::DEFAULT_STORE_ID);
            $keyManager = $this->keyManagerFactory->create(
                ['storeId' => $storeId, 'adminUserEmail' => $this->adminUser->getEmail()]
            );
            $publicKey = $keyManager->generatePublicKey();

            $filename = sprintf('publickey-%s.cer', $keyManager->getCommonName());

            return $this->fileFactory->create(
                $filename,
                $publicKey,
                DirectoryList::VAR_DIR,
                'application/x-x509-ca-cert'
            );
        } catch (\Exception $e) {
            $this->messageManagerInterface->addError('Error '.$e->getMessage());
        }

        //go back to the Fooman Connect > Xero page
        return $this->resultRedirectFactory->create()->setRefererUrl();
    }
}
