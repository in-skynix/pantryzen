<?php

/*
 * @author     Kristof Ringleff
 * @package    \Fooman\Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Fooman\Connect\Controller\Adminhtml\Xero\Auth;

use Magento\Backend\App\Action;

abstract class AbstractAuth extends Action
{
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Fooman_Connect::config_foomanconnect');
    }
}
