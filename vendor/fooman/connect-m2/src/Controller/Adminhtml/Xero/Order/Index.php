<?php

namespace Fooman\Connect\Controller\Adminhtml\Xero\Order;

class Index extends AbstractOrder
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Fooman\Connect\Helper\Config
     */
    protected $helperConfig;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Backend\Helper\Data
     */
    protected $helperBackend;

    /**
     * Index constructor.
     *
     * @param \Magento\Backend\App\Action\Context                $context
     * @param \Fooman\Connect\Helper\Config                      $helperConfig
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Fooman\Connect\Helper\Config $helperConfig,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->helperBackend = $context->getHelper();
        $this->helperConfig = $helperConfig;
        $this->messageManager = $context->getMessageManager();
        $this->scopeConfig = $scopeConfig;

        parent::__construct($context);
    }

    public function execute()
    {
        if (!$this->helperConfig->isConfigured()) {
            $this->messageManager->addError(
                __(
                    'Connection to Xero is not yet set up.'
                )
            );
            if (!$this->scopeConfig->getValue('foomanconnect/settings/privatekey')) {
                $this->messageManager->addNotice(
                    __(
                        'You can create a Private Key by clicking <a href="%1">here</a>.',
                        $this->helperBackend->getUrl('foomanconnect/xero_auth/createKeys')
                    )
                );
            } else {
                $this->messageManager->addNotice(
                    __(
                        'You can download the Public Key file for use in Xero by clicking <a href="%1">here</a>.',
                        $this->helperBackend->getUrl('foomanconnect/xero_auth/downloadPublicKey')
                    )
                );
            }
        }

        $this->_view->loadLayout();
        $this->_setActiveMenu('Fooman_Connect::xero_order');
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Fooman Connect'));
        $this->_addBreadcrumb(__('Xero - Orders'), __('Xero - Orders'));

        $this->_view->renderLayout();
    }
}
