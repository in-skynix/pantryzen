<?php

namespace Fooman\Connect\Controller\Adminhtml\Xero\Order;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class NeverExportSelected extends AbstractOrderMassAction
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Fooman\Connect\Model\Order
     */
    protected $connectOrder;

    /**
     * ExportSelected constructor.
     *
     * @param Context                                                    $context
     * @param \Magento\Ui\Component\MassAction\Filter                    $filter
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Fooman\Connect\Model\Order                                $connectOrder
     */
    public function __construct(
        Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Fooman\Connect\Model\Order $connectOrder
    ) {
        $this->collectionFactory = $orderCollectionFactory;
        $this->connectOrder = $connectOrder;
        parent::__construct($context, $filter);
    }

    /**
     * @param AbstractCollection $collection
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
        $successes = 0;
        foreach ($collection as $order) {
            try {
                $this->connectOrder->neverExportOne($order);
                ++$successes;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        if ($successes == 1) {
            $this->messageManager->addSuccess(
                __('Successfully changed %1 export status', $successes)
            );
        } elseif ($successes) {
            $this->messageManager->addSuccess(
                __('Successfully changed %1 export statuses', $successes)
            );
        }

        return $this->redirect();
    }
}
