<?php

namespace Fooman\Connect\Controller\Adminhtml\Xero\Order;

use Fooman\Connect\Model\Item;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RedirectFactory;

class ExportItems extends AbstractOrder
{
    /**
     * @var Item
     */
    protected $modelItem;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    public function __construct(
        Context $context,
        Item $modelItem
    ) {
        $this->modelItem = $modelItem;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();

        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $stores = array_keys(Mage::app()->getStores());
            foreach ($stores as $storeId) {
                $this->modelItem->exportItems($storeId, true);
            }
        } catch (\Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        $this->_getSession()->addSuccess(__('Successfully Exported Items'));

        //go back to the order overview page
        $this->resultRedirectFactory->create()->setPath('adminhtml/xero_order');
    }
}
