<?php

namespace Fooman\Connect\Controller\Adminhtml\Xero\Invoice;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class ExportSelected extends AbstractInvoiceMassAction
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Fooman\Connect\Model\Invoice
     */
    protected $connectInvoice;

    /**
     * ExportSelected constructor.
     *
     * @param Context                                                            $context
     * @param \Magento\Ui\Component\MassAction\Filter                            $filter
     * @param \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory
     * @param \Fooman\Connect\Model\Invoice                                      $connectInvoice
     */
    public function __construct(
        Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory,
        \Fooman\Connect\Model\Invoice $connectInvoice
    ) {
        $this->collectionFactory = $invoiceCollectionFactory;
        $this->connectInvoice = $connectInvoice;
        parent::__construct($context, $filter);
    }

    /**
     * @param AbstractCollection $collection
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
        $successes = 0;
        foreach ($collection as $item) {
            try {
                $this->connectInvoice->exportOne($item);
                ++$successes;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        if ($successes == 1) {
            $this->messageManager->addSuccess(
                __('Successfully exported %1 invoice', $successes)
            );
        } elseif ($successes) {
            $this->messageManager->addSuccess(
                __('Successfully exported %1 invoices', $successes)
            );
        }

        return $this->redirect();
    }
}
