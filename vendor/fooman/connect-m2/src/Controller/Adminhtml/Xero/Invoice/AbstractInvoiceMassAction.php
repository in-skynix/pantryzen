<?php

/*
 * @author     Kristof Ringleff
 * @package    \Fooman\Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Fooman\Connect\Controller\Adminhtml\Xero\Invoice;

use Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction;

abstract class AbstractInvoiceMassAction extends AbstractMassAction
{
    protected $redirectUrl = 'foomanconnect/xero_invoice/index';

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Fooman_Connect::xero_invoice');
    }

    protected function redirect()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath($this->redirectUrl);

        return $resultRedirect;
    }
}
