<?php

namespace Fooman\Connect\Controller\Adminhtml\Xero\Invoice;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class NeverExportSelected extends AbstractInvoiceMassAction
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Fooman\Connect\Model\Invoice
     */
    protected $connectOrder;


    /**
     * ExportSelected constructor.
     *
     * @param Context                                                            $context
     * @param \Magento\Ui\Component\MassAction\Filter                            $filter
     * @param \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory
     * @param \Fooman\Connect\Model\Invoice                                      $connectInvoice
     */
    public function __construct(
        Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory,
        \Fooman\Connect\Model\Invoice $connectInvoice
    ) {
        $this->collectionFactory = $invoiceCollectionFactory;
        $this->connectInvoice = $connectInvoice;
        parent::__construct($context, $filter);
    }

    /**
     * @param AbstractCollection $collection
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
        $successes = 0;
        foreach ($collection as $item) {
            try {
                $this->connectInvoice->neverExportOne($item);
                ++$successes;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        if ($successes == 1) {
            $this->messageManager->addSuccess(
                __('Successfully changed %1 export status', $successes)
            );
        } elseif ($successes) {
            $this->messageManager->addSuccess(
                __('Successfully changed %1 export statuses', $successes)
            );
        }

        return $this->redirect();
    }
}
