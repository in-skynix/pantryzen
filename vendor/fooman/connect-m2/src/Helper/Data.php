<?php

namespace Fooman\Connect\Helper;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const LOG_FILE_NAME = 'xero.log';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        $this->logger = $context->getLogger();
        $this->scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
    }

    public function debug($msg)
    {
        if ($this->scopeConfig->isSetFlag('foomanconnect/settings/xerologenabled')) {
            $this->logger->debug($msg);
        }
    }

    /**
     * Respects the time zone.
     *
     * @param string $path
     * @param        $storeId
     * @param string $format
     *
     * @return bool|string
     */
    protected function getDateFromConfig($path, $storeId, $format)
    {
        $return = $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);

        return true === empty($return)
            ? false
            : $return;
    }

    /**
     * @param        $storeId
     * @param string $format
     *
     * @return bool|string
     */
    public function getOrderStartDate($storeId, $format = 'y-MM-dd')
    {
        return $this->getDateFromConfig('foomanconnect/order/startdate', $storeId, $format);
    }

    /**
     * @param        $storeId
     * @param string $format
     *
     * @return bool|string
     */
    public function getCreditMemoStartDate($storeId, $format = 'y-MM-dd')
    {
        return $this->getDateFromConfig('foomanconnect/creditmemo/startdate', $storeId, $format);
    }
}
