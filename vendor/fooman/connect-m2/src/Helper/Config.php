<?php

namespace Fooman\Connect\Helper;

use Magento\Framework\Encryption\Encryptor;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2013 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_CONNECT_SETTINGS = 'foomanconnect/settings/';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Fooman\Connect\Model\Tracking\RuleFactory
     */
    protected $connectTrackingRuleFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Encryptor
     */
    protected $encryptor;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Fooman\Connect\Model\Tracking\RuleFactory $connectTrackingRuleFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Encryption\Encryptor $encryptor
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->connectTrackingRuleFactory = $connectTrackingRuleFactory;
        $this->logger = $context->getLogger();
        $this->storeManager = $storeManager;
        $this->encryptor = $encryptor;
        parent::__construct($context);
    }

    /**
     * check if configuration parameters have been entered
     * minimum required Consumer Key, Consumer Secret and Private Key.
     *
     * @return bool
     */
    public function isConfigured()
    {
        $consumerkey = $this->encryptor->decrypt(
            $this->scopeConfig->getValue(
                'foomanconnect/settings/consumerkey',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
        );
        $consumersecret = $this->encryptor->decrypt(
            $this->scopeConfig->getValue(
                'foomanconnect/settings/consumersecret',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
        );
        $privatekey = $this->encryptor->decrypt(
            $this->scopeConfig->getValue(
                'foomanconnect/settings/privatekey',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
        );

        return (!empty($consumerkey) && !empty($consumersecret) && !empty($privatekey));
    }

    public function getTrackingCategory($storeId, $customerGroupId = false)
    {
        $data = [];
        $data['xeroTrackingCategoryID'] = '';
        $data['xeroTrackingName'] = '';
        $data['xeroTrackingOption'] = '';

        $xeroTracking = $this->scopeConfig->getValue(
            'foomanconnect/settings/xerotracking',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        if (!empty($xeroTracking)) {
            $xeroTracking = explode('|', $xeroTracking);
            $data['xeroTrackingCategoryID'] = $xeroTracking[0];
            $data['xeroTrackingName'] = $xeroTracking[1];
            $data['xeroTrackingOption'] = $xeroTracking[2];
        }

        if (false !== $customerGroupId) {
            $trackingRule = $this->connectTrackingRuleFactory->create()->loadCustomerGroupRule($customerGroupId);
            if ($trackingRule->getId() && $trackingRule->getTrackingCategoryId()) {
                $data['xeroTrackingCategoryID'] = $trackingRule->getTrackingCategoryId();
                $data['xeroTrackingName'] = $trackingRule->getTrackingName();
                $data['xeroTrackingOption'] = $trackingRule->getTrackingOption();
            }
        }

        return $data;
    }

    public function isInvoiceMode()
    {
        return $this->scopeConfig->getValue(
            'foomanconnect/order/exportmode'
        ) === \Fooman\Connect\Model\System\ExportMode::INVOICE_MODE;
    }
}
