<?php

namespace Fooman\Connect\Model;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Item extends AbstractModel
{
    const ITEM_CODE_MAX_LENGTH = 30;

    protected function _construct()
    {
        $this->_init('Fooman\Connect\Model\ResourceModel\Item');
    }

    public function ensureItemsExist($data, $storeId)
    {
        $this->queueSkus($data, $storeId);
        $this->exportItems($storeId);
    }

    public function queueSkus($data, $storeId)
    {
        foreach ($data['invoiceLines'] as $line) {
            if (isset($line['itemCode'])) {
                $this->load($line['itemCode']);
                if ($this->getXeroExportStatus() != \Fooman\Connect\Model\Status::EXPORTED) {
                    $this->setStoreId($storeId);
                    $this->setItemCode($line['itemCode']);
                    $this->setDescription($line['name']);
                    $this->save();
                }
            }
        }
    }

    public function exportItems($storeId, $all = false)
    {
        $collection = $this->getCollection();
        if (!$all) {
            $collection->getUnexportedItems();
        }
        if (count($collection)) {
            $data = $collection->toArray();
            try {
                $result = $this->sendToXero(
                    \Fooman\ConnectLicense\Model\DataSource\Converter\ItemsXml::convert($data['items']),
                    $storeId
                );
                foreach ($result['Items'] as $item) {
                    $this->unsetData();
                    $this->storedData = [];
                    $this->setItemCode($item['Code']);
                    $this->setDescription($item['Description']);
                    $this->setXeroItemId($item['ItemID']);
                    $this->setXeroExportStatus(\Fooman\Connect\Model\Status::EXPORTED);
                    $this->save();
                }
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
    }

    /**
     * @param $xml
     * @param $storeId
     *
     * @return array
     */
    public function sendToXero($xml, $storeId)
    {
        return $this->getApi()->setStoreId($storeId)->sendData(
            \Fooman\Connect\Model\Xero\Api::ITEMS,
            \Zend\Http\Request::METHOD_POST,
            $xml
        );
    }
}
