<?php

namespace Fooman\Connect\Model;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class TaxrateLinkRepository
{

    /**
     * @var ResourceModel\TaxrateLink
     */
    protected $resource;

    protected $taxrateLink;

    public function __construct(
        ResourceModel\TaxrateLink $taxrateLinkResource,
        TaxrateLink $taxrateLink
    ) {
        $this->resource = $taxrateLinkResource;
        $this->taxrateLink = $taxrateLink;
    }

    public function save(TaxrateLink $taxrateLink)
    {
        $this->resource->save($taxrateLink);
        return $taxrateLink;
    }

    public function get($id)
    {
        $this->resource->load($this->taxrateLink, $id);
        return $this->taxrateLink;
    }

    public function getByCode($code)
    {
        $this->resource->load($this->taxrateLink, $code, 'magento_code');
        return $this->taxrateLink;
    }
}
