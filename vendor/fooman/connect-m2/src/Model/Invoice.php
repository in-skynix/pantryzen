<?php

namespace Fooman\Connect\Model;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Invoice extends AbstractModel
{
    /**
     * @var \Magento\Sales\Model\Order\InvoiceFactory
     */
    protected $salesOrderInvoiceFactory;

    /**
     * @var \Fooman\Connect\Model\DataSource\InvoiceFactory
     */
    protected $connectDataSourceInvoiceFactory;

    /**
     * @var \Fooman\Connect\Model\ItemFactory
     */
    protected $connectItemFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Fooman\Connect\Model\Xero\Api $connectXeroApi,
        \Magento\Backend\Model\Auth\Session $backendAuthSession,
        \Fooman\Connect\Helper\Data $connectHelper,
        \Magento\Sales\Model\Order\InvoiceFactory $salesOrderInvoiceFactory,
        \Fooman\Connect\Model\DataSource\InvoiceFactory $connectDataSourceInvoiceFactory,
        \Fooman\Connect\Model\ItemFactory $connectItemFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->salesOrderInvoiceFactory = $salesOrderInvoiceFactory;
        $this->connectDataSourceInvoiceFactory = $connectDataSourceInvoiceFactory;
        $this->connectItemFactory = $connectItemFactory;
        $this->storeManager = $storeManager;

        parent::__construct(
            $context,
            $registry,
            $connectXeroApi,
            $backendAuthSession,
            $connectHelper,
            $scopeConfig,
            $resource,
            $resourceCollection,
            $data
        );
    }

    protected function _construct()
    {
        $this->_init('Fooman\Connect\Model\ResourceModel\Invoice');
    }

    /**
     * @param int $invoiceId
     *
     * @return array
     */
    public function exportByInvoiceId($invoiceId)
    {
        $invoice = $this->salesOrderInvoiceFactory->create()->load($invoiceId);

        return $this->exportOne($invoice);
    }

    /**
     * @param \Magento\Sales\Api\Data\InvoiceInterface $invoice
     *
     * @return array
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function neverExportOne(\Magento\Sales\Api\Data\InvoiceInterface $invoice)
    {
        $invoiceStatus = $this->load($invoice->getId(), 'invoice_id');
        $invoiceStatus->setInvoiceId($invoice->getId());

        if ($invoiceStatus->getXeroExportStatus() == \Fooman\Connect\Model\Status::EXPORTED) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('$1: ' . 'Has already been exported.', $invoice->getIncrementId())
            );
        }

        $invoiceStatus->setXeroExportStatus(\Fooman\Connect\Model\Status::WONT_EXPORT)->save();
    }


    /**
     * @param \Magento\Sales\Api\Data\InvoiceInterface $invoice
     *
     * @return array
     */
    public function exportOne(\Magento\Sales\Api\Data\InvoiceInterface $invoice)
    {
        $this->unsetData();
        $invoiceStatus = $this->load($invoice->getId(), 'invoice_id');
        if (!$invoiceStatus->getInvoiceId()) {
            $invoiceStatus->isObjectNew(true);
            $invoiceStatus->setInvoiceId((int) $invoice->getId());
        }
        if ($invoice->getBaseGrandTotal() == 0
            && !$this->scopeConfig->isSetFlag(
                'foomanconnect/order/exportzero',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $invoice->getStoreId()
            )
        ) {
            $invoice->setXeroExportStatus(\Fooman\Connect\Model\Status::WONT_EXPORT);
            $invoice->setXeroLastValidationErrors('');
            $invoice->save();
        } else {
            try {
                $dataSource = $this->connectDataSourceInvoiceFactory->create(['invoice' => $invoice]);
                $invoiceData = $dataSource->getInvoiceData();
                //$this->connectItemFactory->create()->ensureItemsExist($invoiceData, $invoice->getStoreId());
                $result = $this->sendToXero($dataSource->getXml($invoiceData), $invoice->getStoreId());
                $invoiceStatus->setXeroInvoiceId($result['Invoices'][0]['InvoiceID']);
                $invoiceStatus->setXeroInvoiceNumber($result['Invoices'][0]['InvoiceNumber']);
                $invoiceStatus->setXeroExportStatus(\Fooman\Connect\Model\Status::EXPORTED);
                $invoiceStatus->setXeroLastValidationErrors('');
                $invoiceStatus->save();

                return $result;
            } catch (\Fooman\Connect\Exception $e) {
                $this->_handleError($invoiceStatus, $e, $e->getXeroErrors(), $invoice, $invoiceData);
            } catch (\Exception $e) {
                $this->_handleError($invoiceStatus, $e, $e->getMessage(), $invoice, $invoiceData);
            }
        }
    }

    public function exportInvoices()
    {
        $stores = array_keys($this->storeManager->getStores());
        foreach ($stores as $storeId) {
            if ($this->scopeConfig->isSetFlag(
                'foomanconnect/cron/xeroautomatic',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            )) {
                $collection = $this->getCollection()->getUnexportedOrders($storeId)->setPageSize(self::PROCESS_PER_RUN);
                $collection->getSelect()->order('created_at DESC');
                $collection->addConfigDateFilter($storeId);
                foreach ($collection as $invoice) {
                    /* @var $invoice \Fooman\Connect\Model\Invoice */
                    $this->exportByInvoiceId($invoice->getEntityId());
                }
            }
        }
    }

    /**
     * @param $xml
     * @param $storeId
     *
     * @return array
     */
    public function sendToXero($xml, $storeId)
    {
        return $this->getApi()->setStoreId($storeId)->sendData(
            \Fooman\Connect\Model\Xero\Api::INVOICES_PATH,
            \Zend\Http\Request::METHOD_POST,
            $xml
        );
    }

    /**
     * @return bool|string
     */
    public function getSalesEntityViewId()
    {
        return $this->_getSalesEntityViewId('invoice');
    }
}
