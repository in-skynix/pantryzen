<?php

namespace Fooman\Connect\Model;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Creditmemo extends AbstractModel
{
    /**
     * @var \Magento\Sales\Model\Order\CreditmemoFactory
     */
    protected $salesOrderCreditmemoFactory;

    /**
     * @var \Fooman\Connect\Model\DataSource\CreditmemoFactory
     */
    protected $connectDataSourceCreditmemoFactory;

    /**
     * @var \Fooman\Connect\Model\ItemFactory
     */
    protected $connectItemFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Fooman\Connect\Model\Xero\Api $connectXeroApi,
        \Magento\Backend\Model\Auth\Session $backendAuthSession,
        \Fooman\Connect\Helper\Data $connectHelper,
        \Magento\Sales\Model\Order\CreditmemoFactory $salesOrderCreditmemoFactory,
        \Fooman\Connect\Model\DataSource\CreditmemoFactory $connectDataSourceCreditmemoFactory,
        \Fooman\Connect\Model\ItemFactory $connectItemFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->salesOrderCreditmemoFactory = $salesOrderCreditmemoFactory;
        $this->connectDataSourceCreditmemoFactory = $connectDataSourceCreditmemoFactory;
        $this->connectItemFactory = $connectItemFactory;
        $this->storeManager = $storeManager;

        parent::__construct(
            $context,
            $registry,
            $connectXeroApi,
            $backendAuthSession,
            $connectHelper,
            $scopeConfig,
            $resource,
            $resourceCollection,
            $data
        );
    }

    protected function _construct()
    {
        $this->_init('Fooman\Connect\Model\ResourceModel\Creditmemo');
    }

    /**
     * @param int $creditmemoId
     *
     * @return array
     */
    public function exportByCreditmemoId($creditmemoId)
    {
        $creditmemo = $this->salesOrderCreditmemoFactory->create()->load($creditmemoId);

        return $this->exportOne($creditmemo);
    }

    /**
     * @param \Magento\Sales\Api\Data\CreditmemoInterface $creditmemo
     *
     * @return array
     */
    public function exportOne(\Magento\Sales\Api\Data\CreditmemoInterface $creditmemo)
    {
        $this->unsetData();
        $creditmemoStatus = $this->load($creditmemo->getId(), 'creditmemo_id');
        if (!$creditmemoStatus->getCreditmemoId()) {
            $creditmemoStatus->isObjectNew(true);
            $creditmemoStatus->setCreditmemoId((int) $creditmemo->getId());
        }
        if (0 == $creditmemo->getBaseGrandTotal()) {
            $creditmemoStatus->setXeroExportStatus(\Fooman\Connect\Model\Status::WONT_EXPORT);
            $creditmemoStatus->setXeroLastValidationErrors('');
            $creditmemoStatus->save();
        } else {
            try {
                $creditmemoData = [];
                /** @var \Fooman\Connect\Model\DataSource\Creditmemo $dataSource */
                $dataSource = $this->connectDataSourceCreditmemoFactory->create(['creditmemo' => $creditmemo]);
                $creditmemoData = $dataSource->getCreditmemoData();
                //$this->connectItemFactory->create()->ensureItemsExist($creditmemoData, $creditmemo->getStoreId());
                $result = $this->sendToXero($dataSource->getXml($creditmemoData), $creditmemo->getStoreId());
                $creditmemoStatus->setXeroCreditnoteId($result['CreditNotes'][0]['CreditNoteID']);
                $creditmemoStatus->setXeroCreditnoteNumber($result['CreditNotes'][0]['CreditNoteNumber']);
                $creditmemoStatus->setXeroExportStatus(\Fooman\Connect\Model\Status::EXPORTED);
                $creditmemoStatus->setXeroLastValidationErrors('');
                $creditmemoStatus->save();
                if ($this->scopeConfig->isSetFlag(
                    'foomanconnect/creditmemo/cashrefund',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $creditmemo->getStoreId()
                )
                ) {
                    $data = [];
                    $data['xero_id'] = $creditmemoStatus->getXeroCreditnoteId();
                    $data['account_id'] = $this->scopeConfig->getValue(
                        'foomanconnect/creditmemo/cashrefundbankaccount',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                        $creditmemo->getStoreId()
                    );
                    $data['date'] = $creditmemoData['createdAt'];
                    $data['amount'] = $creditmemoData['grandTotal'];
                    $this->sendPaymentToXero(
                        \Fooman\ConnectLicense\Model\DataSource\Converter\PaymentsXml::convert(
                            ['CreditNotes' => [$data]]
                        ),
                        $creditmemo->getStoreId()
                    );
                }

                return $result;
            } catch (\Fooman\Connect\Exception $e) {
                $this->_handleError($creditmemoStatus, $e, $e->getXeroErrors(), $creditmemo, $creditmemoData);
            } catch (\Exception $e) {
                $this->_handleError($creditmemoStatus, $e, $e->getMessage(), $creditmemo, $creditmemoData);
            }
        }
    }

    public function neverExportOne(\Magento\Sales\Api\Data\CreditmemoInterface $creditmemo)
    {
        $status = $this->load($creditmemo->getId(), 'creditmemo_id');
        $status->setCreditmemoId($creditmemo->getId());

        if ($status->getXeroExportStatus() == \Fooman\Connect\Model\Status::EXPORTED) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('$1: ' . 'Has already been exported.', $creditmemo->getIncrementId())
            );
        }

        $status->setXeroExportStatus(\Fooman\Connect\Model\Status::WONT_EXPORT)->save();
    }

    public function exportCreditmemos()
    {
        $stores = array_keys($this->storeManager->getStores());
        foreach ($stores as $storeId) {
            if ($this->scopeConfig->isSetFlag(
                'foomanconnect/cron/xeroautomatic',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            )
            ) {
                /** @var \Fooman\Connect\Model\ResourceModel\Creditmemo\Collection $collection */
                $collection = $this->getCollection()->getUnexportedCreditmemos($storeId)->setPageSize(
                    self::PROCESS_PER_RUN
                );
                $collection->getSelect()->order('created_at DESC');
                $collection->addConfigDateFilter($storeId);
                foreach ($collection as $creditmemo) {
                    /* @var $creditmemo \Fooman\Connect\Model\Creditmemo */
                    $this->exportByCreditmemoId($creditmemo->getEntityId());
                }
            }
        }
    }

    /**
     * @param $xml
     * @param $storeId
     *
     * @return array
     */
    public function sendToXero($xml, $storeId)
    {
        return $this->getApi()->setStoreId($storeId)->sendData(
            \Fooman\Connect\Model\Xero\Api::CREDITNOTES_PATH,
            \Zend\Http\Request::METHOD_POST,
            $xml
        );
    }

    /**
     * @return bool|string
     */
    public function getSalesEntityViewId()
    {
        return $this->_getSalesEntityViewId('creditmemo');
    }

    /**
     * @param $xml
     * @param $storeId
     *
     * @return array
     */
    public function sendPaymentToXero($xml, $storeId)
    {
        return $this->getApi()->setStoreId($storeId)->sendData(
            \Fooman\Connect\Model\Xero\Api::PAYMENTS_PATH,
            \Zend\Http\Request::METHOD_PUT,
            $xml
        );
    }
}
