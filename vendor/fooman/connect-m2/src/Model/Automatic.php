<?php

namespace Fooman\Connect\Model;

use \Fooman\Connect\Model\System\ExportMode;
/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Automatic
{
    /**
     * @var \Fooman\Connect\Helper\Config
     */
    protected $connectConfigHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Fooman\Connect\Model\OrderFactory
     */
    protected $connectOrderFactory;

    /**
     * @var \Fooman\Connect\Model\InvoiceFactory
     */
    protected $connectInvoiceFactory;

    /**
     * @var \Fooman\Connect\Model\CreditmemoFactory
     */
    protected $connectCreditmemoFactory;

    public function __construct(
        \Fooman\Connect\Helper\Config $connectConfigHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Fooman\Connect\Model\OrderFactory $connectOrderFactory,
        \Fooman\Connect\Model\InvoiceFactory $connectInvoiceFactory,
        \Fooman\Connect\Model\CreditmemoFactory $connectCreditmemoFactory
    ) {
        $this->connectConfigHelper = $connectConfigHelper;
        $this->scopeConfig = $scopeConfig;
        $this->connectOrderFactory = $connectOrderFactory;
        $this->connectInvoiceFactory = $connectInvoiceFactory;
        $this->connectCreditmemoFactory = $connectCreditmemoFactory;
    }
    public function cron()
    {
        $cronEnabled = $this->scopeConfig->isSetFlag('foomanconnect/cron/xeroautomatic');
        $isConfigured = $this->connectConfigHelper->isConfigured();

        if ($cronEnabled && $isConfigured) {
            if ($this->scopeConfig->getValue('foomanconnect/order/exportmode') === ExportMode::ORDER_MODE) {
                $this->connectOrderFactory->create()->exportOrders();
            } else {
                $this->connectInvoiceFactory->create()->exportInvoices();
            }
            $this->connectCreditmemoFactory->create()->exportCreditmemos();
        }
    }
}
