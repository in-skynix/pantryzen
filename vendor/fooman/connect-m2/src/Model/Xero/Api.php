<?php

namespace Fooman\Connect\Model\Xero;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Api
{
    const URL_ROOT = 'https://api.xero.com';
    const API_VERSION = 'version/2';

    //Entry points for Fooman Connect: Xero
    const BASE_URL = '/api.xro/2.0/';
    const ORGANISATION_PATH = 'Organisation';
    const INVOICE_PATH = 'Invoice';
    const INVOICES_PATH = 'Invoices';
    const CREDITNOTES_PATH = 'CreditNotes';
    const PAYMENTS_PATH = 'Payments';
    const CONTACTS_PATH = 'Contacts';
    const TRACKING_PATH = 'TrackingCategories';
    const ACCOUNTS_PATH = 'Accounts';
    const TAXRATES_PATH = 'TaxRates';
    const ITEMS = 'Items';
    const XERO_INVOICE_LINK = 'https://go.xero.com/AccountsReceivable/View.aspx?InvoiceID=';
    const XERO_CREDITNOTE_LINK = 'https://go.xero.com/AccountsReceivable/ViewCreditNote.aspx?creditNoteID=';

    const LINEITEMS_TAX_INCL = 'Inclusive';
    const LINEITEMS_TAX_EXCL = 'Exclusive';

    const USER_AGENT = 'Fooman - Magento';

    protected $clients = [];
    protected $storeId = \Magento\Store\Model\Store::DEFAULT_STORE_ID;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;


    /**
     * @var \Fooman\Connect\Helper\Data
     */
    protected $connectHelper;

    /**
     * @var \Fooman\Connect\Model\KeyManagerFactory
     */
    protected $keyManagerFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Fooman\Connect\Helper\Data $connectHelper,
        \Fooman\Connect\Model\KeyManagerFactory $keyManagerFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->connectHelper = $connectHelper;
        $this->keyManagerFactory = $keyManagerFactory;
    }

    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;

        return $this;
    }

    public function getStoreId()
    {
        return $this->storeId;
    }

    /**
     * Get configuration settings to work with Xero's Oauth.
     *
     * @param   $storeId
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getConfiguration($storeId)
    {
        try {
            $keyManager = $this->keyManagerFactory->create(['storeId' => $storeId]);
            $rsaPrivateKey = new \Zend_Crypt_Rsa_Key_Private(
                $keyManager->getPrivateKey(),
                $keyManager->getPrivateKeyPassword()
            );
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Private Key error: ' . $e->getMessage() . openssl_error_string())
            );
        }

        return [
            'siteUrl'         => self::URL_ROOT,
            'signatureMethod' => 'RSA-SHA1',
            'consumerKey'     => $keyManager->getConsumerKey(),
            'consumerSecret'  => $keyManager->getConsumerSecret(),
            'requestTokenUrl' => self::URL_ROOT . '/oauth/RequestToken',
            'accessTokenUrl'  => self::URL_ROOT . '/oauth/AccessToken',
            'authorizeUrl'    => self::URL_ROOT . '/oauth/Authorize',
            'rsaPrivateKey'   => $rsaPrivateKey,
        ];
    }

    /**
     * @param int $storeId
     *
     * @return \Zend_Oauth_Client
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Http_Client_Exception
     */
    public function getClientForStore($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->getStoreId();
        }

        if (!isset($this->clients[$storeId])) {
            try {
                $configuration = $this->getConfiguration($storeId);
                $xeroAccessToken = new \Zend_Oauth_Token_Access();
                $xeroAccessToken->setToken($configuration['consumerKey']);
                $xeroAccessToken->setTokenSecret($configuration['consumerSecret']);
                $this->clients[$storeId] = $xeroAccessToken
                    ->getHttpClient($configuration, null, ['useragent' => self::USER_AGENT])
                    ->setHeaders('Accept', 'application/json');
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
            }
        }

        return $this->clients[$storeId];
    }

    /**
     * construct complete URL from given entrypoint.
     *
     * @param string $endpoint
     *
     * @return string
     */
    public function getApiUrl($endpoint = '')
    {
        return self::URL_ROOT . self::BASE_URL . $endpoint;
    }

    public function sendData($entryPoint, $method = \Zend\Http\Request::METHOD_GET, $data = false)
    {
        $client = $this->getClientForStore()->resetParameters();
        if ($data && ($method == \Zend\Http\Request::METHOD_POST || $method == \Zend\Http\Request::METHOD_PUT)) {
            $client->setParameterPost('xml', $data);
        }
        //turn off acceptance of gzipped content as some servers might not be able to unzip
        //$client->setHeaders('Accept-encoding', 'identity');
        $client->setUri($this->getApiUrl($entryPoint));
        $response = $client->request($method);

        return $this->handleResponse($response);
    }

    public function getInvoice($xeroInvoiceId)
    {
        $result = $this->sendData(self::INVOICES_PATH . '/' . $xeroInvoiceId);

        return $result;
    }

    public function getPaymentsForInvoice($xeroInvoiceId)
    {
        $invoice = $this->getInvoice($xeroInvoiceId);
        $returnArray = [];

        if ($invoice) {
            if (isset($invoice['Invoices'][0]['Payments'])) {
                foreach ($invoice['Invoices'][0]['Payments'] as $payment) {
                    $returnArray['payments'][] = [
                        'date'   => $this->parseDateAndConvertToUtcTimestamp($payment['Date']),
                        'amount' => $payment['Amount'],
                    ];
                }
            }
            $returnArray['amountDue'] = $invoice['Invoices'][0]['AmountDue'];
        }
        return $returnArray;
    }

    private function parseDateAndConvertToUtcTimestamp($input)
    {
        if (preg_match('/\/Date\((\d+)([-+])(\d+)\)\//', $input, $date)) {
            $timestamp = $date[1] / 1000;
            if ($date[2] == '+') {
                $timestamp += $date[3] * 36;
            } else {
                $timestamp -= $date[3] * 36;
            }
        } else {
            preg_match('/\/Date\((\d+)\)\//', $input, $date);
            $timestamp = $date[1] / 1000;
        }

        return $timestamp;
    }

    public function getAccounts()
    {
        $result = $this->sendData(self::ACCOUNTS_PATH);
        if (isset($result['Accounts'])) {
            return $result['Accounts'];
        }
    }

    public function getOrganisations()
    {
        $result = $this->sendData(self::ORGANISATION_PATH);
        if (isset($result['Organisations'])) {
            return $result['Organisations'];
        }
    }

    public function getOrganisation()
    {
        $result = $this->getOrganisations();
        if (isset($result['Organisation'])) {
            return $result['Organisation'];
        }
        //return value contradicts Xero documentation - should be $result['Organisation']
        if (isset($result['0'])) {
            return $result['0'];
        }
    }

    public function getTrackingCategories()
    {
        $result = $this->sendData(self::TRACKING_PATH);
        if (isset($result['TrackingCategories'])) {
            return $result['TrackingCategories'];
        }
    }

    public function getTaxRates()
    {
        $result = $this->sendData(self::TAXRATES_PATH);
        if (isset($result['TaxRates'])) {
            return $result['TaxRates'];
        }
    }

    /**
     * error checking of response returned by server.
     *
     * @param null $response
     *
     * @return mixed
     *
     * @throws \Fooman\Connect\Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function handleResponse($response = null)
    {
        if ($response === null) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Empty Response. Please check your settings.')
            );
        }

        if (!$response instanceof \Zend_Http_Response) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Wrong Response. Please check your settings.')
            );
        }
        $responseBody = $response->getBody();
        if (!$responseBody) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Please use a valid ApiKey and Save Config'));
        }
        if (strpos($responseBody, 'oauth_problem') === 0) {
            foreach (explode('&', $responseBody) as $item) {
                if (strpos($item, 'oauth_problem_advice=') === 0) {
                    $errorMessage = urldecode(substr($item, strlen('oauth_problem_advice=')));
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __('Xero Oauth error: %1', $errorMessage)
                    );
                }
            }

            throw new \Magento\Framework\Exception\LocalizedException(
                __('Unknown Xero Oauth error: %1', $responseBody)
            );
        }
        try {
            $result = json_decode($responseBody, true);
        } catch (\Exception $e) {
            $this->connectHelper->debug($responseBody);
            $this->connectHelper->debug($e->getMessage());
            throw new \Magento\Framework\Exception\LocalizedException(__('Result is not a valid response.'));
        }

        if (isset($result['ErrorNumber'])) {
            $this->connectHelper->debug($result);
            $collectedErrors = $this->_getErrorsFromResult($result);
            $exception = new \Fooman\Connect\Exception($result['Message']);
            if ($collectedErrors) {
                $exception->setXeroErrors($collectedErrors);
            }
            throw $exception;
        }

        return $result;
    }

    protected function _getErrorsFromResult($result)
    {
        $res = [];
        $res[] = $result['Message'];
        if (isset($result['Elements'])) {
            foreach ($result['Elements'] as $elements) {
                if (isset($elements['ValidationErrors'])) {
                    foreach ($elements['ValidationErrors'] as $error) {
                        $res[] = $error['Message'];
                    }
                }
            }
        }

        return $res;
    }
}
