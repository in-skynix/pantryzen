<?php

namespace Fooman\Connect\Model\System;

use \Fooman\Connect\Model\Xero\Defaults as XeroDefaults;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class XeroVersionsOptions extends AbstractSystem
{

    public function __construct(
        \Fooman\Connect\Helper\Config $connectConfigHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Fooman\Connect\Model\Xero\Api $connectXeroApi
    ) {
        parent::__construct($connectConfigHelper);
        $this->scopeConfig = $scopeConfig;
        $this->connectXeroApi = $connectXeroApi;
    }

    public function toOptionArray()
    {

        $storeId = $this->getCurrentStoreId();
        if ($this->isConfigured()
            && $this->scopeConfig->getValue(
                'foomanconnect/settings/xeroenabled',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            )
        ) {
            $this->connectXeroApi->setStoreId($storeId);
            try {
                $version = strtolower($this->connectXeroApi->getOrganisation()['Version']);
            } catch (\Exception $e) {
                //display the error message in the dropdown
                return ['value' => '', 'label' => $e->getMessage()];
            }

            if (isset(XeroDefaults::$taxRates[$version])) {
                $returnArray[] = [
                    'value' => $version,
                    'label' => XeroDefaults::$taxRates[$version]['name']
                ];
            } else {
                foreach (XeroDefaults::$taxRates as $version) {
                    $returnArray[] = ['value' => $version['code'], 'label' => $version['name']];
                }
            }
        } else {
            $returnArray[] = [
                'value' => '0',
                'label' => __('Please configure and enable the integration above and save config.')
            ];
        }

        return $returnArray;
    }


}
