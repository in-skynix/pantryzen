<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class TrackingOptions extends AbstractSystem
{
    const XERO_TRACKING_REGISTRY_KEY = 'xero-tracking';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Fooman\Connect\Model\Xero\Api
     */
    protected $connectXeroApi;

    public function __construct(
        \Fooman\Connect\Helper\Config $connectConfigHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Registry $registry,
        \Fooman\Connect\Model\Xero\Api $connectXeroApi
    ) {
        parent::__construct($connectConfigHelper);
        $this->scopeConfig = $scopeConfig;
        $this->registry = $registry;
        $this->connectXeroApi = $connectXeroApi;
    }

    public function toOptionArray()
    {
        $this->toOptionArraySafe();
    }

    public function toOptionArraySafe($useDefaultLabel = false)
    {
        $returnArray = [];

        try {
            if ($useDefaultLabel) {
                $returnArray[] = [
                    'value' => '0',
                    'label' => __('Use Store Default')
                ];
            } else {
                $returnArray[] = [
                    'value' => '',
                    'label' => __('None')
                ];
            }

            $trackingCategories = $this->getXeroTracking();
            foreach ($trackingCategories as $category) {
                foreach ($category['Options'] as $option) {
                    $returnArray[] = [
                        'value' => $category['TrackingCategoryID'].'|'.$category['Name'].'|'.$option['Name'],
                        'label' => '['.$category['Name'].'] '.$option['Name']
                    ];
                }
            }
        } catch (\Exception $e) {
            //display the error message in the dropdown
            $returnArray[] = ['value' => '0', 'label' => $e->getMessage()];
        }

        return $returnArray;
    }

    public function getXeroTracking($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->getCurrentStoreId();
        }
        if ($this->isConfigured() && $this->scopeConfig->getValue(
            'foomanconnect/settings/xeroenabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        )) {
            $result = $this->registry->registry(self::XERO_TRACKING_REGISTRY_KEY);
            if (!$result) {
                $this->connectXeroApi->setStoreId($storeId);
                $result = $this->connectXeroApi->getTrackingCategories();
                $this->registry->register(self::XERO_TRACKING_REGISTRY_KEY, $result);
            }

            return $result;
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Please configure and enable the integration above and save config.')
            );
        }
    }
}
