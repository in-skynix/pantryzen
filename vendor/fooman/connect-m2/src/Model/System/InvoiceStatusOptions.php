<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class InvoiceStatusOptions implements \Magento\Framework\Data\OptionSourceInterface
{
    public function toOptionArray()
    {
        $returnArray = [];
        foreach (\Magento\Sales\Model\Order\Invoice::getStates() as $status => $statusLabel) {
            $returnArray[] = ['value' => $status, 'label' => $statusLabel];
        }

        return $returnArray;
    }
}
