<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class RoundingAccountOptions extends AbstractAccounts
{
    public function toOptionArray()
    {
        $returnArray = [];
        try {
            $accounts = $this->getXeroAccounts();
            $wantedAccountTypes = ['ROUNDING'];

            foreach ($accounts as $account) {
                if (isset($account['SystemAccount']) && in_array($account['SystemAccount'], $wantedAccountTypes)) {
                    $returnArray[] = [
                        'value' => $account['Code'],
                        'label' => '['.$account['Code'].'] '.substr($account['Name'], 0, 30),
                    ];
                }
            }
        } catch (\Exception $e) {
            $returnArray[] = ['value' => '0', 'label' => $e->getMessage()];
        }

        return $returnArray;
    }
}
