<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class InitialInvoiceStatus implements \Magento\Framework\Data\OptionSourceInterface
{
    const DRAFT = 'DRAFT';
    const AUTHORISED = 'AUTHORISED';

    public function toOptionArray()
    {
        $returnArray = [];
        $returnArray[] = ['value' => self::DRAFT, 'label' => __('Draft')];
        $returnArray[] = ['value' => self::AUTHORISED, 'label' => __('Authorised')];

        return $returnArray;
    }
}
