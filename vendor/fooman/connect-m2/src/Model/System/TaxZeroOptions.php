<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class TaxZeroOptions extends TaxOptions
{

    public function toOptionArray()
    {
        $this->getOptions();
    }

    /**
     * @param null $storeId
     *
     * @return array
     */
    public function getOptions($storeId = null)
    {
        $returnArray = [];

        if (null === $storeId) {
            $storeId = $this->getCurrentStoreId();
        }
        if ($this->isConfigured()
            && $this->scopeConfig->getValue(
                'foomanconnect/settings/xeroenabled',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            )
        ) {
            $returnArray[] = ['value' => '', 'label' => ''];

            $result = $this->registry->registry(self::XERO_TAX_RATES_REGISTRY_KEY);
            if (!$result) {
                try {
                    $this->connectXeroApi->setStoreId($storeId);
                    $result = $this->connectXeroApi->getTaxRates();
                    $this->registry->register(self::XERO_TAX_RATES_REGISTRY_KEY, $result);
                } catch (\Exception $e) {
                    //display the error message in the dropdown
                    $returnArray[] = ['value' => '', 'label' => $e->getMessage()];

                    return $returnArray;
                }
            }

            //we have been successful
            foreach ($result as $taxRate) {
                if ($taxRate['EffectiveRate'] == 0 && $taxRate['Status'] == TaxOptions::XERO_TAX_ACTIVE) {
                    $returnArray[] = [
                        'value' => $taxRate['TaxType'],
                        'label' => substr($taxRate['Name'], 0, 30) . ' [' . $taxRate['EffectiveRate'] . '%]'
                    ];
                }
            }
        } else {
            $returnArray[] = [
                'value' => '',
                'label' => __(
                    'Please configure and enable the integration above and save config.'
                )
            ];
        }

        return $returnArray;
    }
}
