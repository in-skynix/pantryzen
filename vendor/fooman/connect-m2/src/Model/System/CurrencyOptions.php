<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class CurrencyOptions implements \Magento\Framework\Data\OptionSourceInterface
{
    const TRANSFER_BASE = 'base';
    const TRANSFER_ORDER = 'order';

    public function toOptionArray()
    {
        $returnArray = [];
        $returnArray[] = ['value' => self::TRANSFER_BASE, 'label' => __('Store Base Currency')];
        $returnArray[] = ['value' => self::TRANSFER_ORDER, 'label' => __('Order Currency')];

        return $returnArray;
    }
}
