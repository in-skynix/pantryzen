<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

abstract class AbstractSystem implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Fooman\Connect\Helper\Config
     */
    protected $connectConfigHelper;

    public function __construct(
        \Fooman\Connect\Helper\Config $connectConfigHelper
    ) {
        $this->connectConfigHelper = $connectConfigHelper;
    }

    public function isConfigured()
    {
        return $this->connectConfigHelper->isConfigured();
    }

    public function getCurrentStoreId()
    {
        /* TODO work this out from config or context model to support multi store configs
        \Magento\Config\Model\Config $configConfig
         */
        /*if (strlen($code = $this->configConfig->getStore())) {
            $storeId = $this->storeStoreFactory->create()->load($code)->getId();
        } elseif (strlen($code = $this->configConfig->getWebsite())) {
            $websiteId = $this->storeWebsiteFactory->create()->load($code)->getId();
            $storeId = $this->storeManager->getWebsite($websiteId)->getDefaultStore()->getId();
        } else {
            $storeId = \Magento\Store\Model\Store::DEFAULT_STORE_ID;
        }*/

        $storeId = \Magento\Store\Model\Store::DEFAULT_STORE_ID;

        return $storeId;
    }
}
