<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class TaxOptions extends AbstractSystem
{
    const XERO_TAX_ACTIVE = 'ACTIVE';
    const XERO_TAX_RATES_REGISTRY_KEY = 'xero-tax-rates';
    const USE_ITEM_TAX_TYPE = 'fooman-use-items-tax-type';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Fooman\Connect\Model\Xero\Api
     */
    protected $connectXeroApi;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Fooman\Connect\Helper\Config $connectConfigHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Registry $registry,
        \Fooman\Connect\Model\Xero\Api $connectXeroApi,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($connectConfigHelper);
        $this->scopeConfig = $scopeConfig;
        $this->registry = $registry;
        $this->connectXeroApi = $connectXeroApi;
        $this->storeManager = $storeManager;
    }


    public function toOptionArray()
    {
        $this->toOptionArraySafe();
    }

    /**
     * $output = null return array of tax rates from xero for back-end select drop down
     * $output = 'XERO_TAX_RATE_IDENTIFIER' return effective tax rate percentage
     * $output not null return array of tax rates keyed by effective tax rates
     * Note the last is used during fallback when no tax rate is present, has the
     * potential to return the wrong one when using multiple rates within Xero with the same percentage.
     *
     * @param      $output
     * @param null $storeId
     * @param bool $includeItemOption
     *
     * @return array | float
     */
    public function toOptionArraySafe($output = null, $storeId = null, $includeItemOption = false)
    {
        $returnArray = [];
        if (null === $storeId) {
            $storeId = $this->getCurrentStoreId();
        }
        if ($this->isConfigured()
            && $this->scopeConfig->getValue(
                'foomanconnect/settings/xeroenabled',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            )
        ) {
            if (empty($output)) {
                $returnArray[] = ['value' => '', 'label' => ''];
                if ($includeItemOption) {
                    $returnArray[] = [
                        'value' => self::USE_ITEM_TAX_TYPE,
                        'label' => __('Use Items\'s Tax Rate'),
                    ];
                }
            }

            $result = $this->registry->registry($this->getRegistryKey($storeId));
            if (!$result) {
                try {
                    $this->connectXeroApi->setStoreId($storeId);
                    $result = $this->connectXeroApi->getTaxRates();
                    $this->registry->register($this->getRegistryKey($storeId), $result);
                } catch (\Exception $e) {
                    //display the error message in the dropdown
                    return ['value' => '', 'label' => $e->getMessage()];
                }
            }

            //we have been successful
            foreach ($result as $taxRate) {
                if ($taxRate['Status'] == self::XERO_TAX_ACTIVE) {
                    if (empty($output)) {
                        $returnArray[] = [
                            'value' => $taxRate['TaxType'],
                            'label' => substr($taxRate['Name'], 0, 30) . ' [' . $taxRate['EffectiveRate'] . '%]'
                        ];
                    } elseif ($output == $taxRate['TaxType']) {
                        return $taxRate['EffectiveRate'];
                    } elseif ($taxRate['CanApplyToRevenue']) {
                        $returnArray[sprintf('%01.4f', $taxRate['EffectiveRate'])] = $taxRate['TaxType'];
                    }
                }
            }
        } else {
            $returnArray[] = [
                'value' => '',
                'label' => __(
                    'Please configure and enable the integration above and save config.'
                ),
            ];
        }

        return $returnArray;
    }

    public function getRegistryKey($storeId)
    {
        return self::XERO_TAX_RATES_REGISTRY_KEY . $storeId;
    }

    public function getTaxRatesForAllStores()
    {
        $resultArray = [];
        $stores = $this->storeManager->getStores();
        foreach ($stores as $store) {
            $storeTaxes = $this->toOptionArraySafe(false, $store->getId());
            array_shift($storeTaxes); //removes the empty entry from the beginning
            $resultArray = array_merge($resultArray, $storeTaxes);
        }

        return $resultArray;
    }

    public function getTaxPercentageForRate($taxType, $storeId)
    {
        return $this->toOptionArraySafe($taxType, $storeId);
    }
}
