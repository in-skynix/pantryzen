<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class ExportMode implements \Magento\Framework\Data\OptionSourceInterface
{
    const ORDER_MODE = 'order';
    const INVOICE_MODE = 'invoice';

    public function toOptionArray()
    {
        $returnArray = [];
        $returnArray[] = [
            'value' => self::ORDER_MODE,
            'label' => __('Magento Order to Xero Invoice'),
        ];
        $returnArray[] = [
            'value' => self::INVOICE_MODE,
            'label' => __('Magento Invoice to Xero Invoice'),
        ];

        return $returnArray;
    }
}
