<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class OrderStatusOptions implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Magento\Sales\Model\Order\ConfigFactory
     */
    protected $salesOrderConfigFactory;

    public function __construct(
        \Magento\Sales\Model\Order\ConfigFactory $salesOrderConfigFactory
    ) {
        $this->salesOrderConfigFactory = $salesOrderConfigFactory;
    }
    public function toOptionArray()
    {
        $returnArray = [];
        foreach ($this->salesOrderConfigFactory->create()->getStatuses() as $status => $statusLabel) {
            $returnArray[] = ['value' => $status, 'label' => $statusLabel];
        }

        return $returnArray;
    }
}
