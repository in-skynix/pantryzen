<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

abstract class AbstractAccounts extends AbstractSystem
{
    const XERO_ACCOUNTS_REGISTRY_KEY = 'xero-accounts';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Fooman\Connect\Model\Xero\Api
     */
    protected $connectXeroApi;

    public function __construct(
        \Fooman\Connect\Helper\Config $connectConfigHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Registry $registry,
        \Fooman\Connect\Model\Xero\Api $connectXeroApi
    ) {
        parent::__construct($connectConfigHelper);
        $this->scopeConfig = $scopeConfig;
        $this->registry = $registry;
        $this->connectXeroApi = $connectXeroApi;
    }

    public function getXeroAccounts()
    {
        $storeId = $this->getCurrentStoreId();
        if ($this->isConfigured()
            && $this->scopeConfig->getValue(
                'foomanconnect/settings/xeroenabled',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            )
        ) {
            $result = $this->registry->registry($this->getRegistryKey($storeId));
            if (!$result) {
                $this->connectXeroApi->setStoreId($storeId);
                $result = $this->connectXeroApi->getAccounts();
                $this->registry->register($this->getRegistryKey($storeId), $result);
            }

            return $result;
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Please configure and enable the integration above and save config.')
            );
        }
    }

    public function getRegistryKey($storeId)
    {
        return self::XERO_ACCOUNTS_REGISTRY_KEY.$storeId;
    }
}
