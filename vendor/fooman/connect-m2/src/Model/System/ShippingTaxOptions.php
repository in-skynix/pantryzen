<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class ShippingTaxOptions extends TaxOptions
{
    public function toOptionArraySafe($output = null, $storeId = null, $includeItemOption = true)
    {
        return parent::toOptionArraySafe($output, $storeId, $includeItemOption);
    }
}
