<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class SalesProductAccountOptions extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @var \Fooman\Connect\Model\System\SalesAccountOptionsFactory
     */
    protected $connectSystemSalesAccountOptionsFactory;

    public function __construct(
        \Fooman\Connect\Model\System\SalesAccountOptionsFactory $connectSystemSalesAccountOptionsFactory
    ) {
        $this->connectSystemSalesAccountOptionsFactory = $connectSystemSalesAccountOptionsFactory;
    }
    public function getAllOptions()
    {
        return $this->connectSystemSalesAccountOptionsFactory->create()->toOptionArraySafe(true, false);
    }
}
