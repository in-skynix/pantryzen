<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class TaxOverrideOptions implements \Magento\Framework\Data\OptionSourceInterface
{
    const MAGE_CALC = 'mage';
    const XERO_CALC = 'xero';
    const MIXED_CALC = 'mixed';
    const XERO_REDUCED = 'reduced';

    public function toOptionArray()
    {
        $returnArray = [];
        //$returnArray[] = ['value' => self::MAGE_CALC, 'label' => __('Magento calculated')];
        $returnArray[] = ['value' => self::MIXED_CALC, 'label' => __('Magento re-calculated')];
        $returnArray[] = ['value' => self::XERO_REDUCED, 'label' => __('Magento merged')];
        $returnArray[] = ['value' => self::XERO_CALC, 'label' => __('Xero re-calculated')];

        return $returnArray;
    }
}
