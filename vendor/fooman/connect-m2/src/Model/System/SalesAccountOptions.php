<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class SalesAccountOptions extends AbstractAccounts
{
    public function toOptionArray()
    {
        return $this->toOptionArraySafe();
    }

    public function toOptionArraySafe($includeGlobal = false, $humanLabel = true)
    {
        $returnArray = [];
        try {
            $accounts = $this->getXeroAccounts();
            $wantedAccountTypes = ['REVENUE', 'SALES'];
            if ($includeGlobal) {
                $returnArray[] = [
                    'value' => '0',
                    'label' => __('Use Global Setting'),
                ];
            }
            foreach ($accounts as $account) {
                if (in_array($account['Type'], $wantedAccountTypes)) {
                    if ($humanLabel) {
                        $returnArray[] = [
                            'value' => $account['Code'],
                            'label' => '['.$account['Code'].'] '.substr($account['Name'], 0, 30)
                        ];
                    } else {
                        $returnArray[] = [
                            'value' => $account['Code'],
                            'label' => $account['Code'],
                        ];
                    }
                }
            }
        } catch (\Exception $e) {
            $returnArray[] = ['value' => '0', 'label' => $e->getMessage()];
        }

        return $returnArray;
    }
}
