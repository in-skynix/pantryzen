<?php

namespace Fooman\Connect\Model\System;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class BankAccountOptions extends AbstractAccounts
{
    public function toOptionArray()
    {
        $returnArray = [];
        try {
            $accounts = $this->getXeroAccounts();
            $wantedAccountTypes = ['BANK'];
            foreach ($accounts as $account) {
                if (in_array($account['Type'], $wantedAccountTypes)) {
                    $returnArray[] = [
                        'value' => $account['AccountID'],
                        'label' => '['.$account['BankAccountNumber'].'] '.substr($account['Name'], 0, 30),
                    ];
                }
            }
        } catch (\Exception $e) {
            $returnArray[] = ['value' => '0', 'label' => $e->getMessage()];
        }

        return $returnArray;
    }
}
