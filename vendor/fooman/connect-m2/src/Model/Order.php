<?php

namespace Fooman\Connect\Model;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Order extends AbstractModel
{
    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $salesOrderFactory;

    /**
     * @var \Fooman\Connect\Model\DataSource\OrderFactory
     */
    protected $connectDataSourceOrderFactory;

    /**
     * @var \Fooman\Connect\Model\ItemFactory
     */
    protected $connectItemFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Fooman\Connect\Model\Xero\Api $connectXeroApi,
        \Magento\Backend\Model\Auth\Session $backendAuthSession,
        \Fooman\Connect\Helper\Data $connectHelper,
        \Magento\Sales\Model\OrderFactory $salesOrderFactory,
        \Fooman\Connect\Model\DataSource\OrderFactory $connectDataSourceOrderFactory,
        \Fooman\Connect\Model\ItemFactory $connectItemFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->salesOrderFactory = $salesOrderFactory;
        $this->connectDataSourceOrderFactory = $connectDataSourceOrderFactory;
        $this->connectItemFactory = $connectItemFactory;
        $this->storeManager = $storeManager;

        parent::__construct(
            $context,
            $registry,
            $connectXeroApi,
            $backendAuthSession,
            $connectHelper,
            $scopeConfig,
            $resource,
            $resourceCollection,
            $data
        );
    }

    protected function _construct()
    {
        $this->_init('Fooman\Connect\Model\ResourceModel\Order');
    }

    /**
     * @param int $orderId
     *
     * @return array
     */
    public function exportByOrderId($orderId)
    {
        $order = $this->salesOrderFactory->create()->load($orderId);

        return $this->exportOne($order);
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return array
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function neverExportOne(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $orderStatus = $this->load($order->getId(), 'order_id');
        $orderStatus->setOrderId($order->getId());

        if ($orderStatus->getXeroExportStatus() == \Fooman\Connect\Model\Status::EXPORTED) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('$1: ' . 'Has already been exported.', $order->getIncrementId())
            );
        }

        $orderStatus->setXeroExportStatus(\Fooman\Connect\Model\Status::WONT_EXPORT)->save();
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return array
     */
    public function exportOne(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        /** @var \Fooman\Connect\Model\Order $orderStatus */
        $this->unsetData();
        $orderStatus = $this->load($order->getId(), 'order_id');
        if (!$orderStatus->getOrderId()) {
            $orderStatus->isObjectNew(true);
            $orderStatus->setOrderId((int) $order->getId());
        }
        if ($order->getBaseGrandTotal() == 0
            && !$this->scopeConfig->isSetFlag(
                'foomanconnect/order/exportzero',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $order->getStoreId()
            )
        ) {
            $orderStatus->setXeroExportStatus(\Fooman\Connect\Model\Status::WONT_EXPORT);
            $orderStatus->setXeroLastValidationErrors('');
            $orderStatus->save();
        } else {
            try {
                $orderData = [];
                $dataSource = $this->connectDataSourceOrderFactory->create(['order' => $order]);
                $orderData = $dataSource->getOrderData();
                //$this->connectItemFactory->create()->ensureItemsExist($orderData, $order->getStoreId());
                $result = $this->sendToXero($dataSource->getXml($orderData), $order->getStoreId());
                $orderStatus->setXeroInvoiceId($result['Invoices'][0]['InvoiceID']);
                $orderStatus->setXeroInvoiceNumber($result['Invoices'][0]['InvoiceNumber']);
                $orderStatus->setXeroExportStatus(\Fooman\Connect\Model\Status::EXPORTED);
                $orderStatus->setXeroLastValidationErrors('');
                $orderStatus->save();

                return $result;
            } catch (\Fooman\Connect\Exception $e) {
                $this->_handleError($orderStatus, $e, $e->getXeroErrors(), $order, $orderData);
            } catch (\Exception $e) {
                $this->_handleError($orderStatus, $e, $e->getMessage(), $order, $orderData);
            }
        }
    }

    public function exportOrders()
    {
        $stores = array_keys($this->storeManager->getStores());
        foreach ($stores as $storeId) {
            if ($this->scopeConfig->isSetFlag(
                'foomanconnect/cron/xeroautomatic',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            )
            ) {
                /** @var \Fooman\Connect\Model\ResourceModel\Order\Collection $collection */
                $collection = $this->getCollection()->getUnexportedOrders($storeId)->setPageSize(self::PROCESS_PER_RUN);
                $collection->getSelect()->order('created_at DESC');
                $collection->addConfigDateFilter($storeId);
                foreach ($collection as $order) {
                    /* @var $order \Fooman\Connect\Model\Creditmemo */
                    $this->exportByOrderId($order->getEntityId());
                }
            }
        }
    }

    /**
     * @param $xml
     * @param $storeId
     *
     * @return array
     */
    public function sendToXero($xml, $storeId)
    {
        return $this->getApi()->setStoreId($storeId)->sendData(
            \Fooman\Connect\Model\Xero\Api::INVOICES_PATH,
            \Zend\Http\Request::METHOD_POST,
            $xml
        );
    }

    /**
     * @return bool|string
     */
    public function getSalesEntityViewId()
    {
        return $this->_getSalesEntityViewId('order');
    }
}
