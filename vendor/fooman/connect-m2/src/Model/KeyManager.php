<?php

namespace Fooman\Connect\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class KeyManager
{
    const KEY_LENGTH = 2048;
    const CERT_DAYS_VALID = 3650;
    const DEFAULT_UNIT_NAME = 'Magento Xero Integration by Fooman';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Encryption\Encryptor
     */
    protected $encryptor;

    protected $storeManager;

    protected $configResource;

    protected $cacheTypeList;

    /**
     * @var string
     */
    protected $adminUserEmail;

    /**
     * @var int
     */
    protected $storeId;

    public function __construct(
        ScopeConfigInterface $configScopeConfigInterface,
        \Magento\Framework\Encryption\Encryptor $encryptor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Fooman\Connect\Model\Config\Backend\Encrypted $configResource,
        \Magento\Framework\App\Cache\TypeListInterface $typeListInterface,
        $adminUserEmail = '',
        $storeId = \Magento\Store\Model\Store::DEFAULT_STORE_ID
    ) {
        $this->scopeConfig = $configScopeConfigInterface;
        $this->encryptor = $encryptor;
        $this->storeManager = $storeManager;
        $this->configResource = $configResource;
        $this->cacheTypeList = $typeListInterface;
        $this->adminUserEmail = $adminUserEmail;
        $this->storeId = $storeId;
    }

    public function createPrivateKey()
    {
        $privateKey = $this->getPrivateKey();
        if ($privateKey) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Private Key already exists.')
            );
        }
        $this->configResource->setPath('foomanconnect/settings/privatekey');
        $this->configResource->setValue($this->generatePrivateKey());
        $this->configResource->setScope(ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
        $this->configResource->setScopeId($this->storeId);
        $this->configResource->save();

        // clear the config since only tag based cleaning
        // \Magento\Framework\App\CacheInterface::clean(['fooman-encrypted']);
        // has no effect
        $this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
    }

    protected function generatePrivateKey()
    {
        $dn = [
            'private_key_bits' => self::KEY_LENGTH,
        ];
        $privKeyResource = openssl_pkey_new($dn);
        if (!$privKeyResource) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __(
                    'Couldn\'t create private key - please check your server\'s php openssl configuration: %s',
                    openssl_error_string()
                )
            );
        }
        openssl_pkey_export($privKeyResource, $privateKey);
        if (empty($privateKey)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __(
                    'Couldn\'t create private key - please check your server\'s php openssl configuration: %s',
                    openssl_error_string()
                )
            );
        }

        return $privateKey;
    }

    public function generatePublicKey()
    {
        $privateKey = $this->getPrivateKey();
        if (!$privateKey) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('A Private Key is required before generating a Public Key.')
            );
        }

        $privateKeyResource = openssl_pkey_get_private($privateKey);
        $dn = [
            'countryName' => $this->getConfigValue('general/country/default'),
            'stateOrProvinceName' => $this->getConfigValue('general/country/default'),
            'organizationName' => $this->storeManager->getStore($this->storeId)->getName(),
            'organizationalUnitName' => self::DEFAULT_UNIT_NAME,
            'commonName' => $this->getCommonName(),
            'emailAddress' => $this->adminUserEmail,
        ];
        $csrResource = openssl_csr_new($dn, $privateKeyResource);
        $cert = openssl_csr_sign($csrResource, null, $privateKeyResource, self::CERT_DAYS_VALID);
        openssl_x509_export($cert, $publicKey);

        return $publicKey;
    }

    public function getCommonName()
    {
        return rtrim(str_replace(['https://', 'http://'], '', $this->getConfigValue('web/unsecure/base_url')), '/');
    }

    protected function getConfigValue($path)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeId
        );
    }

    /**
     * @return bool|string
     */
    public function getPrivateKey()
    {
        $privateKey = $this->getConfigValue('foomanconnect/settings/privatekey');
        if (empty($privateKey)) {
            return false;
        }

        return $this->encryptor->decrypt($privateKey);
    }

    public function getPrivateKeyPassword()
    {
        $privateKeyPw = $this->getConfigValue('foomanconnect/settings/privatekeypassword');
        if (empty($privateKeyPw)) {
            return false;
        }

        return $this->encryptor->decrypt($privateKeyPw);
    }

    public function getConsumerKey()
    {
        $consumerKey = $this->getConfigValue('foomanconnect/settings/consumerkey');
        if (empty($consumerKey)) {
            return false;
        }

        return $this->encryptor->decrypt($consumerKey);
    }

    public function getConsumerSecret()
    {
        $consumerSecret = $this->getConfigValue('foomanconnect/settings/consumersecret');
        if (empty($consumerSecret)) {
            return false;
        }

        return $this->encryptor->decrypt($consumerSecret);
    }
}
