<?php

namespace Fooman\Connect\Model\DataSource;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Invoice extends AbstractSalesModelDataSource
{
    /**
     * @var LineItemHandlerFactory
     */
    protected $itemHandlerFactory;

    /**
     * @var \Magento\Sales\Api\Data\InvoiceInterface
     */
    protected $invoice;

    /**
     * @var \Fooman\Connect\Model\DataSource\TotalFactory
     */
    protected $connectDataSourceTotalFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Fooman\Connect\Helper\Config $connectConfigHelper,
        \Magento\Customer\Model\CustomerFactory $customerCustomerFactory,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Directory\Model\CountryInformationAcquirer $countryInformationAcquirer,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Sales\Api\Data\InvoiceInterface $invoice,
        \Fooman\Connect\Model\DataSource\LineItemHandlerFactory $itemHandler,
        \Fooman\Connect\Model\DataSource\TotalFactory $connectDataSourceTotalFactory,
        array $data = []
    ) {
        $this->invoice = $invoice;
        $this->itemHandlerFactory = $itemHandler;
        $this->connectDataSourceTotalFactory = $connectDataSourceTotalFactory;
        parent::__construct(
            $scopeConfig,
            $connectConfigHelper,
            $customerCustomerFactory,
            $backendHelper,
            $eventManager,
            $countryInformationAcquirer,
            $pdfConfig,
            $data
        );
    }

    public function getSalesObject()
    {
        return $this->invoice;
    }

    public function getOrder()
    {
        return  $this->invoice->getOrder();
    }

    /**
     * @param array $input
     *
     * @return string
     */
    public function getXml(array $input = null)
    {
        if (null === $input) {
            $input = $this->getInvoiceData();
        }

        return \Fooman\ConnectLicense\Model\DataSource\Converter\OrderXml::convert($input);
    }

    public function getInvoiceData()
    {
        if (!$this->invoice->getEntityId()) {
            throw new \Fooman\Connect\Model\DataSource\Exception(
                'No Invoice'
            );
        }
        $base = $this->scopeConfig->getValue(
            'foomanconnect/settings/xerotransfercurrency',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        ) == \Fooman\Connect\Model\System\CurrencyOptions::TRANSFER_BASE;
        $data = [];
        $data += $this->getOrderInfo($base);
        $data += $this->getSettings();
        $data += $this->getLineItems($base);
        $data += $this->getCustomerInfo();
        $data += $this->getBillingAddress();
        $data += $this->getTotals($base);
        $data = $this->applyFixes($data);
        ksort($data);

        return $this->dispatchEvent('invoice', $this->getSalesObject(), $data);
    }

    public function getLineItems($base = false)
    {
        $data = [];
        $versionUsed = $this->scopeConfig->getValue(
            'foomanconnect/settings/xeroversion',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        );
        $taxRate = \Fooman\Connect\Model\Xero\Defaults::getTaxrate($versionUsed, 0);
        $taxPercentage = 0;
        foreach ($this->getSalesObject()->getItems() as $item) {
            $dataSource = $this->itemHandlerFactory->create(
                [
                    'item' => $item,
                    'base_to_order_rate' => $this->getOrder()->getBaseToOrderRate(),
                ]
            );

            $itemData = $dataSource->getItemData($base);
            $data += $itemData;

            if ($itemData) {
                $taxRate = array_shift($itemData);
                if (isset($taxRate['taxType'])) {
                    $taxPercentage = $taxRate['taxPercent'];
                    $taxRate = $taxRate['taxType'];
                } else {
                    $taxRate = null;
                }
            }
        }

        foreach ($this->pdfConfig->getTotals() as $code => $total) {
            if (strlen($total['source_field'])) {
                $dataSource = $this->getTotalDataSourceModel($code, $total, $taxRate, $taxPercentage);
                $itemData = $dataSource->getItemData($base);
                if ($itemData) {
                    $data += $itemData;
                }
            }
        }

        return ['invoiceLines' => $data];
    }

    protected function getLinkToObject()
    {
        return $this->backendHelper->getUrl(
            'sales/order_invoice/view/invoice_id/'.$this->getSalesObject()->getEntityId(),
            ['_nosid' => true, '_nosecret' => true]
        );
    }
}
