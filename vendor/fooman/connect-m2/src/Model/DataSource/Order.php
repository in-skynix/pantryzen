<?php

namespace Fooman\Connect\Model\DataSource;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Order extends AbstractSalesModelDataSource
{
    protected $actualDiscount = 0;
    protected $actualSubtotal = 0;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Fooman\Connect\Helper\Config
     */
    protected $connectConfigHelper;

    /**
     * @var \Fooman\Connect\Model\DataSource\OrderItemHandler
     */
    protected $itemHandlerFactory;

    /**
     * @var \Fooman\Connect\Model\DataSource\TotalFactory
     */
    protected $connectDataSourceTotalFactory;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerCustomerFactory;

    /**
     * @var \Magento\Backend\Helper\Data
     */
    protected $backendHelper;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    protected $order;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Fooman\Connect\Helper\Config $connectConfigHelper,
        \Magento\Customer\Model\CustomerFactory $customerCustomerFactory,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Directory\Model\CountryInformationAcquirer $countryInformationAcquirer,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Fooman\Connect\Model\DataSource\OrderItemHandlerFactory $orderItemHandler,
        \Fooman\Connect\Model\DataSource\TotalFactory $connectDataSourceTotalFactory,
        array $data = []
    ) {
        $this->order = $order;
        $this->itemHandlerFactory = $orderItemHandler;
        $this->connectDataSourceTotalFactory = $connectDataSourceTotalFactory;
        parent::__construct(
            $scopeConfig,
            $connectConfigHelper,
            $customerCustomerFactory,
            $backendHelper,
            $eventManager,
            $countryInformationAcquirer,
            $pdfConfig,
            $data
        );
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getSalesObject()
    {
        return $this->getOrder();
    }

    /**
     * @param array $input
     *
     * @return string
     */
    public function getXml(array $input = null)
    {
        if (null === $input) {
            $input = $this->getOrderData();
        }

        return \Fooman\ConnectLicense\Model\DataSource\Converter\OrderXml::convert($input);
    }

    /**
     * @return array
     *
     * @throws \Fooman\Connect\Model\DataSource\Exception
     */
    public function getOrderData()
    {
        if (!$this->getOrder()->getId()) {
            throw new \Fooman\Connect\Model\DataSource\Exception(
                'No Order'
            );
        }
        $base = $this->scopeConfig->getValue(
            'foomanconnect/settings/xerotransfercurrency',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        ) == \Fooman\Connect\Model\System\CurrencyOptions::TRANSFER_BASE;
        $data = [];
        $data += $this->getOrderInfo($base);
        $data += $this->getSettings();
        $data += $this->getLineItems($base);
        $data += $this->getCustomerInfo();
        $data += $this->getBillingAddress();
        $data += $this->getTotals($base);
        $data = $this->applyFixes($data);
        ksort($data);

        return $this->dispatchEvent('order', $this->getSalesObject(), $data);
    }

    public function getLineItems($base = false)
    {
        $data = [];
        $taxRate = false;
        $taxPercentage = 0;
        foreach ($this->getOrder()->getItems() as $item) {
            $dataSource = $this->itemHandlerFactory->create(
                [
                    'item' => $item,
                    'base_to_order_rate' => $this->getOrder()->getBaseToOrderRate(),
                ]
            );
            $itemData = $dataSource->getItemData($base);
            $data += $itemData;

            if ($itemData) {
                $taxRate = array_shift($itemData);
                if (isset($taxRate['taxType'])) {
                    $taxPercentage = $taxRate['taxPercent'];
                    $taxRate = $taxRate['taxType'];
                } else {
                    $taxRate = null;

                }
            }
        }

        foreach ($this->pdfConfig->getTotals() as $code => $total) {
            if (strlen($total['source_field'])) {
                $dataSource = $this->getTotalDataSourceModel($code, $total, $taxRate, $taxPercentage);
                $itemData = $dataSource->getItemData($base);
                if ($itemData) {
                    $data += $itemData;
                }
            }
        }

        return ['invoiceLines' => $data];
    }

    protected function getLinkToObject()
    {
        return $this->backendHelper->getUrl(
            'sales/order/view/order_id/'.$this->getOrder()->getId(),
            ['_nosid' => true, '_nosecret' => true]
        );
    }
}
