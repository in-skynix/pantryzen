<?php

namespace Fooman\Connect\Model\DataSource;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class LineItemHandler implements \Fooman\Connect\Model\Api\LineItemHandlerInterface
{
    /**
     * @var \Fooman\Connect\Model\DataSource\LineItem\BundleFactory
     */
    protected $connectDataSourceLineItemBundleFactory;

    /**
     * @var \Fooman\Connect\Model\DataSource\LineItem\SimpleFactory
     */
    protected $connectDataSourceLineItemSimpleFactory;

    protected $item;
    /**
     * @var \Magento\Sales\Api\Data\OrderItemInterface
     */
    protected $orderItem;

    public function __construct(
        \Fooman\Connect\Model\DataSource\LineItem\BundleFactory $connectDataSourceLineItemBundleFactory,
        \Fooman\Connect\Model\DataSource\LineItem\SimpleFactory $connectDataSourceLineItemSimpleFactory,
        \Magento\Sales\Api\OrderItemRepositoryInterface $salesOrderItemFactory,
        $item
    ) {
        $this->connectDataSourceLineItemBundleFactory = $connectDataSourceLineItemBundleFactory;
        $this->connectDataSourceLineItemSimpleFactory = $connectDataSourceLineItemSimpleFactory;
        $this->orderItemRepository = $salesOrderItemFactory;
        $this->item = $item;
    }

    public function getItem()
    {
        return $this->item;
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderItemInterface
     */
    public function getOrderItem()
    {
        if (null === $this->orderItem) {
            //Magento unexpected behaviour
            //quote_item_id exists instead of order_item_id
            $orderItemId = $this->getItem()->getOrderItemId()
                ? $this->getItem()->getOrderItemId()
                : $this->getItem()->getQuoteItemId();
            $this->orderItem = $this->orderItemRepository->get($orderItemId);
        }

        return $this->orderItem;
    }

    public function getItemData($base = false)
    {
        if ($this->getOrderItem()->getParentItemId()) {
            return [];
        }
        switch ($this->getOrderItem()->getProductType()) {
            case \Magento\Bundle\Model\Product\Type::TYPE_CODE:
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Support for bundle products is coming in a future version.')
                );
                /*$dataSource = $this->connectDataSourceLineItemBundleFactory->create(
                    ['data' => ['item' => $this->getItem(), 'order_item' => $this->getOrderItem()]]
                );
                break;*/
            case \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE:
            case \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL:
            case \Magento\GroupedProduct\Model\Product\Type\Grouped::TYPE_CODE:
            case \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE:
            case \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE:
                $dataSource = $this->connectDataSourceLineItemSimpleFactory->create(
                    ['data' => ['item' => $this->getItem(), 'order_item' => $this->getOrderItem()]]
                );
                break;
            default:
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Unsupported product type %1', $this->getOrderItem()->getProductType())
                );
        }

        return $dataSource->getItemData($base);
    }
}
