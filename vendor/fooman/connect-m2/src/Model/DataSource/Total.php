<?php

namespace Fooman\Connect\Model\DataSource;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Total extends AbstractDataSource
{
    protected $product = null;
    protected $order = null;

    protected $toIgnore = ['grand_total', 'subtotal', 'tax', 'discount', 'adjustment_positive', 'adjustment_negative'];

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Fooman\Connect\Model\System\TaxOptions
     */
    protected $connectSystemTaxOptions;

    protected $pdfTotalFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Fooman\Connect\Model\System\TaxOptions $connectSystemTaxOptions,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->connectSystemTaxOptions = $connectSystemTaxOptions;
        $this->pdfTotalFactory = $pdfTotalFactory;

        parent::__construct($scopeConfig, $eventManager, $data);
    }

    protected function _construct()
    {
        if (!$this->getSalesObject() instanceof \Magento\Sales\Model\AbstractModel) {
            throw new \Fooman\Connect\Model\DataSource\Exception(
                'Expected Mage_Sales_Model_Abstract as data source input.'
            );
        }

        if (!$this->getTotal() instanceof \Magento\Framework\App\Config\Element) {
            throw new \Fooman\Connect\Model\DataSource\Exception(
                'Expected a config node as total input.'
            );
        }

        if (!$this->getTotal()['source_field']) {
            throw new \Fooman\Connect\Model\DataSource\Exception(
                'Expected a non empty string as order total source field.'
            );
        }

        if (!$this->getCode()) {
            throw new \Fooman\Connect\Model\DataSource\Exception(
                'Expected a non empty string as order total code.'
            );
        }
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function getOrder()
    {
        if (null === $this->order) {
            if ($this->getSalesObject() instanceof \Magento\Sales\Api\Data\OrderInterface) {
                $this->order = $this->getSalesObject();
            } else {
                $this->order = $this->getSalesObject()->getOrder();
            }
        }

        return $this->order;
    }

    public function getItemData($base = false)
    {
        if (array_search($this->getCode(), $this->toIgnore) !== false) {
            return false;
        }

        $method = 'get'.\Magento\Framework\Api\SimpleDataObjectConverter::snakeCaseToUpperCamelCase(
            $this->getTotal()['source_field']
        );
        if (method_exists($this, $method)) {
            return $this->$method($base);
        } else {
            if ($this->getAmount($this->getSalesObject(), $this->getTotal()['source_field'], $base) == 0) {
                return false;
            }

            return $this->getTotalDefault($base);
        }
    }

    protected function _standardTotalId()
    {
        return $this->getCode();
    }

    protected function _standardTotalData()
    {
        $data = [];
        $data['sku'] = $this->getCode();
        if (strlen($this->getCode()) <= \Fooman\Connect\Model\Item::ITEM_CODE_MAX_LENGTH) {
            $data['itemCode'] = $this->getCode();
        }
        $data['qtyOrdered'] = '1.0000';
        $data['name'] = __($this->getTotal()['title']);
        $data['taxPercent'] = 0;

        return $data;
    }

    public function getShippingAmount($base)
    {
        if ($this->getAmount($this->getSalesObject(), $this->getTotal()['source_field'], $base) == 0) {
            return false;
        }
        $data = $this->_standardTotalData();
        $data['name'] = $this->getOrder()->getShippingDescription();
        if (strlen($this->getOrder()->getShippingMethod()) <= \Fooman\Connect\Model\Item::ITEM_CODE_MAX_LENGTH) {
            $data['itemCode'] = $this->getOrder()->getShippingMethod();
        }
        $data['taxAmount'] = $this->getAmount($this->getSalesObject(), 'shipping_tax_amount', $base);
        $data['taxType'] = $this->_getShippingTax();
        $data['price'] = $this->getAmount($this->getSalesObject(), $this->getTotal()['source_field'], $base);
        $data['unitAmount'] = $this->_getShippingAmount($base);
        $data['taxPercent'] = $this->getShippingTaxPercent($data);
        $data['lineTotalNoAdjust'] = $data['price'];
        $data['lineTotal'] = $data['unitAmount'];
        $data['xeroAccountCodeSale'] = $this->scopeConfig->getValue(
            'foomanconnect/xeroaccount/codeshipping',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        );

        return [$this->_standardTotalId() => $data];
    }

    protected function _getShippingTax()
    {
        $storeId = $this->getSalesObject()->getStoreId();
        $shippingTaxType = $this->scopeConfig->getValue(
            'foomanconnect/tax/xeroshipping',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        if (\Fooman\Connect\Model\System\TaxOptions::USE_ITEM_TAX_TYPE == $shippingTaxType) {
            $shippingTaxType = $this->getItemTaxRate();
            $shippingTaxPercent = $this->getItemTaxPercentage();
        } else {
            $shippingTaxPercent = $this->connectSystemTaxOptions->getTaxPercentageForRate(
                $shippingTaxType,
                $storeId
            );
        }

        if ((float) $this->getSalesObject()->getShippingAmount() > 0
            && (float) $this->getSalesObject()->getShippingTaxAmount() == 0
            && $shippingTaxPercent != 0
        ) {
            if ($this->scopeConfig->isSetFlag(
                'foomanconnect/xeroaccount/shippingisexpenseaccount',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            )
            ) {
                $shippingTaxType = $this->scopeConfig->getValue(
                    'foomanconnect/tax/xeroexpensezerotaxrate',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $storeId
                );
            } else {
                $shippingTaxType = $this->scopeConfig->getValue(
                    'foomanconnect/tax/xerodefaultzerotaxrate',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $storeId
                );
            }
        }

        return $shippingTaxType;
    }

    /*
     * retrieve the shipping amount
     * favour shipping_incl_tax
     * as shipping_incl_tax != shipping_amount + shipping_tax_amount
     * however there are buggy implementations for shipping_incl_tax out there
     */
    public function _getShippingAmount($base)
    {
        if (floatval($this->getAmount($this->getSalesObject(), 'shipping_incl_tax', $base)) > 0.00) {
            return $this->getAmount($this->getSalesObject(), 'shipping_incl_tax', $base);
        } else {
            return $this->getAmount($this->getSalesObject(), 'shipping_amount', $base) +
            $this->getAmount($this->getSalesObject(), 'shipping_tax_amount', $base);
        }
    }

    public function getShippingTaxPercent($data)
    {
        if ($data['taxAmount'] == 0) {
            return 0;
        }

        //Magento does not keep track of the shipping tax percentage directly
        $shippingTaxType = $data['taxType'];

        if (empty($shippingTaxType)) {
            throw new \Fooman\Connect\Model\System\Exception(
                'No Shipping Tax Rate is defined under Stores > Configuration > Fooman Connect > Xero > Tax'
            );
        }

        return $this->connectSystemTaxOptions->toOptionArraySafe(
            $shippingTaxType,
            $this->getSalesObject()->getStoreId()
        );
    }

    public function getTotalDefault($base)
    {
        $data = $this->_standardTotalData();
        $data['taxAmount'] = 0;
        $data['taxType'] = 'NONE';
        $data['price'] = $this->getAmount($this->getSalesObject(), $this->getTotal()['source_field'], $base);
        $data['unitAmount'] = $this->getAmount($this->getSalesObject(), $this->getTotal()['source_field'], $base);
        $data['lineTotalNoAdjust'] = $this->getAmount(
            $this->getSalesObject(),
            $this->getTotal()['source_field'],
            $base
        );
        $data['lineTotal'] = $this->getAmount(
            $this->getSalesObject(),
            $this->getTotal()['source_field'],
            $base
        );
        $data['xeroAccountCodeSale'] = '';

        return [$this->_standardTotalId() => $data];
    }
}
