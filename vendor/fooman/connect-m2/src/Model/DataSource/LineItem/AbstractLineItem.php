<?php

namespace Fooman\Connect\Model\DataSource\LineItem;

use Fooman\Connect\Model\DataSource\AbstractDataSource;
use \Fooman\Connect\Model\Api\DataSourceLineItemInterface;

abstract class AbstractLineItem extends AbstractDataSource implements DataSourceLineItemInterface
{
    protected $taxRates = [];

    protected $salesTaxItem;

    /**
     * @var \Magento\Catalog\Api\Data\ProductInterface
     */
    protected $product;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $catalogProductFactory;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Fooman\Connect\Model\System\TaxOptionsFactory
     */
    protected $connectSystemTaxOptionsFactory;

    /**
     * @var \Magento\Tax\Model\ResourceModel\Calculation\Rate\CollectionFactory
     */
    protected $rateFactory;

    protected $item;

    /**
     * @var \Fooman\Connect\Model\ResourceModel\Order\Tax\Item\Collection
     */
    protected $taxOrderItemCollection;

    /**
     * @var \Fooman\Connect\Model\TaxrateLinkRepository
     */
    protected $foomanTaxrateLinkRepository;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Catalog\Model\ProductFactory $catalogProductFactory,
        \Fooman\Connect\Model\System\TaxOptionsFactory $connectSystemTaxOptionsFactory,
        \Magento\Tax\Model\Calculation\RateFactory $rateFactory,
        \Fooman\Connect\Model\ResourceModel\Order\Tax\Item\Collection $taxOrderItemCollection,
        \Fooman\Connect\Model\TaxrateLinkRepository $foomanTaxrateLinkRepository,
        array $data = []
    ) {
        $this->catalogProductFactory = $catalogProductFactory;
        $this->connectSystemTaxOptionsFactory = $connectSystemTaxOptionsFactory;
        $this->rateFactory = $rateFactory;
        $this->taxOrderItemCollection = $taxOrderItemCollection;
        $this->foomanTaxrateLinkRepository = $foomanTaxrateLinkRepository;
        parent::__construct($scopeConfig, $eventManager, $data);
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderItemInterface
     */
    abstract public function getOrderItem();

    public function getItem()
    {
        return $this->getData('item');
    }

    public function getQty()
    {
        return $this->getItem()->getQty();
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function getProduct()
    {
        if (null === $this->product && $this->getItem()->getProductId()) {
            $this->product = $this->catalogProductFactory->create()
                ->setStoreId($this->getOrderItem()->getStoreId())
                ->load($this->getItem()->getProductId());
        }

        return $this->product;
    }

    /**
     * @param bool|false $base
     *
     * @return array
     */
    public function getItemData($base = false)
    {
        $data = [];
        $magentoItemId = $this->getItemId();
        $data['sku'] = $this->getItem()->getSku();
        //if (strlen($this->getItem()->getSku()) <= \Fooman\Connect\Model\Item::ITEM_CODE_MAX_LENGTH) {
            $data['itemCode'] = $this->getItem()->getSku();
        //}
        $data['qtyOrdered'] = $this->getQty();
        $data['name'] = $this->getItem()->getName();
        $data['taxAmount'] = $this->getAmount($this->getItem(), 'tax_amount', $base);
        if (null === $data['taxAmount']) {
            $data['taxAmount'] = 0;
        }
        $data['discountRate'] = $this->getDiscountPercent($base);
        $data['taxType'] = $this->getItemTaxRate();
        $data['price'] = $this->getAmount($this->getOrderItem(), 'price', $base);
        $data['taxPercent'] = $this->getOrderItem()->getTaxPercent();
        $data['unitAmount'] = $this->getAmount($this->getOrderItem(), 'price_incl_tax', $base);
        // disconnect between price_incl_tax and price+tax
        // $data['unitAmount'] =  $data['price'] + $data['taxAmount']/$data['qtyOrdered'];
        $data['lineTotalNoAdjust'] = $this->getAmount($this->getItem(), 'row_total_incl_tax', $base);
        $data['lineTotal'] = $this->getAmount($this->getItem(), 'row_total_incl_tax', $base);
        $data['xeroAccountCodeSale'] = $this->getXeroAccountCodeSale();

        $transport = new \Magento\Framework\DataObject(['item_data' => $data]);
        $this->eventManager->dispatch(
            'foomanconnect_xero_lineitem',
            [
                'item'       => $this->getItem(),
                'order_item' => $this->getOrderItem(),
                'transport'  => $transport,
            ]
        );

        return [$magentoItemId => $transport->getItemData()];
    }

    public function getDiscountPercent($base)
    {
        if ($this->getOrderItem()->getDiscountPercent() != 0) {
            return $this->getOrderItem()->getDiscountPercent();
        }
        $discount = $this->getAmount($this->getItem(), 'discount_amount', $base);

        if ($this->getHiddenTaxAmount()) {
            $discount += $this->getAmount($this->getItem(), 'hidden_tax_amount', $base)
                * (1 + $this->getItem()->getTaxPercent() / 100);
        } elseif ($this->scopeConfig->getValue(
            'tax/calculation/apply_after_discount',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getOrderItem()->getStoreId()
        ) == 1
        ) {
            $discount *= 1 + $this->getItem()->getTaxPercent() / 100;
        }

        $rowTotal = $this->getAmount($this->getItem(), 'row_total_incl_tax', $base);
        if (0 == $discount || 0 == $rowTotal) {
            return 0;
        }

        return round(100 * $discount / $rowTotal, 2);
    }

    public function getXeroAccountCodeSale()
    {
        if ($this->getProduct() && $this->getProduct()->getXeroSalesAccountCode()) {
            return $this->getProduct()->getXeroSalesAccountCode();
        }

        return $this->scopeConfig->getValue(
            'foomanconnect/xeroaccount/codesale',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getOrderItem()->getStoreId()
        );
    }

    /**
     * Retrieve taxcode as used in Xero for item in the following order:
     * 1. check against mapped tax rates via tax code
     * 2. check for Default Tax Rate with zero tax
     * 3. check if tax rate matches default as mentioned here: http://blog.xero.com/developer/api/types/
     * 4. download all rates from Xero and match based on tax percentage
     * 5. last return empty to let Xero use its default for the account.
     *
     * @return string
     */
    protected function getItemTaxRate()
    {
        $item = $this->getOrderItem();
        $storeId = $item->getStoreId();

        if ($item->getTaxAmount() == 0) {
            return $this->scopeConfig->getValue(
                'foomanconnect/tax/xerodefaultzerotaxrate',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            );
        }

        $versionUsed = $this->scopeConfig->getValue(
            'foomanconnect/settings/xeroversion',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $taxRate = $this->getTaxRateFromItem($item);

        if (!$taxRate) {
            $taxRate = \Fooman\Connect\Model\Xero\Defaults::getTaxrate($versionUsed, $item->getTaxPercent());
        }

        if (!$taxRate) {
            if (empty($this->taxRates)) {
                $this->taxRates = $this->connectSystemTaxOptionsFactory->create()->toOptionArray('options-only');
            }
            if (isset($this->taxRates[sprintf('%01.4f', $item->getTaxPercent())])) {
                $taxRate = $this->taxRates[sprintf('%01.4f', $item->getTaxPercent())];
            } else {
                $taxRate = '';
            }
        }

        return $taxRate;
    }

    /**
     * @param $item
     *
     * @return bool|string
     */
    protected function getTaxRateFromItem($item)
    {
        $salesOrderTaxItem = $this->getSalesOrderTaxItem($item);
        $taxrateLink = $this->foomanTaxrateLinkRepository->getByCode($salesOrderTaxItem->getCode());
        return $taxrateLink->getXeroTaxrate();
    }

    /**
     * @param $item
     *
     * @return \Magento\Framework\DataObject
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getSalesOrderTaxItem($item)
    {
        if (is_null($this->salesTaxItem)) {
            $itemTaxCollection = $this->taxOrderItemCollection->getTaxItemsByItemId($item->getItemId());

            if (count($itemTaxCollection) > 1) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    'Xero can\'t accept multiple tax rates on a single item'
                );
            }
            $firstItem = $itemTaxCollection->getFirstItem();

            $taxCollection = $this->rateFactory->create()->getCollection();
            $taxCollection->getSelect()->joinLeft(
                ['sot' => $taxCollection->getTable('sales_order_tax')],
                'sot.code = main_table.code'
            );

            $taxCollection->addFieldToFilter('sot.tax_id', $firstItem->getTaxId());
            $this->salesTaxItem = $taxCollection->getFirstItem();
        }

        return $this->salesTaxItem;
    }

    /**
     * @return mixed
     */
    protected function getItemId()
    {
        return $this->getItem()->getEntityId();
    }
}
