<?php

namespace Fooman\Connect\Model\DataSource\LineItem;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Bundle extends \Fooman\Connect\Model\DataSource\LineItem\AbstractLineItem
{
    /**
     * @var \Magento\Sales\Api\Data\OrderItemInterface
     */
    protected $orderItem;

    /**
     * @var \Magento\Sales\Model\Order\ItemFactory
     */
    protected $salesOrderItemFactory;

    /**
     * @var \Fooman\Connect\Model\DataSource\LineItem\SimpleFactory
     */
    protected $connectDataSourceLineItemSimpleFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Catalog\Model\ProductFactory $catalogProductFactory,
        \Fooman\Connect\Model\System\TaxOptionsFactory $connectSystemTaxOptionsFactory,
        \Magento\Tax\Model\Calculation\RateFactory $rateFactory,
        \Fooman\Connect\Model\ResourceModel\Order\Tax\Item\Collection $taxOrderItemCollection,
        \Fooman\Connect\Model\TaxrateLinkRepository $foomanTaxrateLinkRepository,
        \Magento\Sales\Model\Order\ItemFactory $salesOrderItemFactory,
        \Fooman\Connect\Model\DataSource\LineItem\SimpleFactory $connectDataSourceLineItemSimpleFactory,
        array $data = []
    ) {
        $this->salesOrderItemFactory = $salesOrderItemFactory;
        $this->connectDataSourceLineItemSimpleFactory = $connectDataSourceLineItemSimpleFactory;
        parent::__construct(
            $scopeConfig,
            $eventManager,
            $catalogProductFactory,
            $connectSystemTaxOptionsFactory,
            $rateFactory,
            $taxOrderItemCollection,
            $foomanTaxrateLinkRepository,
            $data
        );
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderItemInterface
     */
    public function getOrderItem()
    {
        if (null === $this->orderItem) {
            //Magento unexpected behaviour
            //quote_item_id exists instead of order_item_id
            $orderItemId = $this->getItem()->getOrderItemId()
                ? $this->getItem()->getOrderItemId()
                : $this->getItem()->getQuoteItemId();
            $this->orderItem = $this->salesOrderItemFactory->create()->load($orderItemId);
        }

        return $this->orderItem;
    }

    public function getQty()
    {
        return $this->getItem()->getQty();
    }

    protected function _getBundleChildren()
    {
        if ($this->getItem() instanceof \Magento\Sales\Model\Order\Creditmemo\Item) {
            $collection = Mage::getResourceModel('sales/order_creditmemo_item_collection');
        } else {
            $collection = Mage::getResourceModel('sales/order_invoice_item_collection');
        }
        //parent_item_id relation is not maintained for invoices,creditmemos
        $collection->getSelect()->joinLeft(
            ['soi' => $collection->getTable('sales_flat_order_item')],
            'soi.item_id = main_table.order_item_id'
        );
        $collection->addFieldToFilter('parent_item_id', $this->getOrderItem()->getId());

        return $collection;
    }

    protected function _getNoneFixedPriceChildItem($item, $parentData, $base = false)
    {
        $magentoItemId = $item->getId();
        $dataSource = $this->connectDataSourceLineItemSimpleFactory->create();

        $data = $dataSource->getItemData($base);
        if (empty($data[$magentoItemId]['taxPercent'])) {
            $data[$magentoItemId]['taxPercent'] = $parentData['taxPercent'];
        }
        if (empty($data[$magentoItemId]['discountRate'])) {
            $data[$magentoItemId]['discountRate'] = $parentData['discountRate'];
        }

        return $data;
    }

    public function getItemData($base = false)
    {
        $data = [];
        $magentoItemId = $this->getItem()->getId();

        $taxInclusive = Mage::getStoreConfigFlag(
            \Magento\Tax\Model\Config::CONFIG_XML_PATH_PRICE_INCLUDES_TAX,
            $this->getOrderItem()->getStoreId()
        );
        $fixed = $this->_isFixedPriceBundle();

        if ($fixed) {
            $parentData = $this->_getFixedParentItem($taxInclusive, $base);
        } else {
            $parentData = $this->_getNoneFixedParentItem();
        }
        $data += [$magentoItemId => $parentData];
        foreach ($this->_getBundleChildren() as $childItem) {
            if ($fixed) {
                $data += $this->_getFixedPriceChildItem(
                    $childItem,
                    $parentData,
                    $taxInclusive,
                    $base
                );
            } else {
                $data += $this->_getNoneFixedPriceChildItem(
                    $childItem,
                    $parentData,
                    $base
                );
            }
        }

        $transport = new \Magento\Framework\DataObject;
        $transport->setItemData($data);
        $this->eventManager->dispatch(
            'foomanconnect_xero_bundlelineitem',
            [
                'item' => $this->getItem(),
                'order_item' => $this->getOrderItem(),
                'transport' => $transport,
            ]
        );

        return $transport->getItemData();
    }

    protected function _isFixedPriceBundle()
    {
        $fixedBundlePrice = false;
        $options = $this->getOrderItem()->getProductOptions();
        if ($options) {
            if (isset($options['product_calculations'])
                && $options['product_calculations'] == \Magento\Catalog\Model\Product\Type\AbstractType::CALCULATE_CHILD
            ) {
                $fixedBundlePrice = false;
            } else {
                $fixedBundlePrice = true;
            }
        }

        return $fixedBundlePrice;
    }

    protected function _getFixedParentItem($taxInclusive, $base = false)
    {
        $bundleprice = $this->_getBundlePrice($taxInclusive, $base);
        $data = $this->_getCommonItemData($this->getItem(), $base);

        $data['qtyOrdered'] = $this->getQty();
        $data['taxAmount'] = $this->getAmount($this->getItem(), 'tax_amount', $base);
        if (null === $data['taxAmount']) {
            $data['taxAmount'] = 0;
        }
        $data['discountRate'] = $this->getOrderItem()->getDiscountPercent();
        $data['taxType'] = $this->getItemTaxRate();
        $data['price'] = $this->getAmount($this->getItem(), 'price', $base);
        $data['taxPercent'] = $this->getOrderItem()->getTaxPercent();
        $data['unitAmount'] = $this->getAmount($this->getItem(), 'price_incl_tax', $base) - $bundleprice;
        $data['lineTotalNoAdjust'] = $this->getAmount($this->getItem(), 'row_total_incl_tax', $base);
        $data['lineTotal'] = $this->getAmount($this->getItem(), 'row_total_incl_tax', $base);
        $data['xeroAccountCodeSale'] = $this->getXeroAccountCodeSale();

        return $data;
    }

    protected function _getNoneFixedParentItem()
    {
        $data = $this->_getCommonItemData($this->getItem());
        $data['qtyOrdered'] = $this->getQty();

        $data['taxAmount'] = 0;
        $data['discountRate'] = 0;
        $data['taxType'] = 'NONE';
        $data['price'] = 0;
        $data['taxPercent'] = 0;
        $data['unitAmount'] = 0;
        $data['lineTotalNoAdjust'] = 0;
        $data['lineTotal'] = 0;
        $data['xeroAccountCodeSale'] = $this->getXeroAccountCodeSale();

        return $data;
    }

    protected function _getFixedPriceChildItem($item, $parentData, $taxInclusive, $base = false)
    {
        $magentoItemId = $item->getId();
        $orderItem = $this->getOrderItemForItem($item);
        $productOptions = $orderItem->getProductOptions();

        $data = $this->_getCommonItemData($item, $base);
        $data['discountRate'] = $orderItem->getDiscountPercent() != '0.0000'
            ? $orderItem->getDiscountPercent()
            : $parentData['discountRate'];
        $data['taxPercent'] = $orderItem->getTaxPercent() != '0.0000'
            ? $orderItem->getTaxPercent()
            : $parentData['taxPercent'];
        $data['xeroAccountCodeSale'] = $this->getXeroAccountCodeSaleForItem($item, $parentData['xeroAccountCodeSale']);
        $data['taxType'] = $parentData['taxType'];

        $data['price'] = $this->getAmount($orderItem, 'price', $base);

        if (isset($productOptions['bundle_selection_attributes'])) {
            $bundleSelectionAttributes = unserialize($productOptions['bundle_selection_attributes']);
            $data['qtyOrdered'] = $parentData['qtyOrdered'] * $bundleSelectionAttributes['qty'];
            $data['unitAmount'] = round($bundleSelectionAttributes['price'] / $bundleSelectionAttributes['qty'], 2);
            if (!$taxInclusive) {
                $data['unitAmount'] = round($data['unitAmount'] * (1 + $data['taxPercent'] / 100), 2);
            }
            $data['lineTotal'] = $parentData['qtyOrdered']
                * $bundleSelectionAttributes['qty']
                * round($bundleSelectionAttributes['price'], 2);
        } else {
            $data['unitAmount'] = 0;
            $data['qtyOrdered'] = 1;
        }

        $data['taxAmount'] = $this->recalculatedTax($data);

        return [$magentoItemId => $data];
    }

    protected function _getCommonItemData($item, $base = false)
    {
        $data = [];
        $data['sku'] = $item->getSku();
        //if (strlen($item->getSku()) <= Fooman_Connect_Model_Item::ITEM_CODE_MAX_LENGTH) {
            $data['itemCode'] = $item->getSku();
        //}
        $data['name'] = $item->getName();
        $data['lineTotalNoAdjust'] = $this->getAmount($item, 'row_total_incl_tax', $base);

        return $data;
    }

    protected function _getBundlePrice($taxInclusive, $base = false)
    {
        $bundleItemsWithPrices = 0;
        $productOptions = $this->getOrderItem()->getProductOptions();
        if ($base) {
            if (isset($productOptions['bundle_options'])) {
                foreach ($productOptions['bundle_options'] as $bundleOption) {
                    foreach ($bundleOption['value'] as $bundleValue) {
                        $itemPrice = round(
                            $bundleValue['price'] * $this->getBaseToOrderRate(),
                            2
                        );
                        if (!$taxInclusive) {
                            $itemPrice = round($itemPrice * (1 + $this->getOrderItem()->getTaxPercent() / 100), 2);
                        }
                        $bundleItemsWithPrices += $itemPrice;
                    }
                }
            }
        } else {
            if (isset($productOptions['bundle_options'])) {
                foreach ($productOptions['bundle_options'] as $bundleOption) {
                    foreach ($bundleOption['value'] as $bundleValue) {
                        $itemPrice = round($bundleValue['price'], 2);
                        if (!$taxInclusive) {
                            $itemPrice = round($itemPrice * (1 + $this->getOrderItem()->getTaxPercent() / 100), 2);
                        }
                        $bundleItemsWithPrices += $itemPrice;
                    }
                }
            }
        }

        return $bundleItemsWithPrices;
    }

    public function getXeroAccountCodeSaleForItem($item, $parentXeroSalesAccountCode)
    {
        $product = $this->catalogProductFactory->create()->load($item->getProductId());
        if ($product && $product->getXeroSalesAccountCode()) {
            return $product->getXeroSalesAccountCode();
        }

        return $parentXeroSalesAccountCode;
    }

    public function getOrderItemForItem($item)
    {
        if ($item instanceof \Magento\Sales\Model\Order\Item) {
            return $item;
        }
        //Magento unexpected behaviour
        //quote_item_id can exist instead of order_item_id
        $orderItemId = $this->getItem()->getOrderItemId()
            ? $this->getItem()->getOrderItemId()
            : $this->getItem()->getQuoteItemId();
        $this->_orderItem = $this->salesOrderItemFactory->create()->load($orderItemId);

        return $this->salesOrderItemFactory->create()->load($item->getOrderItemId());
    }

    public function recalculatedTax($data)
    {
        //unitAmount is tax inclusive since derived from bundle_options
        $taxed = $data['unitAmount'] * $data['qtyOrdered'];
        $untaxed = $taxed / (1 + ($data['taxPercent'] / 100));

        return round($taxed - $untaxed, 2);
    }
}
