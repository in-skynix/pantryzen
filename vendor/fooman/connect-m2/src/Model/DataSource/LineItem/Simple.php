<?php

namespace Fooman\Connect\Model\DataSource\LineItem;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Simple extends \Fooman\Connect\Model\DataSource\LineItem\AbstractLineItem
{
    /**
     * @var \Magento\Sales\Api\Data\OrderItemInterface
     */
    protected $orderItem;

    /**
     * @var \Magento\Sales\Model\Order\ItemFactory
     */
    protected $salesOrderItemFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Catalog\Model\ProductFactory $catalogProductFactory,
        \Fooman\Connect\Model\System\TaxOptionsFactory $connectSystemTaxOptionsFactory,
        \Magento\Tax\Model\Calculation\RateFactory $rateFactory,
        \Fooman\Connect\Model\ResourceModel\Order\Tax\Item\Collection $taxOrderItemCollection,
        \Magento\Sales\Model\Order\ItemFactory $salesOrderItemFactory,
        \Fooman\Connect\Model\TaxrateLinkRepository $foomanTaxrateLinkRepository,
        array $data = []
    ) {
        $this->salesOrderItemFactory = $salesOrderItemFactory;
        parent::__construct(
            $scopeConfig,
            $eventManager,
            $catalogProductFactory,
            $connectSystemTaxOptionsFactory,
            $rateFactory,
            $taxOrderItemCollection,
            $foomanTaxrateLinkRepository,
            $data
        );
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderItemInterface
     */
    public function getOrderItem()
    {
        if (null === $this->orderItem) {
            //Magento unexpected behaviour
            //quote_item_id exists instead of order_item_id
            $orderItemId = $this->getItem()->getOrderItemId()
                ? $this->getItem()->getOrderItemId()
                : $this->getItem()->getQuoteItemId();
            $this->orderItem = $this->salesOrderItemFactory->create()->load($orderItemId);
        }

        return $this->orderItem;
    }

    public function getQty()
    {
        return $this->getItem()->getQty();
    }
}
