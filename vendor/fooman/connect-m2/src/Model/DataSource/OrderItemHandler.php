<?php

namespace Fooman\Connect\Model\DataSource;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class OrderItemHandler implements \Fooman\Connect\Model\Api\LineItemHandlerInterface
{
    protected $product = null;

    /**
     * @var \Fooman\Connect\Model\DataSource\OrderItem\BundleFactory
     */
    protected $connectDataSourceOrderItemBundleFactory;

    /**
     * @var \Fooman\Connect\Model\DataSource\OrderItem\SimpleFactory
     */
    protected $connectDataSourceOrderItemSimpleFactory;

    /**
     * @var \Magento\Sales\Api\Data\OrderItemInterface
     */
    protected $item;

    public function __construct(
        \Fooman\Connect\Model\DataSource\OrderItem\BundleFactory $connectDataSourceOrderItemBundleFactory,
        \Fooman\Connect\Model\DataSource\OrderItem\SimpleFactory $connectDataSourceOrderItemSimpleFactory,
        \Magento\Sales\Api\Data\OrderItemInterface $item
    ) {
        $this->connectDataSourceOrderItemBundleFactory = $connectDataSourceOrderItemBundleFactory;
        $this->connectDataSourceOrderItemSimpleFactory = $connectDataSourceOrderItemSimpleFactory;
        $this->item = $item;
    }

    public function getItem()
    {
        return $this->item;
    }

    public function getItemData($base = false)
    {
        if ($this->getItem()->getParentItemId()) {
            return [];
        }
        switch ($this->getItem()->getProductType()) {
            case \Magento\Bundle\Model\Product\Type::TYPE_CODE:
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Support for bundle products is coming in a future version.')
                );
                /*$dataSource = $this->connectDataSourceOrderItemBundleFactory->create(
                    ['data' => ['item' => $this->getItem()]]
                );
                break;*/
            case \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE:
            case \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL:
            case \Magento\GroupedProduct\Model\Product\Type\Grouped::TYPE_CODE:
            case \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE:
            case \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE:
                $dataSource = $this->connectDataSourceOrderItemSimpleFactory->create(
                    ['data' => ['item' => $this->getItem()]]
                );
                break;
            default:
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Unsupported product type %1', $this->getItem()->getProductType())
                );
        }

        return $dataSource->getItemData($base);
    }
}
