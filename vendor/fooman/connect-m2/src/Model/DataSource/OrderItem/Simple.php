<?php

namespace Fooman\Connect\Model\DataSource\OrderItem;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Simple extends \Fooman\Connect\Model\DataSource\LineItem\AbstractLineItem
{
    public function getOrderItem()
    {
        return $this->getItem();
    }

    public function getQty()
    {
        return $this->getItem()->getQtyOrdered();
    }

    public function getItemId()
    {
        return $this->getItem()->getItemId();
    }
}
