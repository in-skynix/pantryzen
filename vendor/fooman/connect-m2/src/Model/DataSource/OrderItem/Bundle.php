<?php

namespace Fooman\Connect\Model\DataSource\OrderItem;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Bundle extends \Fooman\Connect\Model\DataSource\LineItem\Bundle
{
    public function getOrderItem()
    {
        return $this->getItem();
    }

    protected function _getNoneFixedPriceChildItem($item, $parentData, $base = false)
    {
        $magentoItemId = $item->getId();
        $dataSource = $this->connectDataSourceOrderItemSimpleFactory->create();

        $data = $dataSource->getItemData($base);
        if (empty($data[$magentoItemId]['taxPercent'])) {
            $data[$magentoItemId]['taxPercent'] = $parentData['taxPercent'];
        }
        if (empty($data[$magentoItemId]['discountRate'])) {
            $data[$magentoItemId]['discountRate'] = $parentData['discountRate'];
        }

        return $data;
    }

    protected function _getBundleChildren()
    {
        $collection = Mage::getResourceModel('sales/order_item_collection');
        $collection->addFieldToFilter('parent_item_id', $this->getItem()->getId());

        return $collection;
    }
}
