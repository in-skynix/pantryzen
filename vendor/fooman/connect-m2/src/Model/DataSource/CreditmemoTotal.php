<?php

namespace Fooman\Connect\Model\DataSource;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class CreditmemoTotal extends Total
{
    protected $toIgnore = ['grand_total', 'subtotal', 'tax', 'discount'];

    public function getAdjustmentNegative($base)
    {
        if ($this->getAmount($this->getSalesObject(), (string) $this->getTotal()['source_field'], $base) == 0) {
            return false;
        }
        $data = $this->_standardTotalData();
        $data['taxAmount'] = 0;
        $data['taxType'] = 'NONE';
        $data['price'] = -1 * $this->getAmount(
            $this->getSalesObject(),
            (string)$this->getTotal()['source_field'],
            $base
        );
        $data['unitAmount'] = -1 * $this->getAmount(
            $this->getSalesObject(),
            (string)$this->getTotal()['source_field'],
            $base
        );
        $data['lineTotalNoAdjust'] = -1 * $this->getAmount(
            $this->getSalesObject(),
            (string) $this->getTotal()->source_field,
            $base
        );
        $data['lineTotal'] = -1 * $this->getAmount(
            $this->getSalesObject(),
            (string) $this->getTotal()->source_field,
            $base
        );
        $data['xeroAccountCodeSale'] = $this->scopeConfig->getValue(
            'foomanconnect/xeroaccount/coderefunds',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        );

        return [$this->_standardTotalId() => $data];
    }
}
