<?php

namespace Fooman\Connect\Model\DataSource;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class AbstractDataSource extends \Magento\Framework\DataObject
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->eventManager = $eventManager;
        parent::__construct($data);
    }

    /**
     * @param \Magento\Framework\DataObject $object
     * @param string                        $field
     * @param bool                          $base
     *
     * @return mixed
     */
    protected function getAmount(\Magento\Framework\DataObject $object, $field, $base = false)
    {
        if ($base) {
            $field = 'base_'.$field;
        }
        $amount = $object->getDataUsingMethod($field);
        if (null === $amount) {
            $amount = 0;
        }

        return $amount;
    }

    /**
     * @param \Magento\Framework\DataObject $object
     *
     * @return string
     */
    public function getCreatedAtAsUTC(\Magento\Framework\DataObject $object)
    {
        $datetime = new \DateTime($object->getCreatedAt());

        return $datetime->format('Y-m-d');
    }

    /**
     * @param \Magento\Framework\DataObject $object
     *
     * @return string
     */
    public function getCreatedAtStore(\Magento\Framework\DataObject $object)
    {
        $datetime = new \DateTime($object->getCreatedAt());

        $timezone = $this->scopeConfig->getValue(
            'general/locale/timezone',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $object->getStoreId()
        );
        if ($timezone) {
            $storeTime = new \DateTimeZone($timezone);
            $datetime->setTimezone($storeTime);
        }

        return $datetime->format('Y-m-d');
    }

    /**
     * @param float $amount
     *
     * @return string
     */
    public function roundedAmount($amount)
    {
        return sprintf('%01.4f', round($amount, 2));
    }

    /**
     * @param string                                 $eventName
     * @param \Magento\Framework\Model\AbstractModel $eventObject
     * @param array                                  $data
     *
     * @return mixed
     */
    protected function dispatchEvent($eventName, \Magento\Framework\Model\AbstractModel $eventObject, array $data)
    {
        $transport = new \Magento\Framework\DataObject();
        $transport->setDataUsingMethod($eventName.'_data', $data);
        $this->eventManager->dispatch(
            'fooman_connect_datasource_'.$eventName,
            [
                'transport' => $transport,
                $eventName => $eventObject,
            ]
        );

        return $transport->getDataUsingMethod($eventName.'_data');
    }
}
