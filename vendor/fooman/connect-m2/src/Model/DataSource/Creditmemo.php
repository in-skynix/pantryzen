<?php

namespace Fooman\Connect\Model\DataSource;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Creditmemo extends AbstractSalesModelDataSource
{
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Fooman\Connect\Helper\Config $connectConfigHelper,
        \Magento\Customer\Model\CustomerFactory $customerCustomerFactory,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Directory\Model\CountryInformationAcquirer $countryInformationAcquirer,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Sales\Api\Data\CreditmemoInterface $creditmemo,
        \Fooman\Connect\Model\DataSource\LineItemHandlerFactory $itemHandler,
        \Fooman\Connect\Model\DataSource\CreditmemoTotalFactory  $connectDataSourceTotalFactory,
        array $data = []
    ) {
        $this->creditmemo = $creditmemo;
        $this->itemHandlerFactory = $itemHandler;
        $this->connectDataSourceTotalFactory = $connectDataSourceTotalFactory;
        parent::__construct(
            $scopeConfig,
            $connectConfigHelper,
            $customerCustomerFactory,
            $backendHelper,
            $eventManager,
            $countryInformationAcquirer,
            $pdfConfig,
            $data
        );
    }

    public function getSalesObject()
    {
        return $this->creditmemo;
    }

    public function getOrder()
    {
        return $this->creditmemo->getOrder();
    }

    /**
     * @param array $input
     *
     * @return string
     */
    public function getXml(array $input = null)
    {
        if (null === $input) {
            $input = $this->getCreditmemoData();
        }

        return \Fooman\ConnectLicense\Model\DataSource\Converter\CreditmemoXml::convert($input);
    }

    public function getCreditmemoData()
    {
        if (!$this->creditmemo->getEntityId()) {
            throw new \Fooman\Connect\Model\DataSource\Exception(
                'No Creditmemo'
            );
        }
        $base = $this->scopeConfig->getValue(
            'foomanconnect/settings/xerotransfercurrency',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        ) == \Fooman\Connect\Model\System\CurrencyOptions::TRANSFER_BASE;
        $data = [];
        $data += $this->getOrderInfo($base);
        $data += $this->getSettings();
        $data += $this->getLineItems($base);
        $data += $this->getCustomerInfo();
        $data += $this->getBillingAddress();
        $data += $this->getTotals($base);
        $data['lineAmountTypes'] = $this->confirmLineAmountTypes($data);
        $data = $this->applyFixes($data);
        ksort($data);

        return $this->dispatchEvent('creditmemo', $this->getSalesObject(), $data);
    }

    protected function getLinkToObject()
    {
        return $this->backendHelper->getUrl(
            'sales/order_creditmemo/view/creditmemo_id/'.$this->getSalesObject()->getId(),
            ['_nosid' => true, '_nosecret' => true]
        );
    }

    protected function getIncrementId()
    {
        if ($this->scopeConfig->getValue(
            'foomanconnect/settings/xeronumbering',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getOrder()->getStoreId()
        )) {
            return '';
        } else {
            $prefix = $this->scopeConfig->getValue(
                'foomanconnect/creditmemo/xeroprefix',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $this->getOrder()->getStoreId()
            );

            return $prefix.$this->getSalesObject()->getIncrementId();
        }
    }

    protected function getReference()
    {
        $reference = $this->getOrder()->getIncrementId();
        $transport = new \Magento\Framework\DataObject();
        $transport->setReference($reference);
        $this->eventManager->dispatch(
            'foomanconnect_xero_creditmemo_reference',
            [
                'sales_object' => $this->getSalesObject(),
                'transport' => $transport,
            ]
        );

        return $transport->getReference();
    }

    public function runMixedCalcAdjustments($line, $taxInclusive)
    {
        $line = $this->factorOutDiscount($line);

        return parent::runMixedCalcAdjustments($line, $taxInclusive);
    }

    public function runXeroCalcAdjustments($line, $taxInclusive)
    {
        $line = $this->factorOutDiscount($line);

        return parent::runXeroCalcAdjustments($line, $taxInclusive);
    }

    public function getXeroStatus()
    {
        return $this->scopeConfig->getValue(
            'foomanconnect/creditmemo/xerostatus',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        );
    }

    public function getLineItems($base = false)
    {
        $data = [];
        $versionUsed = $this->scopeConfig->getValue(
            'foomanconnect/settings/xeroversion',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        );
        $taxRate = \Fooman\Connect\Model\Xero\Defaults::getTaxrate($versionUsed, 0);
        $taxPercentage = 0;
        foreach ($this->getSalesObject()->getItems() as $item) {
            $dataSource = $this->itemHandlerFactory->create(
                [
                    'item' => $item,
                    'base_to_order_rate' => $this->getOrder()->getBaseToOrderRate(),
                ]
            );

            $itemData = $dataSource->getItemData($base);
            $data += $itemData;

            if ($itemData) {
                $taxRate = array_shift($itemData);
                if (isset($taxRate['taxType'])) {
                    $taxPercentage = $taxRate['taxPercent'];
                    $taxRate = $taxRate['taxType'];
                } else {
                    $taxRate = null;
                }
            }
        }

        foreach ($this->pdfConfig->getTotals() as $code => $total) {
            if (strlen($total['source_field'])) {
                $dataSource = $this->getTotalDataSourceModel($code, $total, $taxRate, $taxPercentage);
                $itemData = $dataSource->getItemData($base);
                if ($itemData) {
                    $data += $itemData;
                }
            }
        }

        return ['invoiceLines' => $data];
    }

    /**
     * Xero does not support discounts on CreditNotes
     * switch to tax inclusive line items if a
     * discount is present.
     *
     * @param $data
     *
     * @return string
     */
    public function confirmLineAmountTypes($data)
    {
        if ($data['lineAmountTypes'] == \Fooman\Connect\Model\Xero\Api::LINEITEMS_TAX_INCL) {
            return $data['lineAmountTypes'];
        }

        foreach ($data['invoiceLines'] as $line) {
            if (isset($line['discountRate']) && $line['discountRate'] > 0) {
                return \Fooman\Connect\Model\Xero\Api::LINEITEMS_TAX_INCL;
            }
        }

        return $data['lineAmountTypes'];
    }
}
