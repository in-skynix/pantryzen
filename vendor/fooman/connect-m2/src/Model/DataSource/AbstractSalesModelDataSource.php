<?php

namespace Fooman\Connect\Model\DataSource;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

abstract class AbstractSalesModelDataSource extends AbstractDataSource
{
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Fooman\Connect\Helper\Config $connectConfigHelper,
        \Magento\Customer\Model\CustomerFactory $customerCustomerFactory,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Directory\Model\CountryInformationAcquirer $countryInformationAcquirer,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        array $data = []
    ) {
        $this->connectConfigHelper = $connectConfigHelper;
        $this->customerCustomerFactory = $customerCustomerFactory;
        $this->backendHelper = $backendHelper;
        $this->countryInformationAcquirer = $countryInformationAcquirer;
        $this->pdfConfig = $pdfConfig;
        parent::__construct($scopeConfig, $eventManager, $data);
    }

    abstract protected function getLinkToObject();

    public function getOrderInfo($base)
    {
        $data = [];
        $data['url'] = $this->getLinkToObject();
        $data['incrementId'] = $this->getIncrementId();
        $data['reference'] = $this->getReference();
        if ($base) {
            $data['currencyCode'] = $this->getOrder()->getStoreCurrencyCode();
        } else {
            $data['currencyCode'] = $this->getOrder()->getOrderCurrencyCode();
        }
        $data['createdAt'] = $this->getCreatedAtStore($this->getSalesObject());
        $data['shippingDescription'] = $this->getOrder()->getShippingDescription();
        $data['status'] = $this->getXeroStatus();

        return $data;
    }

    public function getCustomerInfo()
    {
        $data = [];
        //unfortunately using customer id will fail in Xero if the same customer uses a different billing name
        //on subsequent orders - likely related to Xero's inability to have the same customer name twice
        //$data['customerId'] = $this->getOrder()->getCustomerId();
        $data['customerFirstname'] = trim($this->getOrder()->getBillingAddress()->getFirstname());
        $data['customerLastname'] = trim($this->getOrder()->getBillingAddress()->getLastname());
        $data['customerEmail'] = $this->getOrder()->getCustomerEmail();

        if ($this->scopeConfig->isSetFlag(
            'foomanconnect/settings/usecompany',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        )
        ) {
            $company = trim($this->getOrder()->getBillingAddress()->getCompany());
            if (!empty($company)) {
                $data['firstName'] = $data['customerFirstname'];
                $data['lastName'] = $data['customerLastname'];
                $data['customerFirstname'] = $company;
                $data['customerLastname'] = '';
            }
        }
        if (empty($data['customerFirstname'])) {
            $data['customerFirstname'] = trim($this->getOrder()->getCustomerFirstname());
            $data['customerLastname'] = trim($this->getOrder()->getCustomerLastname());
        }

        $data['taxNumber'] = $this->retrieveTaxNumber();

        return $data;
    }

    /**
     * get billing address info.
     *
     * @return array
     *
     * @throws \Fooman\Connect\Model\DataSource\Exception
     */
    public function getBillingAddress()
    {
        $billingAddress = $this->getOrder()->getBillingAddress();
        $data = [];

        if (!$billingAddress) {
            throw new \Fooman\Connect\Model\DataSource\Exception(
                'Order has no billing address.'
            );
        }

        $data['billingStreet1'] = $billingAddress->getStreet()[0];
        $data['billingStreet2'] = isset($billingAddress->getStreet()[1]) ? $billingAddress->getStreet()[1] : '';
        $data['billingCity'] = $billingAddress->getCity();
        $data['billingPostcode'] = $billingAddress->getPostcode();
        $data['billingCountry'] = $this->countryInformationAcquirer
            ->getCountryInfo($billingAddress->getCountryId())
            ->getFullNameLocale();
        $data['billingRegion'] = $billingAddress->getRegion();
        $data['billingTelephone'] = $billingAddress->getTelephone();

        return $data;
    }

    protected function getIncrementId()
    {
        if ($this->scopeConfig->getValue(
            'foomanconnect/settings/xeronumbering',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getOrder()->getStoreId()
        )
        ) {
            return '';
        } else {
            return $this->getSalesObject()->getIncrementId();
        }
    }

    protected function getReference()
    {
        if ($this->scopeConfig->getValue(
            'foomanconnect/settings/xeronumbering',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getOrder()->getStoreId()
        )
        ) {
            $reference = $this->getSalesObject()->getIncrementId();
        } else {
            $payment = $this->getOrder()->getPayment();
            $reference = '';
            if ($payment) {
                switch ($payment->getMethod()) {
                    case 'purchaseorder':
                        $reference = $payment->getPoNumber();
                        break;
                    default:
                        $reference = $payment->getLastTransId();
                        break;
                }
            }
        }

        $transport = new \Magento\Framework\DataObject();
        $transport->setReference($reference);
        $this->eventManager->dispatch(
            'foomanconnect_xero_order_reference',
            [
                'sales_object' => $this->getSalesObject(),
                'transport' => $transport,
            ]
        );

        return $transport->getReference();
    }

    public function getSettings()
    {
        $storeId = $this->getSalesObject()->getStoreId();
        $data = [];
        $data['shippingTaxTypeOverride'] = true;
        $data['xeroAccountCodeSale'] = $this->scopeConfig->getValue(
            'foomanconnect/xeroaccount/codesale',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $data['xeroAccountCodeDiscounts'] = $this->scopeConfig->getValue(
            'foomanconnect/xeroaccount/codediscounts',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $data['xeroAccountRounding'] = $this->scopeConfig->getValue(
            'foomanconnect/xeroaccount/coderounding',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $data['xeroAccountCodeShipping'] = $this->scopeConfig->getValue(
            'foomanconnect/xeroaccount/codeshipping',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $data['xeroAccountCodeSurcharge'] = $this->scopeConfig->getValue(
            'foomanconnect/xeroaccount/codesurcharge',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        if ($this->isTaxOverrideMode()) {
            $data['lineAmountTypes'] = 'Exclusive';
        } elseif ($this->scopeConfig->isSetFlag(
            \Magento\Tax\Model\Config::CONFIG_XML_PATH_PRICE_INCLUDES_TAX,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        )
        ) {
            $data['lineAmountTypes'] = 'Inclusive';
        } else {
            $data['lineAmountTypes'] = 'Exclusive';
        }

        $data += $this->connectConfigHelper->getTrackingCategory(
            $storeId,
            $this->getOrder()->getCustomerGroupId()
        );

        return $data;
    }

    public function isTaxOverrideMode()
    {
        return \Fooman\Connect\Model\System\TaxOverrideOptions::MAGE_CALC ==
        $this->scopeConfig->getValue(
            'foomanconnect/tax/xerooverridetax',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        );
    }

    public function getTotals($base = false)
    {
        $data = [];
        $data['taxAmount'] = $this->roundedAmount($this->getAmount($this->getSalesObject(), 'tax_amount', $base));
        $data['grandTotal'] = $this->roundedAmount($this->getAmount($this->getSalesObject(), 'grand_total', $base));

        return $data;
    }

    protected function getTotalDataSourceModel($code, $total, $taxRate, $taxPercentage = 0)
    {
        return $this->connectDataSourceTotalFactory->create(
            ['data' => [
                'sales_object' => $this->getSalesObject(),
                'code' => $code,
                'total' => $total,
                'item_tax_rate' => $taxRate,
                'item_tax_percentage' => $taxPercentage
                ],
            ]
        );
    }

    /**
     * try to retrieve a tax number from various sources for this order
     * try order first, then billing address and lastly the customer account.
     *
     * @return string|bool
     */
    protected function retrieveTaxNumber()
    {
        if ($this->getOrder()->getCustomerTaxvat()) {
            return $this->getOrder()->getCustomerTaxvat();
        }

        $billingAddress = $this->getOrder()->getBillingAddress();
        if ($billingAddress) {
            $country = $this->scopeConfig->getValue(
                'foomanconnect/settings/xeroversion',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $this->getOrder()->getStoreId()
            );
            if ($country == 'uk') {
                if ($billingAddress->getVatId()) {
                    return $billingAddress->getVatId();
                }
            }

            if ($billingAddress->getTaxId()) {
                return $billingAddress->getTaxId();
            }
        }

        if ($this->getOrder()->getCustomerId()) {
            $customer = $this->customerCustomerFactory->create()->load($this->getOrder()->getCustomerId());
            if ($customer->getId()) {
                if ($customer->getTaxVat()) {
                    return $customer->getTaxVat();
                }
            }
        }

        return '';
    }

    /**
     * unfortunately Magento and Xero do not always agree on how to calculate
     * tax, rounding, bugs, etc.
     *
     * adjust data based on chosen tax calculation mode
     * MAGE_CALC = take Magento's value directly - requires it to calculate exactly like Xero to not get rejected
     * XERO_CALC = let Xero recalculate
     * MIXED_CALC =  attempt to bridge the gap
     *
     * @param $data
     *
     * @return mixed
     */
    public function applyFixes($data)
    {
        $mode = $this->scopeConfig->getValue(
            'foomanconnect/tax/xerooverridetax',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        );
        switch ($mode) {
            case \Fooman\Connect\Model\System\TaxOverrideOptions::MAGE_CALC:
                if ($data['lineAmountTypes'] == 'Exclusive') {
                    foreach ($data['invoiceLines'] as $key => $line) {
                        $data['invoiceLines'][$key] = $this->removeTaxFromUnitAmount($line);
                    }
                }
                break;

            case \Fooman\Connect\Model\System\TaxOverrideOptions::MIXED_CALC:
                foreach ($data['invoiceLines'] as $key => $line) {
                    $data['invoiceLines'][$key] = $this->runMixedCalcAdjustments($line, $data['lineAmountTypes']);
                }
                $data = $this->lineItemsAddUpToGrandTotal($data);
                break;

            case \Fooman\Connect\Model\System\TaxOverrideOptions::XERO_CALC:
                foreach ($data['invoiceLines'] as $key => $line) {
                    $data['invoiceLines'][$key] = $this->runXeroCalcAdjustments($line, $data['lineAmountTypes']);
                }
                $data = $this->lineItemsAddUpToGrandTotal($data);
                break;

            case \Fooman\Connect\Model\System\TaxOverrideOptions::XERO_REDUCED:
                $reducedLines = $this->runReduction($data['invoiceLines'], $data['lineAmountTypes']);
                unset($data['invoiceLines']);
                $data['invoiceLines'] = $reducedLines;
                $data = $this->lineItemsAddUpToGrandTotal($data);
                break;
        }

        return $data;
    }

    public function runReduction($lines, $taxInclusive)
    {
        $combined = [];
        foreach ($lines as $line) {
            $taxType = $line['taxType'];
            if ($taxInclusive == 'Exclusive') {
                $line = $this->removeTaxFromUnitAmount($line);
            }
            if (isset($combined[$taxType])) {
                $qtyOneLine = $this->reduceQtyForLine($line);
                $combined[$taxType]['unitAmount'] += $qtyOneLine['unitAmount'];
                //$combined[$taxType]['taxAmount'] += $qtyOneLine['taxAmount'];
                $combined[$taxType]['name'] .= ', '.$qtyOneLine['name'];
            } else {
                $combined[$taxType] = $this->reduceQtyForLine($line);
            }
        }

        return $combined;
    }

    public function runMixedCalcAdjustments($line, $taxInclusive)
    {
        //$this->fixUnitAmount($line);
        $line = $this->unitPricesAddUpToLineTotal($line, true);
        if ($taxInclusive == 'Exclusive') {
            $line = $this->removeTaxFromUnitAmount($line);
        }
        $line = $this->removeTaxAmount($line);
        $line = $this->removeLineTotalAmount($line);

        return $line;
    }

    public function runXeroCalcAdjustments($line, $taxInclusive)
    {
        //$this->fixUnitAmount($line);
        $line = $this->unitPricesAddUpToLineTotal($line, false);
        if ($taxInclusive == 'Exclusive') {
            $line = $this->removeTaxFromUnitAmount($line);
        }
        $line = $this->removeTaxAmount($line);
        $line = $this->removeLineTotalAmount($line);

        return $line;
    }

    /**
     * this one is generally not required but can be used in cases where
     * price_incl_tax and row_total_incl_tax are incorrect
     * uses price and tax percent to reconstruct unitAmount and lineTotal.
     *
     * @param $line
     *
     * @return mixed
     */
    public function fixUnitAmount($line)
    {
        $priceDiff = ($line['price'] * (1 + ($line['taxPercent']) / 100)) - $line['unitAmount'];
        if (abs($priceDiff) > 0.05) {
            $line['unitAmount'] = round($line['price'] * (1 + ($line['taxPercent']) / 100), 2);
        }
        $line['lineTotal'] = $line['unitAmount'] * $line['qtyOrdered'];

        return $line;
    }

    /**
     * make sure that the individual line items combine to the grand total.
     *
     * @param $data
     *
     * @return mixed
     */
    public function lineItemsAddUpToGrandTotal($data)
    {
        $actualDiscount = 0;
        $actualSubtotal = 0;
        $actualTax = 0;
        foreach ($data['invoiceLines'] as $line) {
            $effectiveSubtotal = $this->getEffectiveLineTotal($line, $data['lineAmountTypes']);
            $actualSubtotal += $effectiveSubtotal;
            $actualTax += round(
                $effectiveSubtotal - round(($effectiveSubtotal / (1 + $line['taxPercent'] / 100)), 2),
                2
            );
        }

        $rounding = $data['grandTotal'] - $actualSubtotal - $actualDiscount;
        if (abs(round($rounding, 2)) >= 0.01) {
            $data['invoiceLines']['rounding'] = $this->getRoundingEntry(
                'Rounding',
                $rounding,
                $data['xeroAccountRounding']
            );
        }
        $data['taxAmount'] = $this->roundedAmount($actualTax);

        return $data;
    }

    /**
     * Xero only supports 2 digit calculations, ensure the line total works.
     *
     * @param $line
     * @param $fixLineTotal
     *
     * @return mixed
     */
    public function unitPricesAddUpToLineTotal($line, $fixLineTotal)
    {
        if (isset($line['unitAmount'])) {
            $lineTotalRecalc = $this->roundedAmount($line['qtyOrdered'] * round($line['unitAmount'], 2));
            if ($line['lineTotal'] != $lineTotalRecalc) {
                if ($fixLineTotal) {
                    $line['lineTotal'] = $lineTotalRecalc;
                } else {
                    $line['name'] = sprintf(
                        '%s x %s @ %s',
                        $line['qtyOrdered'],
                        $line['name'],
                        $line['unitAmount']
                    );
                    $line['qtyOrdered'] = 1;
                    $line['unitAmount'] = $line['lineTotal'];
                }
            }
        }

        return $line;
    }

    /**
     * make sure we only use qty 1 for the invoice line.
     *
     * @param $line
     *
     * @return mixed
     */
    public function reduceQtyForLine($line)
    {
        $line = $this->factorOutDiscount($line);
        $line = $this->removeLineTotalAmount($line);
        if (isset($line['itemCode'])) {
            unset($line['itemCode']);
        }
        if (isset($line['sku'])) {
            unset($line['sku']);
        }
        unset($line['taxAmount']);
        $line['unitAmount'] = $line['unitAmount'] * $line['qtyOrdered'];
        $line['qtyOrdered'] = 1;

        return $line;
    }

    /**
     * adjust tax amount to be in line with Xero's calculation.
     *
     * @param $line
     * @param $taxInclusive
     *
     * @return mixed
     */
    public function checkTaxAmounts($line, $taxInclusive)
    {
        $effectiveLineTotal = $this->getEffectiveLineTotal($line, $taxInclusive);
        $taxRecalculated = $effectiveLineTotal - round($effectiveLineTotal / (1 + ($line['taxPercent']) / 100), 2);
        $taxDifference = round($line['taxAmount'] - $taxRecalculated, 2);
        if ($taxDifference) {
            $line['taxAmount'] = $taxRecalculated;
        }

        return $line;
    }

    /**
     * unitAmount is tax inclusive, factor it out here (pre - discount).
     *
     * @param $line
     */
    public function removeTaxFromUnitAmount($line)
    {
        $line['unitAmount'] = round($line['unitAmount'] / (1 + ($line['taxPercent'] / 100)), 2);

        return $line;
    }

    /**
     * remove tax amount and line total from data to get Xero to calculate the amounts.
     *
     * @param $line
     *
     * @return mixed
     */
    public function removeTaxAmount($line)
    {
        if (isset($line['taxAmount'])) {
            unset($line['taxAmount']);
        }

        return $line;
    }

    public function removeLineTotalAmount($line)
    {
        if (isset($line['unitAmount'])) {
            unset($line['lineTotal']);
        }

        return $line;
    }

    public function getRoundingEntry($name, $roundingAmount, $roundingAcctCode)
    {
        $roundingAmount = $this->roundedAmount($roundingAmount);
        $data['qtyOrdered'] = '1.0000';
        $data['sku'] = '';
        $data['name'] = $name;
        $data['taxAmount'] = '0.0000';
        $data['taxType'] = 'NONE';
        $data['price'] = $roundingAmount;
        $data['unitAmount'] = $roundingAmount;
        $data['lineTotalNoAdjust'] = $roundingAmount;
        $data['lineTotal'] = $roundingAmount;
        $data['xeroAccountCodeSale'] = $roundingAcctCode;

        return $data;
    }

    public function getEffectiveLineTotal($line, $taxInclusive = 'Exclusive')
    {
        if (isset($line['unitAmount'])) {
            $effectiveLineTotal = $line['unitAmount'] * $line['qtyOrdered'];
        } else {
            $effectiveLineTotal = $line['lineTotal'];
        }

        //Quantity * Unit Amount * ((100 – DiscountRate)/100)
        /*@see http://developer.xero.com/documentation/api/invoices/#title2*/
        if (isset($line['discountRate'])) {
            $effectiveLineTotal = round($effectiveLineTotal * ((100 - $line['discountRate']) / 100), 2);
        }
        if (isset($line['unitAmount']) && $taxInclusive == 'Exclusive') {
            $effectiveLineTotal = $effectiveLineTotal * (1 + $line['taxPercent'] / 100);
        }

        return round($effectiveLineTotal, 2);
    }

    /**
     * Xero does not support discountRate on the CreditNote endpoint.
     *
     * @param $line
     */
    public function factorOutDiscount($line)
    {
        if (false === isset($line['discountRate'])) {
            $line['discountRate'] = 0;
        }
        $line['unitAmount'] = $this->roundedAmount($line['unitAmount'] * ((100 - $line['discountRate']) / 100));
        $line['discountRate'] = 0;

        return $line;
    }

    public function getXeroStatus()
    {
        return $this->scopeConfig->getValue(
            'foomanconnect/order/xerostatus',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getSalesObject()->getStoreId()
        );
    }
}
