<?php

namespace Fooman\Connect\Model\Tracking;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Rule extends \Magento\Framework\Model\AbstractModel
{
    const TYPE_GROUP = 'group';
    const TYPE_CUSTOMER = 'customer';
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    protected function _construct()
    {
        $this->_init('Fooman\Connect\Model\ResourceModel\Tracking\Rule');
    }

    public function loadCustomerGroupRule($groupId)
    {
        $collection = $this->getCollection();
        $collection->addFieldToFilter('source_id', $groupId);
        $collection->addFieldToFilter('type', self::TYPE_GROUP);
        $collection->setPageSize(1);

        return $collection->getFirstItem();
    }
}
