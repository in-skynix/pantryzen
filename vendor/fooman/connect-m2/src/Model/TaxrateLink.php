<?php

namespace Fooman\Connect\Model;

class TaxrateLink extends \Magento\Framework\Model\AbstractModel
{

    protected function _construct()
    {
        $this->_init(ResourceModel\TaxrateLink::class);
    }

}
