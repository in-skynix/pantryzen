<?php

namespace Fooman\Connect\Model\Config\Backend;

class ExportMode extends \Magento\Framework\App\Config\Value
{

    /**
     * @var \Magento\Framework\App\Config\ValueFactory
     */
    protected $configValueFactory;

    /**
     * ExportMode constructor.
     *
     * @param \Magento\Framework\Model\Context                             $context
     * @param \Magento\Framework\Registry                                  $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface           $config
     * @param \Magento\Framework\App\Cache\TypeListInterface               $cacheTypeList
     * @param \Magento\Framework\App\Config\ValueFactory                   $configValueFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null           $resourceCollection
     * @param array                                                        $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\ValueFactory $configValueFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->configValueFactory = $configValueFactory;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }


    public function afterSave()
    {
        if ($this->getValue() == \Fooman\Connect\Model\System\ExportMode::INVOICE_MODE) {
            $this->setConfigValue('foomanconnect/menu/display_invoice', true);
            $this->setConfigValue('foomanconnect/menu/display_order', false);
        } else {
            $this->setConfigValue('foomanconnect/menu/display_invoice', false);
            $this->setConfigValue('foomanconnect/menu/display_order', true);
        }
        return parent::afterSave();
    }

    protected function setConfigValue($path, $value)
    {
        /** @var $setting \Magento\Config\Model\ResourceModel\Config\Data\Collection */
        $setting = $this->configValueFactory->create()->load($path, 'path');
        $setting->setPath($path);
        $setting->setValue($value)->save();
    }
}
