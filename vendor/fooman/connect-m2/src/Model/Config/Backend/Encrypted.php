<?php

namespace Fooman\Connect\Model\Config\Backend;

class Encrypted extends \Magento\Config\Model\Config\Backend\Encrypted
{
    const CACHE_TAG = 'fooman-encrypted';

    protected $_cacheTag = self::CACHE_TAG;

    public function beforeSave()
    {
        parent::beforeSave();
        $value = (string)$this->getValue();
        if (empty($value)) {
            $this->_dataSaveAllowed = true;
        }
    }
}
