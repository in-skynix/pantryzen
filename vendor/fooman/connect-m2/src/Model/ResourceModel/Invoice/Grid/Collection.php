<?php

namespace Fooman\Connect\Model\ResourceModel\Invoice\Grid;

class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{

    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        $mainTable = 'foomanconnect_invoice',
        $resourceModel = 'Fooman\Connect\Model\ResourceModel\Invoice'
    ) {

        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }

    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->joinRight(
            ['_joined_table' => $this->getTable('sales_invoice')],
            'main_table.invoice_id=_joined_table.entity_id'
        );

        $this->getSelect()->joinLeft(
            ['order_address' => 'sales_order_address'],
            "`order_address`.`parent_id` = `_joined_table`.`entity_id` AND `order_address`.`address_type`='billing'",
            ['company', 'lastname']
        );

        return $this;
    }

    protected function beforeAddLoadedItem(\Magento\Framework\DataObject $item)
    {
        if (null === $item->getInvoiceId()) {
            $item->setInvoiceId($item->getEntityId());
        }
    }
}
