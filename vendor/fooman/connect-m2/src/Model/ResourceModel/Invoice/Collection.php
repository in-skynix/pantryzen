<?php

namespace Fooman\Connect\Model\ResourceModel\Invoice;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Collection extends \Fooman\Connect\Model\ResourceModel\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Fooman\Connect\Model\Invoice', 'Fooman\Connect\Model\ResourceModel\Invoice');
    }

    /**
     * @param $storeId
     *
     * @return \Fooman\Connect\Model\ResourceModel\Invoice\Collection $this
     */
    public function getUnexportedOrders($storeId)
    {
        $statusToExport = $this->scopeConfig->getValue(
            'foomanconnect/order/exportwithstatusinvoices',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        $this->addFieldToSelect('invoice_id');
        $this->addFieldToFilter(
            'xero_export_status',
            [\Fooman\Connect\Model\Status::NOT_EXPORTED, ['null' => true]]
        );
        $this->addFieldToFilter('state', ['in' => explode(',', $statusToExport)]);
        $this->addFieldToFilter('store_id', $storeId);

        return $this;
    }

    public function addConfigDateFilter($storeId)
    {
        $date = $this->connectHelper->getOrderStartDate($storeId);
        if (false !== $date) {
            $this->addFieldToFilter('_invoice_table.created_at', ['gteq' => $date]);
        }

        return $this;
    }

    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->joinRight(
            ['_invoice_table' => $this->getTable('sales_invoice')],
            'main_table.invoice_id=_invoice_table.entity_id'
        );

        return $this;
    }
}
