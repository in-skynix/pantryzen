<?php

namespace Fooman\Connect\Model\ResourceModel\Creditmemo;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Collection extends \Fooman\Connect\Model\ResourceModel\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Fooman\Connect\Model\Creditmemo', 'Fooman\Connect\Model\ResourceModel\Creditmemo');
    }

    /**
     * @param $storeId
     *
     * @return \Fooman\Connect\Model\ResourceModel\Creditmemo\Collection $this
     */
    public function getUnexportedCreditmemos($storeId)
    {
        $this->addFieldToSelect('creditmemo_id');
        $this->addFieldToFilter(
            'main_table.xero_export_status',
            [\Fooman\Connect\Model\Status::NOT_EXPORTED, ['null' => true]]
        );
        $this->addFieldToFilter('store_id', $storeId);

        return $this;
    }

    public function addConfigDateFilter($storeId)
    {
        $date = $this->connectHelper->getCreditMemoStartDate($storeId);
        if (false !== $date) {
            $this->addFieldToFilter('_creditmemo_table.created_at', ['gteq' => $date]);
        }

        return $this;
    }

    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->joinRight(
            ['_creditmemo_table' => $this->getTable('sales_creditmemo')],
            'main_table.creditmemo_id=_creditmemo_table.entity_id'
        );

        return $this;
    }

    public function getRawSize()
    {
        $select = clone $this->getSelect();
        $select->reset();
        $select->from($this->getMainTable(), 'COUNT(*)');

        return $this->getConnection()->fetchOne($select);
    }
}
