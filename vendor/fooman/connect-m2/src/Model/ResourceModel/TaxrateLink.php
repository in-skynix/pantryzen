<?php

namespace Fooman\Connect\Model\ResourceModel;

class TaxrateLink extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected $_useIsObjectNew = true;
    protected $_isPkAutoIncrement =false;

    const TABLE_NAME = 'foomanconnect_taxrate_link';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, 'taxrate_id');
    }

}
