<?php

namespace Fooman\Connect\Model\ResourceModel;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Order extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected $_isPkAutoIncrement = false;
    protected $_useIsObjectNew = true;

    protected function _construct()
    {
        $this->_init('foomanconnect_order', 'order_id');
    }
}
