<?php

namespace Fooman\Connect\Model\ResourceModel\Collection;

class AbstractCollection extends \Magento\Framework\Model\ResourceModel\Db\VersionControl\Collection
{
    protected $scopeConfig;

    protected $connectHelper;

    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\Model\ResourceModel\Db\VersionControl\Snapshot $entitySnapshot,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Fooman\Connect\Helper\Data $connectHelper,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->connectHelper = $connectHelper;
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $entitySnapshot,
            $connection,
            $resource
        );
    }

    /**
     * Collection can't correctly combine null key items with
     * non-null key items as the array key could clash with real id value.
     *
     * @param \Magento\Framework\DataObject $item
     *
     * @return mixed
     */
    protected function _getItemId(\Magento\Framework\DataObject $item)
    {
        $value = parent::_getItemId($item);
        if ($value) {
            return $value;
        } else {
            return $item->getEntityId();
        }
    }
}
