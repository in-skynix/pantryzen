<?php

namespace Fooman\Connect\Model\ResourceModel\Order;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Collection extends \Fooman\Connect\Model\ResourceModel\Collection\AbstractCollection
{
    protected $_idFieldName = 'order_id';

    protected function _construct()
    {
        $this->_init('Fooman\Connect\Model\Order', 'Fooman\Connect\Model\ResourceModel\Order');
    }

    /**
     * @param $storeId
     *
     * @return \Fooman\Connect\Model\ResourceModel\Order\Collection $this
     */
    public function getUnexportedOrders($storeId)
    {
        $statusToExport = $this->scopeConfig->getValue(
            'foomanconnect/order/exportwithstatus',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        $this->addFieldToSelect('order_id');
        $this->addFieldToFilter(
            'main_table.xero_export_status',
            [\Fooman\Connect\Model\Status::NOT_EXPORTED, ['null' => true]]
        );
        $this->addFieldToFilter('status', ['in' => explode(',', $statusToExport)]);
        $this->addFieldToFilter('store_id', $storeId);

        return $this;
    }

    public function addConfigDateFilter($storeId)
    {
        $date = $this->connectHelper->getOrderStartDate($storeId);
        if (false !== $date) {
            $this->addFieldToFilter('_order_table.created_at', ['gteq' => $date]);
        }

        return $this;
    }

    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->joinRight(
            ['_order_table' => $this->getTable('sales_order')],
            'main_table.order_id=_order_table.entity_id'
        );

        return $this;
    }

    public function getRawSize()
    {
        $select = clone $this->getSelect();
        $select->reset();
        $select->from($this->getMainTable(), 'COUNT(*)');

        return $this->getConnection()->fetchOne($select);
    }
}
