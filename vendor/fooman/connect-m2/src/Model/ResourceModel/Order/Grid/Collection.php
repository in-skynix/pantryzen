<?php

namespace Fooman\Connect\Model\ResourceModel\Order\Grid;

class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        $mainTable = 'foomanconnect_order',
        $resourceModel = 'Fooman\Connect\Model\ResourceModel\Order'
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }

    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->joinRight(
            ['_order_table' => $this->getTable('sales_order')],
            'main_table.order_id=_order_table.entity_id'
        );

        $this->getSelect()->joinLeft(
            ['order_address' => 'sales_order_address'],
            "`order_address`.`parent_id` = `_order_table`.`entity_id` AND `order_address`.`address_type`='billing'",
            ['company', 'lastname']
        );

        return $this;
    }
   
    protected function beforeAddLoadedItem(\Magento\Framework\DataObject $item)
    {
        if (null === $item->getOrderId()) {
            $item->setOrderId($item->getEntityId());
        }
    }
}
