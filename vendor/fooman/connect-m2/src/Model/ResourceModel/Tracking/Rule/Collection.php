<?php

namespace Fooman\Connect\Model\ResourceModel\Tracking\Rule;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Collection extends \Magento\Framework\Model\ResourceModel\Db\VersionControl\Collection
{
    protected function _construct()
    {
        $this->_init('Fooman\Connect\Model\Tracking\Rule', 'Fooman\Connect\Model\ResourceModel\Tracking\Rule');
    }
}
