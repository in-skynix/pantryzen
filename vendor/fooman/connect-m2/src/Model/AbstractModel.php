<?php

namespace Fooman\Connect\Model;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

abstract class AbstractModel extends \Magento\Framework\Model\AbstractModel
{
    const PROCESS_PER_RUN = 50;

    /**
     * @var \Fooman\Connect\Model\Xero\Api
     */
    protected $connectXeroApi;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $backendAuthSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Fooman\Connect\Helper\Data
     */
    protected $connectHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Fooman\Connect\Model\Xero\Api $connectXeroApi,
        \Magento\Backend\Model\Auth\Session $backendAuthSession,
        \Fooman\Connect\Helper\Data $connectHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->connectXeroApi = $connectXeroApi;
        $this->eventManager = $context->getEventDispatcher();
        $this->backendAuthSession = $backendAuthSession;
        $this->logger = $context->getLogger();
        $this->connectHelper = $connectHelper;
        $this->scopeConfig = $scopeConfig;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * @param $xml
     * @param $storeId
     *
     * @return mixed
     */
    abstract public function sendToXero($xml, $storeId);

    /**
     * @return \Fooman\Connect\Model\Xero\Api
     */
    public function getApi()
    {
        return $this->connectXeroApi;
    }

    /**
     * @param string $eventName
     * @param string $eventObject
     * @param array  $data
     *
     * @return mixed
     */
    protected function _dispatchEvent($eventName, $eventObject, array $data)
    {
        $eventData = new \Magento\Framework\DataObject($data);
        $this->eventManager->dispatch('fooman_connect_model_'.$eventName, [$eventObject => $eventData]);

        return $eventData->getData();
    }

    /**
     * @param string $permissionName
     *
     * @return bool|int
     */
    protected function _getSalesEntityViewId($permissionName)
    {
        if (true === $this->backendAuthSession->isAllowed('sales/'.$permissionName.'/actions/view')
            && $this->getEntityId() > 0
        ) {
            return (int) $this->getEntityId();
        }

        return false;
    }

    protected function _handleError($status, $exception, $message, $object, $data)
    {
        $status->setXeroExportStatus(\Fooman\Connect\Model\Status::ATTEMPTED_BUT_FAILED);
        $status->setXeroLastValidationErrors(json_encode($message));
        $status->save();

        $this->logger->critical($exception);

        $this->connectHelper->debug('-------------------------');
        $this->connectHelper->debug($object->debug());
        $this->connectHelper->debug($data);
        if ($exception instanceof \Fooman\Connect\Exception) {
            $this->connectHelper->debug($exception->getXeroErrors());
            if (is_array($exception->getXeroErrors())) {
                $message = '<br/>Errors received from Xero: '.implode('<br/>', $exception->getXeroErrors());
            }
        }
        $this->connectHelper->debug('-------------------------');
        throw new \Magento\Framework\Exception\LocalizedException(
            __('%1: Export did not succeed %2', $object->getIncrementId(), $message)
        );
    }
}
