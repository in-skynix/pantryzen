<?php

namespace Fooman\Connect\Observer;

class AddTaxrateLinkDropdownObserver implements \Magento\Framework\Event\ObserverInterface
{
    protected $configHelper;
    protected $systemTaxOptions;
    protected $taxrateLinkRepo;
    protected $registry;

    public function __construct(
        \Fooman\Connect\Helper\Config $configHelper,
        \Fooman\Connect\Model\System\TaxOptions $systemTaxOptions,
        \Fooman\Connect\Model\TaxrateLinkRepository $foomanTaxrateLinkRepository,
        \Magento\Framework\Registry $registry
    ) {
        $this->configHelper = $configHelper;
        $this->systemTaxOptions = $systemTaxOptions;
        $this->taxrateLinkRepo = $foomanTaxrateLinkRepository;
        $this->registry = $registry;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();
        if ($this->configHelper->isConfigured()) {
            if ($block instanceof \Magento\Tax\Block\Adminhtml\Rate\Form) {
                $this->addTaxRateLinkSelect($block);
            }
        }
    }

    /**
     * @param $block
     */
    protected function addTaxRateLinkSelect($block)
    {
        $form = $block->getForm();

        $fieldset = $form->getElement('foomanconnect_fieldset');
        if (!$fieldset) {
            $fieldset = $block->getForm()->addFieldset(
                'foomanconnect_fieldset',
                ['legend' => __('Fooman Connect')]
            );
        }

        $taxRateId = $this->registry->registry(\Magento\Tax\Controller\RegistryConstants::CURRENT_TAX_RATE_ID);
        $taxrateLink = $this->taxrateLinkRepo->get($taxRateId);

        $fieldset->addField(
            'xero_rate',
            'select',
            [
                'name'     => 'xero_rate',
                'label'    => __('Xero Rate'),
                'title'    => __('Xero Rate'),
                'value'    => $taxrateLink->getXeroTaxrate(),
                'values'   => $this->systemTaxOptions->getTaxRatesForAllStores(),
                'required' => true,
                'class'    => 'required-entry',
            ]
        );


    }
}
