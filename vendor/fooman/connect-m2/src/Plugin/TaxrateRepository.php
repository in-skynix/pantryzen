<?php

namespace Fooman\Connect\Plugin;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2013 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class TaxrateRepository
{

    protected $foomanTaxrateLinkRepository;

    public function __construct(
        \Fooman\Connect\Model\TaxrateLinkRepository $foomanTaxrateLinkRepository
    ) {
        $this->foomanTaxrateLinkRepository = $foomanTaxrateLinkRepository;
    }

    public function afterSave(
        \Magento\Tax\Api\TaxRateRepositoryInterface $subject,
        \Magento\Tax\Api\Data\TaxRateInterface $taxrate
    ) {
        /** @var  \Magento\Tax\Api\Data\TaxRateExtensionInterface $extAttrs */
        $extAttrs = $taxrate->getExtensionAttributes();
        if ($extAttrs) {
            $taxRateLink = $this->foomanTaxrateLinkRepository->get($taxrate->getId());

            if (!$taxRateLink->getId()) {
                $taxRateLink->isObjectNew(true);
            }
            $taxRateLink->setTaxrateId($taxrate->getId());
            $taxRateLink->setMagentoCode($taxrate->getCode());
            $taxRateLink->setXeroTaxrate($extAttrs->getFoomanXeroTaxRate());
            $this->foomanTaxrateLinkRepository->save($taxRateLink);
        }

    }
}
