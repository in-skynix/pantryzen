<?php

namespace Fooman\Connect\Plugin;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2013 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class PopulateTaxrateData
{
    protected $taxrateExtensionFactory;

    public function __construct(
        \Magento\Tax\Api\Data\TaxRateExtensionFactory $taxRateExtensionFactory
    ) {
        $this->taxrateExtensionFactory = $taxRateExtensionFactory;
    }

    public function aroundPopulateTaxRateData(
        \Magento\Tax\Model\Calculation\Rate\Converter $subject,
        \Closure $proceed,
        $formData
    ) {
        /** @var \Magento\Tax\Api\Data\TaxRateInterface $taxrate */
        $taxrate = $proceed($formData);
        $extAttrs = $taxrate->getExtensionAttributes();
        if (null === $extAttrs) {
            /** @var \Magento\Tax\Api\Data\TaxRateExtensionInterface $extAttrs */
            $extAttrs = $this->taxrateExtensionFactory->create();
        }

        $extAttrs->setFoomanXeroTaxrate($formData['xero_rate']);
        $taxrate->setExtensionAttributes($extAttrs);
        return $taxrate;
    }
}
