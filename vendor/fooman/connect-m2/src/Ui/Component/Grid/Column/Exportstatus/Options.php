<?php

namespace Fooman\Connect\Ui\Component\Grid\Column\Exportstatus;

class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    public function toOptionArray()
    {
        $options = [];
        $statuses = \Fooman\Connect\Model\Status::getStatuses(true);
        foreach ($statuses as $value => $label) {
            $options[] = ['value' => $value, 'label' => $label];
        }

        return $options;
    }
}
