<?php

namespace Fooman\Connect\Ui\Component\Grid\Column;

/*
 * @author     Kristof Ringleff
 * @package    Fooman_Connect
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Exportstatus extends \Magento\Ui\Component\Listing\Columns\Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (null === $item['xero_export_status']) {
                    $item['xero_export_status'] = \Fooman\Connect\Model\Status::NOT_EXPORTED;
                }
            }
        }

        return $dataSource;
    }
}
