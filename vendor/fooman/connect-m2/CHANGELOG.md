# Change Log

## [1.0.1] 2016-06-10
### Fixed
- Streamline Context dependencies

## [1.0.0] - 2016-06-07
### Changed
- Initial release for Magento 2
