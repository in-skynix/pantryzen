<?php

namespace Joel\Custom\Block;

/**
 * Onepage checkout block
 */
class Onepage extends \Magento\Checkout\Block\Onepage
{

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Checkout\Model\CompositeConfigProvider $configProvider,
        \Magento\Framework\ObjectManagerInterface $objectInterface,
        array $layoutProcessors = [],
        array $data = []
    ) {
        parent::__construct($context, $formKey, $configProvider, $layoutProcessors, $data);
        $this->_objectManager = $objectInterface;
    }


    /**
     * @return string
     */


}
