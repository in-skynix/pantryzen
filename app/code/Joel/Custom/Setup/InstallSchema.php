<?php 
namespace Joel\Custom\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
    	$ObjectManager =  \Magento\Framework\App\ObjectManager::getInstance();
    	$eavmodel = $ObjectManager->create('Magento\Eav\Model\Entity\Attribute')->load('telephone','attribute_code');
    	if($eavmodel->getEntityTypeId() == 2)
    	{
    		$eavmodel = $ObjectManager->create('Magento\Eav\Model\Entity\Attribute')->load($eavmodel->getAttributeId());
    		$eavmodel->setData('is_required',0);
    		$eavmodel->save();
    		
    	}
    	//print_r($eavmodel->getData()); die("kh");
    }

}