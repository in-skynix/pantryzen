<?php
namespace Pantryzen\Popup\Block;
class Popup extends \Magento\Framework\View\Element\Template
{
	public function _prepareLayout()
	{
		return parent::_prepareLayout();
	}

	public function isPopupEnabled()
    {
        return $this->_scopeConfig->getValue('popup/popup/popup_enable');
    }

    public function getHeading()
    {
        return $this->_scopeConfig->getValue('popup/popup/popup_heading_text');
    }

    public function getSubHeading()
    {
        return $this->_scopeConfig->getValue('popup/popup/popup_sub_heading_text');
    }
}
?>