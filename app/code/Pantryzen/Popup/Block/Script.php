<?php
namespace Pantryzen\Popup\Block;

class Script extends \Magento\Framework\View\Element\Template
{
    public function getApiKey()
    {
        return $this->_scopeConfig->getValue('popup/popup/popup_google_map_key');
    }
    public function getDefaultLocation()
    {
        return $this->_scopeConfig->getValue('popup/popup/popup_google_map_default_location');
    }
}

?>