<?php

namespace Magentopush\Recommendations\Block;

use Magento\Catalog\Block\Product;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Catalog\Block\Product\ListProduct;

class Plist extends ListProduct
{
    /**
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    protected $_stockHelper;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * Product factory
     *
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param \Magento\CatalogInventory\Helper\Stock $stockHelper
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\CatalogInventory\Helper\Stock $stockHelper,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        array $data = []
    ) {
        $this->_stockHelper = $stockHelper;
        $this->_productFactory = $productFactory;
        $this->_resource = $resource;
        parent::__construct($context, $postDataHelper,
            $layerResolver, $categoryRepository, $urlHelper,  $data);
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        $orders = $this->getAvailableOrders();
        if ($orders) {
            $toolbar->setAvailableOrders($orders);
        }
        $sort = $this->getSortBy();
        if ($sort) {
            $toolbar->setDefaultOrder($sort);
        }
        $dir = $this->getDefaultDirection();
        if ($dir) {
            $toolbar->setDefaultDirection($dir);
        }
        $modes = $this->getModes();
        if ($modes) {
            $toolbar->setModes($modes);
        }

        // set collection to toolbar and apply sort
        $toolbar->setData('_current_limit', 6);
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);
        $this->_eventManager->dispatch(
            'catalog_block_product_list_collection',
            ['collection' => $this->_getProductCollection()]
        );

        $this->_getProductCollection()->load();

        return \Magento\Framework\View\Element\AbstractBlock::_beforeToHtml();
    }

    /**
     * Retrieve loaded category collection
     *
     * @return AbstractCollection
     */
    protected function _getProductCollection()
    {
        if ($this->_productCollection === null) {
            $this->_productCollection = $this->_productFactory->create()->getCollection();

            $this->_productCollection->addAttributeToSelect('*');

            $this->_stockHelper->addIsInStockFilterToCollection($this->_productCollection);

            if ($productId = $this->getRequest()->getParam('id')) {

                /** @var \Magento\Framework\DB\Adapter\AdapterInterface $conn */
                $conn = $this->_resource->getConnection();

                $orderIds = $conn->fetchCol(
                    $conn->select()->from(
                        ['order_item' => $this->_resource->getTableName('sales_order_item')], ['order_id']
                    )->where('order_item.product_id = ?', $productId)
                );

                $this->_productCollection->getSelect()->join(
                    ['order_item' => $this->_resource->getTableName('sales_order_item')],
                    'order_item.product_id = e.entity_id',
                    ['cnt' => new \Zend_Db_Expr('COUNT(order_item.item_id)')]
                );

                $this->_productCollection->getSelect()
                    ->where('order_item.order_id IN(?)', $orderIds)
                    ->where('order_item.product_id != ?', $productId)
                    ->where('order_item.parent_item_id IS NULL')
                    ->group('order_item.product_id')
                    ->order('cnt DESC');
            }
        }
        return $this->_productCollection;
    }
}