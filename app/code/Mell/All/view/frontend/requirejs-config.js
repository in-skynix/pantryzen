/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            quickSearch:
                'Mell_All/form-mini',
            'Magento_Checkout/template/minicart/content.html':
                'Mell_All/template/minicart/content.html',
            'Magento_Tax/template/checkout/minicart/subtotal/totals.html':
                'Mell_All/template/checkout/minicart/subtotal/totals.html'
        }
    }
};