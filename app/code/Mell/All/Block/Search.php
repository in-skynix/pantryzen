<?php
namespace Mell\All\Block;
use Magento\Framework\View\Element\Template;

class Search extends Template
{
    protected $catalogCategory;

    public function __construct(Template\Context $context, array $data = [], \Magento\Catalog\Helper\Category $catalogCategory)
    {
        $this->catalogCategory = $catalogCategory;
        parent::__construct($context, $data);
    }

    public function getCategories()
    {
        $categories = [];

        foreach ($this->catalogCategory->getStoreCategories() as $category) {
            if ($category->getIsActive() && isset($category['entity_id']) && isset($category['name'])) {
                $categories[$category['entity_id']] = $category['name'];
            }
        }

        return $categories;
    }

    public function getCurrentCategoryId() {
        return $this->getRequest()->getParam('cat');
    }
}
