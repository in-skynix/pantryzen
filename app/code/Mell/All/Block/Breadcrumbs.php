<?php

namespace Mell\All\Block;

use Magento\Store\Model\Store;

class Breadcrumbs extends \Magento\Catalog\Block\Breadcrumbs
{

    protected function _prepareLayout()
    {
        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbsBlock->addCrumb(
                'home',
                [
                    'label' => __('Home'),
                    'title' => __('Go to Home Page'),
                    'link' => $this->_storeManager->getStore()->getBaseUrl()
                ]
            );

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $currentProduct = $objectManager->get('Magento\Framework\Registry')->registry('current_product');
            $currentCategory = $objectManager->get('Magento\Framework\Registry')->registry('current_category');

            if($currentProduct) {
                $categories = $currentProduct->getCategoryCollection();

                foreach ($categories as $category) {
                    $objectManager->get('Magento\Framework\Registry')->unregister('current_category');
                    $objectManager->get('Magento\Framework\Registry')->register('current_category', $category);
                }
            }

            $title = [];
            $path = $this->_catalogData->getBreadcrumbPath();

            $objectManager->get('Magento\Framework\Registry')->unregister('current_category');
            $objectManager->get('Magento\Framework\Registry')->register('current_category', $currentCategory);

            foreach ($path as $name => $breadcrumb) {
                $breadcrumbsBlock->addCrumb($name, $breadcrumb);
                $title[] = $breadcrumb['label'];
            }

            $this->pageConfig->getTitle()->set(join($this->getTitleSeparator(), array_reverse($title)));
        }
        return parent::_prepareLayout();
    }
}
